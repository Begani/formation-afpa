<?php
    $res = "";
    $mesContacts = [];
    $bTrouve = false;
    if(isset($_POST['name']) && isset($_POST['newName'])){

        $sName = ucfirst($_POST['name']);
        $sNewName = ucfirst($_POST['newName']);

        $fichier = fopen("Contact.txt", 'r');
        $i = -1;

        while(!feof($fichier)){
            $i = $i + 1;
            $ligne = fgets($fichier); // Lit ligne par ligne 
            $monContact = [
                'Nom' =>  substr($ligne, 0, 20), 
                'Prenom' => substr($ligne, 20, 20),  
                'Tel' =>  substr($ligne, 40, 11), 
                'Mail' => substr($ligne, 51, 20)
            ];
            
            //si le nom a modifier correspond au nom trouvé dans monContact alors on le modifie avec le nouveau
            if( ($sName == trim($monContact['Nom']))){
                $monContact['Nom'] = $sNewName;
                $bTrouve = true;
            }
            //J'ajoute le contact récupéré dans le tableau $mesContacts
            $mesContacts[$i] = $monContact;
        }  
        fclose($fichier);

        if($bTrouve == false){
            $res = "Aucun contact à ce nom n'a été trouvé, veuillez recommencer.";
        }
        else{
            $fichier = fopen('Contact.txt', 'w+b');
            for($j = 0; $j < count($mesContacts); $j++){
    
            fputs($fichier, trim(str_pad($mesContacts[$j]['Nom'], 20, " ") 
                            . str_pad($mesContacts[$j]['Prenom'], 20, " ") 
                            . str_pad($mesContacts[$j]['Tel'], 11 , " ") 
                            . str_pad($mesContacts[$j]['Mail'], 20, " "))
                            . "\r" ."\n");
        }
        fclose($fichier);
        }
        file_put_contents('Contact.txt',
            preg_replace('~[\r\n]+~', "\r\n", trim(file_get_contents('Contact.txt')))
        );
    }

    require 's10exercice3.html';
?>