<?php 

    $res = "";
    //Tableau multidim numéroté stockant des tableaux associatifs
    $mesContacts = [];
    $bInsere = false;
    if(isset($_POST['Nom']) && isset($_POST['Prenom']) && isset($_POST['Tel']) && isset($_POST['Mail'])){
        //Récupération données utilisateur
        $sNom = $_POST['Nom'];
        $sPrenom = $_POST['Prenom'];
        $sTel = $_POST['Tel'];
        $sMail = $_POST['Mail'];

        //Stocke les données récupérées dans un tableau qui sera injecté dans $mesContacts
        //str_pad pour ajouter " " a la fin de la chaine récupéré jsq'a la taille définie
        $newContact = ['Nom' => ucfirst(str_pad($sNom, 20 , " ")),
                       'Prenom' => str_pad($sPrenom, 20 , " "), 
                       'Tel' => str_pad($sTel, 11 , " "), 
                       'Mail' => str_pad($sMail, 20 , " ")
                      ];
        
        //Ouverture Contact.txt en lecture seule
        $fichier = fopen("Contact.txt", 'r'); 
     
        //Remplissage $mesContacts avec le contenu de Contact.txt
        $i = -1;
        
        while(!feof($fichier)){
            $i = $i + 1;
            $ligne = fgets($fichier); // Lit ligne par ligne 
            $monContact = [
                'Nom' =>  substr($ligne, 0, 20), 
                'Prenom' => substr($ligne, 20, 20),  
                'Tel' =>  substr($ligne, 40, 11), 
                'Mail' => substr($ligne, 51, 20)
            ];
            
            //Réorganisation du fichier txt
            if( ($monContact['Nom'] > $newContact['Nom']) && $bInsere == false){
                $mesContacts[$i] = $newContact;
                $bInsere = true;
                $i = $i + 1;
            }
            $mesContacts[$i] = $monContact;
            $res .= $mesContacts[$i]['Nom'] . " " . $mesContacts[$i]['Prenom'] . " " . $mesContacts[$i]['Tel'] . " " . trim($mesContacts[$i]['Mail']) . "</br>" ; 
        }  
        fclose($fichier);

        //Ici, $mesContacts est composé de tableau associatif rempli avec le fichier texte + nouveau contact ou non
        //Si non :
        if($bInsere == false){
            $i = $i + 1;
            $mesContacts[$i] = $newContact;
            $bInsere = true;
        }

        $fichier = fopen('Contact.txt', 'w+b');
        for($j = 0; $j < count($mesContacts); $j++){
    
            fputs($fichier, trim(str_pad($mesContacts[$j]['Nom'], 20, " ") 
                            . str_pad($mesContacts[$j]['Prenom'], 20, " ") 
                            . str_pad($mesContacts[$j]['Tel'], 11 , " ") 
                            . str_pad($mesContacts[$j]['Mail'], 20, " "))
                            . "\r" ."\n");
        }
        fclose($fichier);
        
    //Suppression des sauts de lignes
    file_put_contents('Contact.txt',
        preg_replace('~[\r\n]+~', "\r\n", trim(file_get_contents('Contact.txt')))
    );
        require 's10exercice2.html';
    }
    else
    {
        require 's10exercice2.html';
    }

?>