<?php 


   $aTab = [];

   $iMax = 0;
   $sTableau = "";
   $sMessage8_2 = "";

   for($i = 0 ; $i < 12 ; $i++)
   {   
       $aTab[$i] = [];

       for($j = 0; $j < 8 ; $j++)
       {
           $aTab[$i][$j] = random_int(1,100);
           $sTableau .= $aTab[$i][$j] . ' ';
           //A la première itération et / ou si valeur récup > iMax on injecte la valeur à iMax
           if( ($j == 0 && $aTab[$i][$j] > $iMax) || $aTab[$i][$j] >  $iMax)
           {
               $iMax = $aTab[$i][$j];
           }
       }

       $sTableau .= '</br>';
   }
   $sMessage8_2 = $sTableau . '</br> La plus grande valeur est : ' . $iMax;

   require 's8exercice2.html';
?>
