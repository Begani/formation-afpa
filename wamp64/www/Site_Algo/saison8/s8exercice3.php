<?php 
    //ASTUCE POUR STOCKER DE MANIERE 
    $aDamier = [];
    $i = 0;
    $j = 0;
    
    //Affichage + actualisation tableau
    $sHTML = "";

    //Variables de déplacement
    $iMove = 0;

    //Variable de position
    $iLigne = 0;
    $iColonne = 0;

    //Variable de position stockée
    $iPosX = 0;
    $iPosY = 0;
    //ETAPE 1 :
    //Au chargement de la page
    if( !isset($_POST['ligne_php']) && !isset($_POST['colonne_php']) )
    {
        for($i = 0; $i < 8 ; $i++)
        {
            $aDamier[$i] = [];
            for($j=0 ; $j < 8 ; $j++){
                $aDamier[$i][$j] = '0';
                $sHTML .= $aDamier[$i][$j] . ' ';
            }
            $sHTML .= "</br>";
        } 
        require 's8exercice3.html';
    }
    //ETAPE 2 :
    //Quand l'utilisateur a renseigné les positions du pion
    else if(isset($_POST['ligne_php']) && isset($_POST['colonne_php']) && !isset($_POST['move_php']))
    {
        //Récupère les positions
        
        $iLigne = $_POST['ligne_php']; 
        $iColonne = $_POST['colonne_php'];
        for($i = 0; $i < 8 ; $i++)
        {
            $aDamier[$i] = [];
            for($j=0 ; $j < 8 ; $j++)
            {
                $aDamier[$i][$j] = '0';
                //A la position on place le X
                if($i == $iLigne && $j == $iColonne)
                $aDamier[$i][$j] = 'X';
                $sHTML .= $aDamier[$i][$j] . ' ';
            }
            $sHTML .= "</br>";
        } 
        require 's8exercice3.html';
    }
    //ETAPE 3 :
    //Gestion du déplacement
    else if (   !isset($_POST['ligne_php'])
                && !isset($_POST['colonne_php']) 
                && isset($_POST['move_php']) 
                && isset($_POST['posX_php'])
                && isset($_POST['posY_php'])
            )
    {
        $iMove = $_POST['move_php'];
        $iPosX = $_POST['posX_php'];
        $iPosY = $_POST['posY_php'];
        var_dump($iMove, 'Move');
        var_dump($iPosX, 'X');
        var_dump($iPosY, 'Y');
        
        if($iMove == 0 && $iMove >= 0 && $iMove <= 7){
            if($iPosX >= 0 && $iPosY >= 0 && $iPosX <= 7 && $iPosY <= 7){
                $aDamier[$iPosX][$iPosY] = '0';
                $aDamier[$iPosX - 1][$iPosY - 1] = 'X';
                $iPosX--;
                $iPosY--;
            }
            else
            {
                $sError = "Vous êtes sorti du damier...";
            }
        }
         //Déplacement en Haut à Droite
        else if($iMove == 1 && $iMove >= 0 && $iMove <= 7){
            if($iPosX >= 0 && $iPosY >= 0 && $iPosX <= 7 && $iPosY <= 7){
                $aDamier[$iPosX][$iPosY] = '0';
                $aDamier[$iPosX - 1][$iPosY + 1] = 'X';
                $iPosX--;
                $iPosY++;
            }
            else
            {
                $sError = "Vous êtes sorti du damier...";
            }
        }
        //Déplacement en Bas à Gauche
        else if($iMove == 2 && $iMove >= 0 && $iMove <= 7){
            if($iPosX >= 0 && $iPosY >= 0 && $iPosX <= 7 && $iPosY <= 7){
                $aDamier[$iPosX][$iPosY] = '0';
                $aDamier[$iPosX + 1][$iPosY - 1] = 'X';
                $iPosX++;
                $iPosY--;
            }
            else
            {
                $sError = "Vous êtes sorti du damier...";
            }
        }
        //Déplacement en Bas à Droite
        else if($iMove == 3 && $iMove >= 0 && $iMove <= 7){
            if($iPosX >= 0 && $iPosY >= 0 && $iPosX <= 7 && $iPosY <= 7){
                $aDamier[$iPosX][$iPosY] = '0';
                $aDamier[$iPosX + 1][$iPosY + 1] = 'X';
                $iPosX++;
                $iPosY++;
            }
            else
            {
                $sError = "Vous êtes sorti du damier...";
            }
        }
        //Actualisation du tableau
        for($i=0 ; $i < 8 ; $i++){
            for($j=0 ; $j < 8 ; $j++){
                $sHTML += $aDamier[$i][$j] + ' ';
            }
            $sHTML += "</br>";
        }
        require 's8exercice3.html';
    }

?>