                            //EXERCICE 1


function s5_exo1js(){
    var iNb5_1 = document.getElementById('iNb5_1js').value;
    if( iNb5_1 < 1 || iNb5_1 > 3 )
    {
        document.getElementById("resultat5_1js").innerHTML = 'Veuillez recommencer s.v.p';
    }
    else
    {
        document.getElementById("resultat5_1js").innerHTML = 'Saisie acceptée';
    }
}


function s5_exo1jq(){
    var iNb5_1 = $('#iNb5_1jq').val();
    if( iNb5_1 < 1 || iNb5_1 > 3 )
    {
        $("#resultat5_1jq").html('Veuillez recommencer s.v.p');
    }
    else
    {
        $("#resultat5_1jq").html('Saisie acceptée');
    }
}
    


                            //EXERCICE 2
function s5_exo2_ajs(){
    //Recup + lecture entrée utilisateur
    var iNb5_2 = document.getElementById('iNb5_2_ajs').value;
    var result = "";

    if ( iNb5_2 < 10 )
    {
        result = 'Plus grand';
    }
    else if (iNb5_2 > 20)
    { 
        result = 'Plus petit';
    }
    else
    {
        result = 'Bien joué !'
    }
    document.getElementById("resultat5_2_ajs").innerHTML = result;
}

function s5_exo2_ajq(){
    //Recup + lecture entrée utilisateur
    var iNb5_2 = $('#iNb5_2_ajq').val();
    var result = "";

    if ( iNb5_2 < 10 )
    {
        result = 'Plus grand';
    }
    else if ( iNb5_2 > 20 )
    { 
        result = 'Plus petit';
    }
    else
    {
        result = 'Bien joué !';
    }
    $("#resultat5_2_ajq").html(result);
}

function s5_exo2_bjs(){
    var iNbUtilisateur = parseInt(document.getElementById("iNbUtilisateur").value);
    console.log(iNbUtilisateur, 'iNbUtilisateur');

    var iNbRandom = document.getElementById("iNbRandom").value;
    console.log(iNbRandom, 'iNbRandom');

    var iNbRandomNotExist = (iNbRandom == "");
    var cpt = document.getElementById("compteur").value;
    
    console.log(cpt, 'compteur');
    if(iNbRandomNotExist){
        cpt++;
        iNbRandom = Math.floor((Math.random() * 100) + 1);
        document.getElementById("iNbRandom").value = iNbRandom;
        console.log(iNbRandom, 'iNbRandom generated');
    }

    if(iNbRandom < iNbUtilisateur)
    {
        document.getElementById("resultat5_2_bjs").innerHTML = 'Plus petit, réessaie !';
        document.getElementById("compteur").value = ++cpt;
    }
    else if(iNbRandom > iNbUtilisateur)
    {
        document.getElementById("resultat5_2_bjs").innerHTML = 'Plus grand, réessaie !';
        document.getElementById("compteur").value = ++cpt;
    }
    else if(iNbRandom == iNbUtilisateur)
    {
        document.getElementById("resultat5_2_bjs").innerHTML = 'Bien joué ! Tu as réussi en ' + cpt + ' coups';
    }
    
}

function s5_exo2_bjq(){
    var iNbUtilisateur_jq = $("#iNbUtilisateur_jq").val();
    console.log(iNbUtilisateur_jq, 'iNbUtilisateur_jq');

    var iNbRandom_jq = $("#iNbRandom_jq").val();
    console.log(iNbRandom_jq, 'iNbRandom_jq');

    var iNbRandomNotExist_jq = (iNbRandom_jq == "");
    var cpt = $("#compteur8_jq").val();

    cpt++;
    $("#compteur8_jq").val(cpt);
    console.log(cpt);

    if(iNbRandomNotExist_jq){
       
        iNbRandom_jq = Math.floor((Math.random() * 100) + 1);
        $("#iNbRandom_jq").val(iNbRandom_jq);
        $('#compteur8_jq').val(cpt);
        console.log(iNbRandom_jq, 'iNbRandom_jq generated');
    }
    
    if(iNbRandom_jq < iNbUtilisateur_jq)
    {
        $("#resultat5_2_bjq").html('Plus petit, réessaie !');
    }
    else if(iNbRandom_jq > iNbUtilisateur_jq)
    {
        $("#resultat5_2_bjq").html('Plus grand, réessaie !');

    }
    else if(iNbRandom_jq == iNbUtilisateur_jq)
    {
        $("#resultat5_2_bjq").html('Bien joué ! Tu as réussi en ' + cpt + 'coups');
    }

}
                            //EXERCICE 3
function s5_exo3_js(){
    var sRep = "";
    var iNbr3 = parseInt(document.getElementById('iNbr3').value); //Entrée utilisateur
    var iMax = iNbr3 + 10;
    
    while(iNbr3 < iMax){
        iNbr3++ // augmente la valeur de iNombre de 1
        sRep += document.getElementById('resultat5_3_js').innerHTML += iNbr3 + ' ' ;//Affiche la valeur de iNombre
    }
}

function s5_exo3_jq(){
    var sRepjq ="";
    var iNbr3jq = parseInt($('#iNbr3_jq').val()); //Entrée utilisateur
    for(i=0; i<10; i++){
        iNbr3jq++ // augmente la valeur de iNombre de 1
        sRepjq += $('#resultat5_3_jq').append(iNbr3jq + ' ');//Affiche la valeur de iNombre
    }
}
                            //EXERCICE 4   
function s5_exo4_js(){
    var sRep ="";
    var iNbr4 = document.getElementById('iNbr4').value; //Entrée utilisateur
    for(i=0; i<10; i++){
        iNbr4++ // augmente la valeur de iNombre de 1
        sRep += document.getElementById('resultat5_4_js').innerHTML += iNbr4 + ' ' ;//Affiche la valeur de iNombre
    }
}
function s5_exo4_jq(){
    var sRep ="";
    var iNbr4 = $('#iNbr4_jq').val(); //Entrée utilisateur
    for(i=0; i<10; i++){
        iNbr4++ // augmente la valeur de iNombre de 1
        sRep += $('#resultat5_4_jq').append(iNbr4 + ' ') ;//Affiche la valeur de iNombre
    }
}

                            //EXERCICE 5
function s5_exo5_js(){
    var sRep ="";
    var multipliant;
    var iNbr5 = document.getElementById('iNbr5').value; //Entrée utilisateur
    for(multipliant=1; multipliant<11; multipliant++){
        sRep += document.getElementById('resultat5_5_js').innerHTML += iNbr5 + 'x' + multipliant + '</br>' ;//Affiche la valeur de iNombre
    }
}
function s5_exo5_jq(){
    var sRep ="";
    var multipliant;
    var iNbr5 = $('#iNbr5_jq').val(); //Entrée utilisateur
    for(multipliant=1; multipliant<11; multipliant++){
        sRep += $('#resultat5_5_jq').append(iNbr5 + 'x' + multipliant + '</br>') ;//Affiche la valeur de iNombre
    }
}

                            //EXERCICE 6
function s5_exo6_js(){
    var sRes = 0;
    var iNbr6 = parseInt(document.getElementById('iNbr6').value); //Entrée utilisateur
    var iNbrOriginal = iNbr6 ;
    
    for(i=1; i<=iNbr6; i++){
        sRes += i;
    }
    document.getElementById("resultat5_6_js").innerHTML =  'La somme de tous les entiers de ' + iNbrOriginal + ' est : ' + sRes;
}

function s5_exo6_jq(){
    var sRes = 0;
    var iNbr6 = parseInt($('#iNbr6_jq').val()); //Entrée utilisateur
    var iNbrOriginal = iNbr6 ;

    for(i=1; i<=iNbr6; i++){
        sRes += i;
    }
    $("#resultat5_6_jq").html('La somme de tous les entiers de ' + iNbrOriginal + ' est : ' + sRes);
}

                            //EXERCICE 7    
function s5_exo7_js(){
    var sRes = 1;
    var iNbr7 = parseInt(document.getElementById('iNbr7').value); //Entrée utilisateur
    var iNbrOriginal = iNbr7 ;
    
    for(i=1; i<=iNbr7; i++){
        sRes *= i;
    }
    document.getElementById("resultat5_7_js").innerHTML =  'La factorielle de ' + iNbrOriginal + ' est : ' + sRes;
}

function s5_exo7_jq(){
    var sRes = 1;
    var iNbr7 = parseInt($('#iNbr7_jq').val()); //Entrée utilisateur
    var iNbrOriginal = iNbr7 ;

    for(i=1; i<=iNbr7; i++){
        sRes *= i;
    }
    $("#resultat5_7_jq").html('La factorielle de ' + iNbrOriginal + ' est : ' + sRes);
}

                            //EXERCICE 8  


function s5_exo8_js(){

    var iNombreRecup = parseInt(document.getElementById('iNombreRecup').value);
    var iNombreStocke = document.getElementById('iNombreStocke').value;
    var compteur8 = parseInt(document.getElementById('compteur8').value);
    var conditionStock = (iNombreStocke < iNombreRecup);
    var compteurRes = document.getElementById('compteurRes').value;
    compteur8++; 
    document.getElementById("compteur8").value = compteur8;
    

    if(iNombreStocke == "" || conditionStock){
        iNombreStocke = iNombreRecup;
        document.getElementById("iNombreStocke").value = iNombreStocke;
        compteurRes = compteur8;
        document.getElementById("compteurRes").value = compteurRes;
    }
    else if(compteur8 == 10 || iNombreRecup == 0 ){
        var message = ' est le plus grand, c\'est le ' + compteurRes + 'è nombre renseigné';
        document.getElementById('resultat5_8_js').innerHTML = "Vous avez fini. " + iNombreStocke + message;
    }
}

function s5_exo8_jq(){
    var iNombreRecup = parseInt($('#iNombreRecup_jq').val());
    var iNombreStocke = $('#iNombreStocke_jq').val();
    var compteur8 = parseInt($('#compteur8_jq').val());
    var conditionStock = (iNombreStocke < iNombreRecup);
    var compteurRes = $('#compteurRes_jq').val();
    compteur8++; 
    $("#compteur8_jq").val(compteur8);
    
    if(iNombreStocke == "" || conditionStock){
        iNombreStocke = iNombreRecup;
        $("#iNombreStocke_jq").val(iNombreStocke);
        compteurRes = compteur8;
        $("#compteurRes_jq").val(compteurRes);
    }
    else if(compteur8 == 10 || iNombreRecup == 0 ){
        var message = ' est le plus grand, c\'est le ' + compteurRes + 'è nombre renseigné';
        $('#resultat5_8_jq').html("Vous avez fini. " + iNombreStocke + message);
    }
}

                            //EXERCICE 9
                            
function s5_exo9_js(){

    var iNombreRecup9 = parseInt(document.getElementById('iNombreRecup9').value);
    var iNombreStocke9 = document.getElementById('iNombreStocke9').value;
    var compteur9 = parseInt(document.getElementById('compteur9').value);
    var conditionStock9 = (iNombreStocke9 < iNombreRecup9);
    var compteurRes9 = document.getElementById('compteurRes9').value;
    compteur9++; 
    document.getElementById("compteur9").value = compteur9;
    
   
    if(iNombreStocke9 == "" || conditionStock9){
        iNombreStocke9 = iNombreRecup9;
        document.getElementById("iNombreStocke9").value = iNombreStocke9;
        compteurRes9 = compteur9;
        document.getElementById("compteurRes9").value = compteurRes9;
  
    }
    else if(iNombreRecup9 == 0 ){
        var message9 = ' est le plus grand, c\'est le ' + compteurRes9 + 'è nombre renseigné';
        document.getElementById('resultat5_9_js').innerHTML = "Vous avez fini. " + iNombreStocke9 + message9;
    }
}

function s5_exo9_jq(){
    var iNombreRecup9 = parseInt($('#iNombreRecup9_jq').val());
    var iNombreStocke9 = parseInt($('#iNombreStocke9_jq').val());
    var compteur9 = parseInt($('#compteur9_jq').val());
    var conditionStock9 = (iNombreStocke9 < iNombreRecup9);
    var compteurRes9 = $('#compteurRes9_jq').val();
    compteur9++; 

    $("#compteur9_jq").val(compteur9);
    console.log(iNombreRecup9);
    console.log(iNombreStocke9);

    if(iNombreStocke9 == 0 || conditionStock9){
        console.log('je passe par la');
        iNombreStocke9 = iNombreRecup9;
        $("#iNombreStocke9_jq").val(iNombreStocke9);
        compteurRes9 = compteur9;
        $("#compteurRes9_jq").val(compteurRes9); 
        console.log(iNombreRecup9);
    }
    else if(iNombreRecup9 == 0){
        console.log('hello c\'est zéro!');
        var message9 = ' est le plus grand, c\'est le ' + compteurRes9 + 'è nombre renseigné';
        $('#resultat5_9_jq').html("Vous avez fini. " + iNombreStocke9 + message9);
    }
}

                            //EXERCICE 10

function s5_exo10_js(){

    var iPrixArticle = parseInt(document.getElementById('iPrixArticle').value);
    var iTotal = parseInt(document.getElementById('iTotal').value);
    var iPaiement = parseInt(document.getElementById('iPaiement').value);
    var iBillet_10 = 0;
    var iBillet_5 = 0;
    var iPiece = 0;
    var iDiff;
    //Recup prix de chaque article + somme dans iTotal
    if(iPrixArticle != 0){
        iTotal += iPrixArticle;
        //injecte iTotal dans input paiement
        document.getElementById('iTotal').value = iTotal;
        document.getElementById('resultat5_10_js').innerHTML = 'Total à payer : ' + iTotal
    }
    else //Quand utilisateur entre 0
    {
        document.getElementById('iPaiement').value = iTotal;
        document.getElementById('iPaiement').type = 'number'; // affiche input paiement
        document.getElementById('iPaiement').innerHTML = "Vous devez régler: " + iTotal; // affiche contenu message  de input paiement
    }
    //Si id paiement != "" c'est que l'utilisateur à entrer tous ses produits
    if(iPaiement != 0)
    {
        iDiff = iPaiement - iTotal;
        document.getElementById('resultat5_10_js').innerHTML = "Je vous dois : " + iDiff + '</br>';
        while(iDiff >= 10)
        {
            iBillet_10 += 1;
            iDiff -= 10;
        }
        if(iDiff >= 5)
        {
            iBillet_5 = 1;
            iDiff -= 5;
        }
        iPiece = iDiff;
        document.getElementById('resultat5_10_js').innerHTML += " Votre monnaie comprend " + iBillet_10 + ' billets de 10€, ' + iBillet_5 + ' billets de 5€ et ' + iPiece + 'pièces de 1€';
    }


}

function s5_exo10_jq(){
    var iPrixArticle = parseInt($('#iPrixArticle_jq').val());
    var iTotal = parseInt($('#iTotal_jq').val());
    var iPaiement = parseInt($('#iPaiement_jq').val());
    var iBillet_10 = 0;
    var iBillet_5 = 0;
    var iPiece = 0;
    var iDiff;
    //Recup prix de chaque article + somme dans iTotal
    if(iPrixArticle != 0){

        iTotal += iPrixArticle;
        //injecte iTotal dans input paiement
        $('#iTotal_jq').val(iTotal);

        $('#resultat5_10_jq').html('Total à payer : ' + iTotal);
    }
    else //Quand utilisateur entre 0
    {
        $('#iPaiement_jq').val(iTotal);
        $('#iPaiement_jq').prop('type', 'number'); // affiche input paiement
        $('#iPaiement_jq').html("Vous devez régler: " + iTotal); // affiche contenu message  de input paiement
    }
    //Si id paiement != "" c'est que l'utilisateur à entrer tous ses produits
    if(iPaiement != 0)
    {
        iDiff = iPaiement - iTotal;
        $('#resultat5_10_jq').html("Je vous dois : " + iDiff + '</br>');
        while(iDiff >= 10)
        {
            iBillet_10 += 1;
            iDiff -= 10;
        }
        console.log(iDiff, 'apres while');
        if(iDiff >= 5)
        {
            iBillet_5 = 1;
            iDiff -= 5;
        }
        iPiece = iDiff;
        $('#resultat5_10_jq').append("Votre monnaie comprend " + iBillet_10 + ' billets de 10€, ' + iBillet_5 + ' billets de 5€ et ' + iPiece + 'pièces de 1€');
    }
}
function inputModifier10(){
    console.log('Affichage input iPaiment_php');
    $('#iPaiement_php').prop('type', 'number');
}
                            //EXERCICE11                                                                   

function s5_exo11_js(){
    var nF = 1; 
    var pF = 1; 
    var calc; 
    var diffF = 1;

    var n = parseInt(document.getElementById('cPartant').value);
    //Calcul factoriel n
    for(var i=1; i<=n; i++){
        nF *= i;
        
    } 

    var p = parseInt(document.getElementById('cJoue').value);
    //Calcul factoriel p
    for(var y=1; y <=p; y++){
        pF *= y; 
        
    }

    //Calcul factoriel n-p
    calc = n-p;
    for(var z = 1; z <= calc; z++){
        diffF *= z;
    }
    console.log(diffF, 'diff');

    var X = parseInt(nF / diffF);
    console.log(X, 'X');
    var Y = parseInt(nF / (pF * diffF));
    console.log(Y, 'Y');
    document.getElementById('ordre').innerHTML = 'Dans l\'ordre : une chance sur ' + X + ' de gagner';
    document.getElementById('ordre').innerHTML += '</br>Dans le désordre : une chance sur ' + Y + ' de gagner';
}

function s5_exo11_jq(){
    var nF = 1; 
    var pF = 1; 
    var calc; 
    var diffF = 1;
    var n = $('#cPartant_jq').val();

     //Calcul factoriel n
    console.log(n);
    for(var i=1; i<=n; i++){
        nF *= i;
    }
    console.log(nF, 'nf');

    //Calcul factoriel p
    var p = $('#cJoue_jq').val();
    console.log(p);
    for(var y=1; y <=p; y++){
        pF *= y; 
        
    }
    console.log(pF, 'pf');

    //Calcul factoriel n-p
    calc = n-p;
    for(var z =1; z <= calc; z++){
        diffF *= z;
    }
    console.log(diffF, 'diff');
    

    var X = nF / diffF;
    var Y = nF / (pF * diffF);
    $('#ordre_jq').html('Dans l\'ordre : une chance sur ' + X + ' de gagner');
    $('#ordre_jq').append('</br>Dans le désordre : une chance sur ' + Y + ' de gagner');
}

// ********************************* AFFICHAGE CODE EXO 1 ******************************

function afficheCode5_1js(){
    $("#showImage").html(`
    function s5_exo1js(){
        var iNb5_1 = document.getElementById('iNb5_1js').value;
        if( iNb5_1 < 1 || iNb5_1 > 3 )
        {
            document.getElementById("resultat5_1js").innerHTML = 'Veuillez recommencer s.v.p';
        }
        else
        {
            document.getElementById("resultat5_1js").innerHTML = 'Saisie acceptée';
        }
    }
    `)
};
function afficheCode5_1jq(){
    $("#showImage").html(`
    function s5_exo1js(){
        var iNb5_1 = document.getElementById('iNb5_1js').value;
        if( iNb5_1 < 1 || iNb5_1 > 3 )
        {
            document.getElementById("resultat5_1js").innerHTML = 'Veuillez recommencer s.v.p';
        }
        else
        {
            document.getElementById("resultat5_1js").innerHTML = 'Saisie acceptée';
        }
    }
    `)
};


function afficheCode1php(){
    $("#showImage").html(`

    $message = "";

    if(
       isset($_POST["iNb5_1php"])
    )
    {
       $iNb5_1 = $_POST["iNb5_1php"];
       if( $iNb5_1 < 1 || $iNb5_1 > 3 )
       {
          $message = 'Veuillez recommencer s.v.p';
       }
       else
       {
          $message = 'Saisie acceptée';
       }
       require 's5exercice1.html';
    }else
    {
       require 's5exercice1.html';
    }

    `)
};
// ********************************* AFFICHAGE CODE EXO 2 ******************************

function afficheCode5_2_ajs(){
    $("#showImage").html(`
    function s5_exo2_ajs(){
        //Recup + lecture entrée utilisateur
        var iNb5_2 = document.getElementById('iNb5_2_ajs').value;
        var result = "";
    
        if ( iNb5_2 < 10 )
        {
            result = 'Plus grand';
        }
        else if (iNb5_2 > 20)
        { 
            result = 'Plus petit';
        }
        else
        {
            result = 'Bien joué !'
        }
        document.getElementById("resultat5_2_ajs").innerHTML = result;
    }
    `)
};
function afficheCode5_2ajq(){
    $("#showImage").html(`
    function s5_exo2_ajq(){
        //Recup + lecture entrée utilisateur
        var iNb5_2 = $('#iNb5_2_ajq').val();
        var result = "";
    
        if ( iNb5_2 < 10 )
        {
            result = 'Plus grand';
        }
        else if ( iNb5_2 > 20 )
        { 
            result = 'Plus petit';
        }
        else
        {
            result = 'Bien joué !';
        }
        $("#resultat5_2_ajq").html(result);
    }
    `)
};

function afficheCode5_2a_php(){
    $("#showImage").html(`

    $message_a = "";

    if(isset($_POST['iNb5_2_aphp']))
    {
        $iNb5_2 = intval($_POST['iNb5_2_aphp']);
        if ( $iNb5_2 < 10 )
        {
            $message_a = "Plus grand !";
        }
        else if($iNb5_2 > 20)
        {
            $message_a = "Plus petit !";
        }
        else
        {
            $message_a = "Bien joué !";
        }
        require 's5exercice2a.html';
    }
    else 
    {
        require 's5exercice2a.html';
    }

    `)
};
function afficheCode5_2_bjs(){
    $("#showImage").html(`
    function s5_exo2_bjs(){
        var iNbUtilisateur = parseInt(document.getElementById("iNbUtilisateur").value);
        console.log(iNbUtilisateur, 'iNbUtilisateur');
    
        var iNbRandom = document.getElementById("iNbRandom").value;
        console.log(iNbRandom, 'iNbRandom');
    
        var iNbRandomNotExist = (iNbRandom == "");
        var cpt = document.getElementById("compteur").value;
        
        console.log(cpt, 'compteur');
        if(iNbRandomNotExist){
            cpt++;
            iNbRandom = Math.floor((Math.random() * 100) + 1);
            document.getElementById("iNbRandom").value = iNbRandom;
            console.log(iNbRandom, 'iNbRandom generated');
        }
    
        if(iNbRandom < iNbUtilisateur)
        {
            document.getElementById("resultat5_2_bjs").innerHTML = 'Plus petit, réessaie !';
            document.getElementById("compteur").value = ++cpt;
        }
        else if(iNbRandom > iNbUtilisateur)
        {
            document.getElementById("resultat5_2_bjs").innerHTML = 'Plus grand, réessaie !';
            document.getElementById("compteur").value = ++cpt;
        }
        else if(iNbRandom == iNbUtilisateur)
        {
            document.getElementById("resultat5_2_bjs").innerHTML = 'Bien joué ! Tu as réussi en ' + cpt + ' coups';
        }
        
    }
    `)
};

function afficheCode5_2bjq(){
    $("#showImage").html(`
    function s5_exo2_bjq(){
        var iNbUtilisateur_jq = $("#iNbUtilisateur_jq").val();
        console.log(iNbUtilisateur_jq, 'iNbUtilisateur_jq');
    
        var iNbRandom_jq = $("#iNbRandom_jq").val();
        console.log(iNbRandom_jq, 'iNbRandom_jq');
    
        var iNbRandomNotExist_jq = (iNbRandom_jq == "");
        var cpt = $("#compteur8_jq").val();
    
        cpt++;
        $("#compteur8_jq").val(cpt);
        console.log(cpt);
    
        if(iNbRandomNotExist_jq){
           
            iNbRandom_jq = Math.floor((Math.random() * 100) + 1);
            $("#iNbRandom_jq").val(iNbRandom_jq);
            $('#compteur8_jq').val(cpt);
            console.log(iNbRandom_jq, 'iNbRandom_jq generated');
        }
        
        if(iNbRandom_jq < iNbUtilisateur_jq)
        {
            $("#resultat5_2_bjq").html('Plus petit, réessaie !');
        }
        else if(iNbRandom_jq > iNbUtilisateur_jq)
        {
            $("#resultat5_2_bjq").html('Plus grand, réessaie !');
    
        }
        else if(iNbRandom_jq == iNbUtilisateur_jq)
        {
            $("#resultat5_2_bjq").html('Bien joué ! Tu as réussi en ' + cpt + 'coups');
        }
    
    }
    `)
};

function afficheCode5_2b_php(){
    $("#showImage").html(`
    $message_b = "";
    $cpt_php = 0;

    if(
        isset($_POST['iNbUtilisateur_php']) && isset($_POST['iNbRandom_php']) && isset($_POST['compteur_php'])
     )
    {
        $iNbUtilisateur_php = $_POST['iNbUtilisateur_php'];
        $iNbRandom_php = $_POST['iNbRandom_php'];
        $cpt_php = $_POST['compteur_php'];
        $cpt_php++;

        if ($iNbRandom_php == "")
        {
            $iNbRandom_php = random_int(1,100);

        }
        

        if ( $iNbUtilisateur_php < $iNbRandom_php )
        {
            $message_b = "Plus grand ! Réessaie";
        }
        else if($iNbUtilisateur_php > $iNbRandom_php)
        {
            $message_b = "Plus petit !  Réessaie";
        }
        else
        {
            $message_b = "Bien joué ! Tu as réussi en " . $cpt_php . ' coups !';
        }
        require 's5exercice2b.html';
    }
    else 
    {
        require 's5exercice2b.html';
    }
    `)
};

// ********************************* AFFICHAGE CODE EXO 3 ******************************



function afficheCode5_3_js(){
    $("#showImage").html(`
    function s5_exo3_js(){
        var sRep = "";
        var iNbr3 = parseInt(document.getElementById('iNbr3').value); //Entrée utilisateur
        var iMax = iNbr3 + 10;
        
        while(iNbr3 < iMax){
            iNbr3++ // augmente la valeur de iNombre de 1
            sRep += document.getElementById('resultat5_3_js').innerHTML += iNbr3 + ' ' ;//Affiche la valeur de iNombre
        }
    }
    `)
};

function afficheCode5_3_jq(){
    $("#showImage").html(`
    function s5_exo3_jq(){
        var sRepjq ="";
        var iNbr3jq = parseInt($('#iNbr3_jq').val()); //Entrée utilisateur
        for(i=0; i<10; i++){
            iNbr3jq++ // augmente la valeur de iNombre de 1
            sRepjq += $('#resultat5_3_jq').append(iNbr3jq + ' ');//Affiche la valeur de iNombre
        }
    }
    `)
};

function afficheCode5_3_php(){
    $("#showImage").html(`
    $message3 = "";
    if(
        isset($_POST['iNb3_php'])
    )
    {
        
        $iNbr3 = $_POST['iNb3_php'];
        $max = $iNbr3 + 10;


        while($iNbr3 < $max)
        {
            $iNbr3++;
            $message3 .= $iNbr3 . ' ';

        }
        require 's5exercice3.html';
    }
    else
    {
        require 's5exercice3.html';
    }

    `)
};

// ********************************* AFFICHAGE CODE EXO 4 ******************************
function afficheCode5_4_js(){
    $("#showImage").html(`
    function s5_exo4_js(){
        var sRep ="";
        var iNbr4 = document.getElementById('iNbr4').value; //Entrée utilisateur
        for(i=0; i<10; i++){
            iNbr4++ // augmente la valeur de iNombre de 1
            sRep += document.getElementById('resultat5_4_js').innerHTML += iNbr4 + ' ' ;//Affiche la valeur de iNombre
        }
    }
   
    `)
};

function afficheCode5_4_jq(){
    $("#showImage").html(`
    function s5_exo4_jq(){
        var sRep ="";
        var iNbr4 = $('#iNbr4_jq').val(); //Entrée utilisateur
        for(i=0; i<10; i++){
            iNbr4++ // augmente la valeur de iNombre de 1
            sRep += $('#resultat5_4_jq').append(iNbr4 + ' ') ;//Affiche la valeur de iNombre
        }
    }
    `)
};

function afficheCode5_4_php(){
    $("#showImage").html(`
    $message4 = "";
    if(
        isset($_POST['iNbr4_php'])
    )
    {
        
        $iNbr4 = $_POST['iNbr4_php'];
        


        for($i=0; $i < 10; $i++)
        {
            $message4 .= ++$iNbr4 . ' ';
        }
        require 's5exercice4.html';
    }
    else
    {
        require 's5exercice4.html';
    }

    `)
};

// ********************************* AFFICHAGE CODE EXO 5 ******************************

function afficheCode5_5_js(){
    $("#showImage").html(`
    function s5_exo5_js(){
        var sRep ="";
        var multipliant;
        var iNbr5 = document.getElementById('iNbr5').value; //Entrée utilisateur
        for(multipliant=1; multipliant<11; multipliant++){
            sRep += document.getElementById('resultat5_5_js').innerHTML += iNbr5 + 'x' + multipliant + '</br>' ;//Affiche la valeur de iNombre
        }
    }
    `)
};

function afficheCode5_5_jq(){
    $("#showImage").html(`
    function s5_exo5_jq(){
        var sRep ="";
        var multipliant;
        var iNbr5 = $('#iNbr5_jq').val(); //Entrée utilisateur
        for(multipliant=1; multipliant<11; multipliant++){
            sRep += $('#resultat5_5_jq').append(iNbr5 + 'x' + multipliant + '</br>') ;//Affiche la valeur de iNombre
        }
    }
    `)
};

function afficheCode5_5_php(){
    $("#showImage").html(`
    $message5 = "";
    if(
        isset($_POST['iNbr5_php'])
    )
    {

        $multipliant;
        $iNbr5 = intval($_POST[('iNbr5_php')]); 

        for($multipliant=1; $multipliant<11; $multipliant++){
            $message5 .= $iNbr5 . 'x' . $multipliant . '</br>' ;
        }
        require 's5exercice5.html';
    }
    else
    {
        require 's5exercice5.html';
    }
    `)
};

// ********************************* AFFICHAGE CODE EXO 6 ******************************

function afficheCode5_6_js(){
    $("#showImage").html(`
    function s5_exo6_js(){
        var sRes = 0;
        var iNbr6 = parseInt(document.getElementById('iNbr6').value); //Entrée utilisateur
        var iNbrOriginal = iNbr6 ;
        
        for(i=1; i<=iNbr6; i++){
            sRes += i;
        }
        document.getElementById("resultat5_6_js").innerHTML =  'La somme de tous les entiers de ' + iNbrOriginal + ' est : ' + sRes;
    }
    `)
};

function afficheCode5_6_jq(){
    $("#showImage").html(`
    function s5_exo6_jq(){
        var sRes = 0;
        var iNbr6 = parseInt($('#iNbr6_jq').val()); //Entrée utilisateur
        var iNbrOriginal = iNbr6 ;
    
        for(i=1; i<=iNbr6; i++){
            sRes += i;
        }
        $("#resultat5_6_jq").html('La somme de tous les entiers de ' + iNbrOriginal + ' est : ' + sRes);
    }
    `)
};

function afficheCode5_6_php(){
    $("#showImage").html(`
    $message6 = "";
    $iRep = 0;
    if(
        isset($_POST['iNbr6_php'])
    )
    {
        $iNbr6 = intval($_POST[('iNbr6_php')]); 
        $iNbrOriginal = $iNbr6;

        for($i=1; $i<=$iNbr6; $i++){
            $iRep += $i; 
            $message6 = 'La somme de tous les entiers de ' .$iNbrOriginal. ' est : ' . $iRep;
        }
        require 's5exercice6.html';
    }
    else
    {
        require 's5exercice6.html';
    }

    `)
};

// ********************************* AFFICHAGE CODE EXO 7 ******************************

function afficheCode5_7_js(){
    $("#showImage").html(`
    function s5_exo7_js(){
        var sRes = 1;
        var iNbr7 = parseInt(document.getElementById('iNbr7').value); //Entrée utilisateur
        var iNbrOriginal = iNbr7 ;
        
        for(i=1; i<=iNbr7; i++){
            sRes *= i;
        }
        document.getElementById("resultat5_7_js").innerHTML =  'La factorielle de ' + iNbrOriginal + ' est : ' + sRes;
    }
    `)
};

function afficheCode5_7_jq(){
    $("#showImage").html(`
    function s5_exo7_jq(){
        var sRes = 1;
        var iNbr7 = parseInt($('#iNbr7_jq').val()); //Entrée utilisateur
        var iNbrOriginal = iNbr7 ;
    
        for(i=1; i<=iNbr7; i++){
            sRes *= i;
        }
        $("#resultat5_7_jq").html('La factorielle de ' + iNbrOriginal + ' est : ' + sRes);
    }
    `)
};

function afficheCode5_7_php(){
    $("#showImage").html(`
    $message7 = "";
    $iRep = 0;
    if(
        isset($_POST['iNbr7_php'])
    )
    {
        $iNbr7 = intval($_POST[('iNbr7_php')]); 
        $iNbrOriginal = $iNbr7;
        $iRep = 1;
        for($i=1; $i<=$iNbr7; $i++){
            $iRep *= $i; 
            $message7 = 'La factorielle de ' .$iNbrOriginal. ' est : ' . $iRep;
        }
        require 's5exercice7.html';
    }
    else
    {
        require 's5exercice7.html';
    }

    `)
};

// ********************************* AFFICHAGE CODE EXO 8 ******************************

function afficheCode5_8_js(){
    $("#showImage").html(`
    function s5_exo8_js(){

        var iNombreRecup = parseInt(document.getElementById('iNombreRecup').value);
        var iNombreStocke = document.getElementById('iNombreStocke').value;
        var compteur8 = parseInt(document.getElementById('compteur8').value);
        var conditionStock = (iNombreStocke < iNombreRecup);
        var compteurRes = document.getElementById('compteurRes').value;
        compteur8++; 
        document.getElementById("compteur8").value = compteur8;
        
    
        if(iNombreStocke == "" || conditionStock){
            iNombreStocke = iNombreRecup;
            document.getElementById("iNombreStocke").value = iNombreStocke;
            compteurRes = compteur8;
            document.getElementById("compteurRes").value = compteurRes;
        }
        else if(compteur8 == 10 || iNombreRecup == 0 ){
            var message = ' est le plus grand, c\'est le ' + compteurRes + 'è nombre renseigné';
            document.getElementById('resultat5_8_js').innerHTML = "Vous avez fini. " + iNombreStocke + message;
        }
    }
    `)
};

function afficheCode5_8_jq(){
    $("#showImage").html(`
    function s5_exo8_jq(){
        var iNombreRecup = parseInt($('#iNombreRecup_jq').val());
        var iNombreStocke = $('#iNombreStocke_jq').val();
        var compteur8 = parseInt($('#compteur8_jq').val());
        var conditionStock = (iNombreStocke < iNombreRecup);
        var compteurRes = $('#compteurRes_jq').val();
        compteur8++; 
        $("#compteur8_jq").val(compteur8);
        
        if(iNombreStocke == "" || conditionStock){
            iNombreStocke = iNombreRecup;
            $("#iNombreStocke_jq").val(iNombreStocke);
            compteurRes = compteur8;
            $("#compteurRes_jq").val(compteurRes);
        }
        else if(compteur8 == 10 || iNombreRecup == 0 ){
            var message = ' est le plus grand, c\'est le ' + compteurRes + 'è nombre renseigné';
            $('#resultat5_8_jq').html("Vous avez fini. " + iNombreStocke + message);
        }
    }
    `)
};

function afficheCode5_8_php(){
    $("#showImage").html(`
    $message8 = "";
    $iNombreRecup = 0;
    $iNombreStocke = 0;
    $compteur8 = 0;
    $compteurRes = 0;
    if(
        isset($_POST['iNombreRecup_php']) && isset($_POST['iNombreStocke_php']) && isset($_POST['compteur8_php']) && isset($_POST['compteurRes_php'])
    )
    {
        $iNombreRecup = intval($_POST['iNombreRecup_php']);
        $iNombreStocke = $_POST['iNombreStocke_php'];
        $compteur8 = intval($_POST['compteur8_php']);
       
        $compteurRes = $_POST['compteurRes_php'];
        $conditionStock = ($iNombreStocke < $iNombreRecup);
        $compteur8++; 

        if($iNombreStocke == "" || $conditionStock){
            $iNombreStocke = $iNombreRecup;
            $compteurRes = $compteur8;
        }
        else if($compteur8 == 10 || $iNombreRecup == 0 ){
            $message8 = 'Vous avez fini. ' . $iNombreStocke .' est le plus grand, c\'est le ' . $compteurRes . 'è nombre renseigné';

        }
        require "s5exercice8.html";
    }
    else
    {
        require 's5exercice8.html';
    }
    `)
};

// ********************************* AFFICHAGE CODE EXO 9 ******************************

function afficheCode5_9_js(){
    $("#showImage").html(`
    function s5_exo9_js(){

        var iNombreRecup9 = parseInt(document.getElementById('iNombreRecup9').value);
        var iNombreStocke9 = document.getElementById('iNombreStocke9').value;
        var compteur9 = parseInt(document.getElementById('compteur9').value);
        var conditionStock9 = (iNombreStocke9 < iNombreRecup9);
        var compteurRes9 = document.getElementById('compteurRes9').value;
        compteur9++; 
        document.getElementById("compteur9").value = compteur9;
        if(iNombreStocke9 == "" || conditionStock9){
            iNombreStocke9 = iNombreRecup9;
            document.getElementById("iNombreStocke9").value = iNombreStocke9;
            compteurRes9 = compteur9;
            document.getElementById("compteurRes9").value = compteurRes9;
      
        }
        else if(iNombreRecup9 == 0 ){
            var message9 = ' est le plus grand, c\'est le ' + compteurRes9 + 'è nombre renseigné';
            document.getElementById('resultat5_9_js').innerHTML = "Vous avez fini. " + iNombreStocke9 + message9;
        }
    }
    `)
};

function afficheCode5_9_jq(){
    $("#showImage").html(`
    function s5_exo9_jq(){
        var iNombreRecup9 = parseInt($('#iNombreRecup9_jq').val()); 
        var iNombreStocke9 = parseInt($('#iNombreStocke9_jq').val());
        var compteur9 = parseInt($('#compteur9_jq').val());
        var conditionStock9 = (iNombreStocke9 < iNombreRecup9);
        var compteurRes9 = $('#compteurRes9_jq').val();
        compteur9++; 
    
        $("#compteur9_jq").val(compteur9);
        console.log(iNombreRecup9);
        console.log(iNombreStocke9);
    
        if(iNombreStocke9 == 0 || conditionStock9){
            console.log('je passe par la');
            iNombreStocke9 = iNombreRecup9;
            $("#iNombreStocke9_jq").val(iNombreStocke9);
            compteurRes9 = compteur9;
            $("#compteurRes9_jq").val(compteurRes9); 
            console.log(iNombreRecup9);
        }
        else if(iNombreRecup9 == 0){
            console.log('hello c\'est zéro!');
            var message9 = ' est le plus grand, c\'est le ' + compteurRes9 + 'è nombre renseigné';
            $('#resultat5_9_jq').html("Vous avez fini. " + iNombreStocke9 + message9);
        }
    }
    `)
};

function afficheCode5_9_php(){
    $("#showImage").html(`
    $message9 = "";
    $iNombreRecup9 = 0;
    $iNombreStocke9 = 0;
    $compteur9 = 0;
    $compteurRes9 = 0;

    if(
        isset($_POST['iNombreRecup9_php']) && isset($_POST['iNombreStocke9_php']) && isset($_POST['compteur9_php']) && isset($_POST['compteurRes9_php'])
    )
    {
        $iNombreRecup9 = intval($_POST['iNombreRecup9_php']);
        $iNombreStocke9 = $_POST['iNombreStocke9_php'];
        $compteur9 = intval($_POST['compteur9_php']);
       
        $compteurRes9 = $_POST['compteurRes9_php'];
        $conditionStock = ($iNombreStocke9 < $iNombreRecup9);
        $compteur9++; 

        if($iNombreStocke9 == "" || $conditionStock){
            $iNombreStocke9 = $iNombreRecup9;
            $compteurRes9 = $compteur9;
        }
        else if($iNombreRecup9 == 0){
            $message9 = 'Vous avez fini. ' . $iNombreStocke9 .' est le plus grand, c\'est le ' . $compteurRes9 . 'è nombre renseigné';

        }
        require "s5exercice9.html";
    }
    else
    {
        require 's5exercice9.html';
    }
    `)
};

// ********************************* AFFICHAGE CODE EXO 10 ******************************

function afficheCode5_10_js(){
    $("#showImage").html(`
    function s5_exo10_js(){

        var iPrixArticle = parseInt(document.getElementById('iPrixArticle').value);
        var iTotal = parseInt(document.getElementById('iTotal').value);
        var iPaiement = parseInt(document.getElementById('iPaiement').value);
        var iBillet_10 = 0;
        var iBillet_5 = 0;
        var iPiece = 0;
        var iDiff;
        //Recup prix de chaque article + somme dans iTotal
        if(iPrixArticle != 0){
            iTotal += iPrixArticle;
            //injecte iTotal dans input paiement
            document.getElementById('iTotal').value = iTotal;
            document.getElementById('resultat5_10_js').innerHTML = 'Total à payer : ' + iTotal
        }
        else //Quand utilisateur entre 0
        {
            document.getElementById('iPaiement').value = iTotal;
            document.getElementById('iPaiement').type = 'number'; // affiche input paiement
            document.getElementById('iPaiement').innerHTML = "Vous devez régler: " + iTotal; // affiche contenu message  de input paiement
        }
        //Si id paiement != "" c'est que l'utilisateur à entrer tous ses produits
        if(iPaiement != 0)
        {
            iDiff = iPaiement - iTotal;
            document.getElementById('resultat5_10_js').innerHTML = "Je vous dois : " + iDiff + '</br>';
            while(iDiff >= 10)
            {
                iBillet_10 += 1;
                iDiff -= 10;
            }
            if(iDiff >= 5)
            {
                iBillet_5 = 1;
                iDiff -= 5;
            }
            iPiece = iDiff;
            document.getElementById('resultat5_10_js').innerHTML += " Votre monnaie comprend " + iBillet_10 + ' billets de 10€, ' + iBillet_5 + ' billets de 5€ et ' + iPiece + 'pièces de 1€';
        }
    
    
    }
    `)
};

function afficheCode5_10_jq(){
    $("#showImage").html(`
    function s5_exo10_jq(){
        var iPrixArticle = parseInt($('#iPrixArticle_jq').val());
        var iTotal = parseInt($('#iTotal_jq').val());
        var iPaiement = parseInt($('#iPaiement_jq').val());
        var iBillet_10 = 0;
        var iBillet_5 = 0;
        var iPiece = 0;
        var iDiff;
        //Recup prix de chaque article + somme dans iTotal
        if(iPrixArticle != 0){
    
            iTotal += iPrixArticle;
            //injecte iTotal dans input paiement
            $('#iTotal_jq').val(iTotal);
    
            $('#resultat5_10_jq').html('Total à payer : ' + iTotal);
        }
        else //Quand utilisateur entre 0
        {
            $('#iPaiement_jq').val(iTotal);
            $('#iPaiement_jq').prop('type', 'number'); // affiche input paiement
            $('#iPaiement_jq').html("Vous devez régler: " + iTotal); // affiche contenu message  de input paiement
        }
        //Si id paiement != "" c'est que l'utilisateur à entrer tous ses produits
        if(iPaiement != 0)
        {
            iDiff = iPaiement - iTotal;
            $('#resultat5_10_jq').html("Je vous dois : " + iDiff + '</br>');
            while(iDiff >= 10)
            {
                iBillet_10 += 1;
                iDiff -= 10;
            }
            console.log(iDiff, 'apres while');
            if(iDiff >= 5)
            {
                iBillet_5 = 1;
                iDiff -= 5;
            }
            iPiece = iDiff;
            $('#resultat5_10_jq').append("Votre monnaie comprend " + iBillet_10 + ' billets de 10€, ' + iBillet_5 + ' billets de 5€ et ' + iPiece + 'pièces de 1€');
        }
    }
    `)
};

function afficheCode5_10_php(){
    $("#showImage").html(`
    $sTotal = "";
    $sMonnaie = "";
    $iPaiement = 0;
    $iTotal = 0;
    if(
        isset($_POST['iPrixArticle_php']) && isset($_POST['iTotal_php']) && isset($_POST['iPaiement_php'])
    )
    {
        $iPrixArticle = intval($_POST['iPrixArticle_php']);
        $iTotal = intval($_POST['iTotal_php']);
        $iPaiement = intval($_POST['iPaiement_php']);
        $iBillet_10;
        $iBillet_5;
        $iPiece;
        $iDiff;
        
        if($iPrixArticle != 0){
            $iTotal += $iPrixArticle;
            $sTotal = 'Vous me devez ' . $iTotal;
        }
        else //Quand utilisateur entre 0
        {
            $sTotal = 'Vous me devez ' . $iTotal;
            var_dump($iPaiement . "ce qu'il y a dans iPaiement_php");
        }
        //Si id paiement != "" c'est que l'utilisateur à entrer tous ses produits
        if($iPaiement != 0)
        {
            $iDiff = $iPaiement - $iTotal;
            $sMonnaie = 'Je vous dois : ' . $iDiff . '</br>';
            while($iDiff >= 10)
            {
                $iBillet_10 += 1;
                $iDiff -= 10;
            }
            if($iDiff >= 5)
            {
            $iBillet_5 = 1;
                $iDiff -= 5;
            }
            $iPiece = $iDiff;
            $sMonnaie .= "Votre monnaie comprend " . $iBillet_10 . ' billets de 10€, ' . $iBillet_5 . ' billets de 5€ et ' . $iPiece . 'pièces de 1€';
        }
        require "s5exercice10.html";
    }
    else
    {
        require 's5exercice10.html';
    }
    
    `)
};

// ********************************* AFFICHAGE CODE EXO 11 ******************************

function afficheCode5_11_js(){
    $("#showImage").html(`
    function s5_exo11_js(){
        var nF = 1; 
        var pF = 1; 
        var calc; 
        var diffF = 1;
    
        var n = parseInt(document.getElementById('cPartant').value);
        //Calcul factoriel n
        for(var i=1; i<=n; i++){
            nF *= i;
            
        } 
    
        var p = parseInt(document.getElementById('cJoue').value);
        //Calcul factoriel p
        for(var y=1; y <=p; y++){
            pF *= y; 
            
        }
    
        //Calcul factoriel n-p
        calc = n-p;
        for(var z = 1; z <= calc; z++){
            diffF *= z;
        }
        console.log(diffF, 'diff');
    
        var X = parseInt(nF / diffF);
        console.log(X, 'X');
        var Y = parseInt(nF / (pF * diffF));
        console.log(Y, 'Y');
        document.getElementById('ordre').innerHTML = 'Dans l\'ordre : une chance sur ' + X + ' de gagner';
        document.getElementById('ordre').innerHTML += '</br>Dans le désordre : une chance sur ' + Y + ' de gagner';
    }
    `)
};

function afficheCode5_11_jq(){
    $("#showImage").html(`
    function s5_exo11_jq(){
        var nF = 1; 
        var pF = 1; 
        var calc; 
        var diffF = 1;
        var n = $('#cPartant_jq').val();
    
         //Calcul factoriel n
        console.log(n);
        for(var i=1; i<=n; i++){
            nF *= i;
        }
        console.log(nF, 'nf');
    
        //Calcul factoriel p
        var p = $('#cJoue_jq').val();
        console.log(p);
        for(var y=1; y <=p; y++){
            pF *= y; 
            
        }
        console.log(pF, 'pf');
    
        //Calcul factoriel n-p
        calc = n-p;
        for(var z =1; z <= calc; z++){
            diffF *= z;
        }
        console.log(diffF, 'diff');
        
    
        var X = nF / diffF;
        var Y = nF / (pF * diffF);
        $('#ordre_jq').html('Dans l\'ordre : une chance sur ' + X + ' de gagner');
        $('#ordre_jq').append('</br>Dans le désordre : une chance sur ' + Y + ' de gagner');
    }
    `)
};

function afficheCode5_11_php(){
    $("#showImage").html(`
    $sTotal = "";
    $sMonnaie = "";
    $iPaiement = 0;
    $iTotal = 0;
    if(
        isset($_POST['iPrixArticle_php']) && isset($_POST['iTotal_php']) && isset($_POST['iPaiement_php'])
    )
    {
        $iPrixArticle = intval($_POST['iPrixArticle_php']);
        $iTotal = intval($_POST['iTotal_php']);
        $iPaiement = intval($_POST['iPaiement_php']);
        $iBillet_10;
        $iBillet_5;
        $iPiece;
        $iDiff;
        
        if($iPrixArticle != 0){
            $iTotal += $iPrixArticle;
            $sTotal = 'Vous me devez ' . $iTotal;
        }
        else //Quand utilisateur entre 0
        {
            $sTotal = 'Vous me devez ' . $iTotal;
            var_dump($iPaiement . "ce qu'il y a dans iPaiement_php");
        }
        //Si id paiement != "" c'est que l'utilisateur à entrer tous ses produits
        if($iPaiement != 0)
        {
            $iDiff = $iPaiement - $iTotal;
            $sMonnaie = 'Je vous dois : ' . $iDiff . '</br>';
            while($iDiff >= 10)
            {
                $iBillet_10 += 1;
                $iDiff -= 10;
            }
            if($iDiff >= 5)
            {
               $iBillet_5 = 1;
                $iDiff -= 5;
            }
            $iPiece = $iDiff;
            $sMonnaie .= "Votre monnaie comprend " . $iBillet_10 . ' billets de 10€, ' . $iBillet_5 . ' billets de 5€ et ' . $iPiece . 'pièces de 1€';
        }
        require "s5exercice10.html";
    }
    else
    {
        require 's5exercice10.html';
    }

    `)
};