
function exo1(){
  console.log("Exo1 : je m'execute");
  var targetModal = document.getElementById("enonce");
  var targetCorpse = document.getElementById("aResoudre");
  var HTMLmodalCorpse = ""; //Variable pour envoyer dynamiquement le HTML
  var HTMLmodalHeader = "";
  HTMLmodalHeader += "Quelles seront les valeurs des variables A et B après éxécution des instructions suivantes ?";
  HTMLmodalCorpse +=
  `
  
  Variables A, B en Entier </br>
  Début </br>
  A <- 17;</br>
  B <- A +2;</br>
  A <- 9;</br>
  Fin </br>
  Réponse : A = 9 et B = 19.
 
  `;
  targetModal.innerHTML = HTMLmodalHeader;
  targetCorpse.innerHTML = HTMLmodalCorpse;
  console.log("GoodBye !")
}

function exo2(){
  var targetModal = document.getElementById("enonce");
  var targetCorpse = document.getElementById("aResoudre");
  var HTMLmodalCorpse = ""; //Variable pour envoyer dynamiquement le HTML
  var HTMLmodalHeader = "";
  HTMLmodalHeader += "Quelles seront les valeurs des variables A, B et C après exécution des instructions suivantes ?";
  HTMLmodalCorpse +=
  `
  
  Variables A, B, C en Entier </br>
  Début </br>
  A ← 51 </br>
  B ← 34 </br>
  C ← A + B </br>
  A ← 21 </br>
  C ← B – A </br>
  Fin</br>

  Reponse : A = 21 / B = 34 / C = 13
  
  `;
  targetModal.innerHTML = HTMLmodalHeader;
  targetCorpse.innerHTML = HTMLmodalCorpse;   
}

function exo3(){
   var targetModal = document.getElementById("enonce");
  var targetCorpse = document.getElementById("aResoudre");
  var HTMLmodalCorpse = ""; //Variable pour envoyer dynamiquement le HTML
  var HTMLmodalHeader = "";
  HTMLmodalHeader += "Quelles seront les valeurs des variables A, B après exécution des instructions suivantes ?";
  HTMLmodalCorpse +=
  `
  <p>
  Variables A, B en Entier </br>
  Début </br>
  A ← 49 </br>
  B ← A + 4 </br>
  A ← A + 1 </br>
  B <- A - 4 </br>
  Fin</br>

  Reponse : A = 50 / B = 46
  </p>
  `;
  targetModal.innerHTML = HTMLmodalHeader;
  targetCorpse.innerHTML = HTMLmodalCorpse;
}

function exo4(){
   var targetModal = document.getElementById("enonce");
  var targetCorpse = document.getElementById("aResoudre");
  var HTMLmodalCorpse = ""; //Variable pour envoyer dynamiquement le HTML
  var HTMLmodalHeader = "";
  HTMLmodalHeader += "Quelles seront les valeurs des variables A, B et C après exécution des instructions suivantes ?";
  HTMLmodalCorpse +=
  `
  <p>
  Variables A, B, C en Entier </br>
  Début </br>
  A ← 13 </br>
  B ← 19 </br>
  C ← A + B </br>
  B ← A + B </br>
  A ← C </br>
  Fin</br>

  Reponse : A = 32 / B = 32 / C = 32
  </p>
  `;
  targetModal.innerHTML = HTMLmodalHeader;
  targetCorpse.innerHTML = HTMLmodalCorpse;
}// Exercice 5


function exo5(){
  var targetModal = document.getElementById("enonce");
  var targetCorpse = document.getElementById("aResoudre");
  var HTMLmodalCorpse = ""; //Variable pour envoyer dynamiquement le HTML
  var HTMLmodalHeader = "";
  HTMLmodalHeader += "Quelles seront les valeurs des variables A, B après exécution des instructions suivantes ?";
  HTMLmodalCorpse +=
  `
  <p>
  Variables A, B en Entier </br>
  Début </br>
  A ← 14 </br>
  B ← 29 </br>
  A ← B </br>
  B ← A </br>
  Fin</br>

  Reponse : A = 29 / B = 29
  </p>
  `;
  targetModal.innerHTML = HTMLmodalHeader;
  targetCorpse.innerHTML = HTMLmodalCorpse;
}

function exo6(){
  var targetModal = document.getElementById("enonce");
  var targetCorpse = document.getElementById("aResoudre");
  var HTMLmodalCorpse = ""; //Variable pour envoyer dynamiquement le HTML
  var HTMLmodalHeader = "";
  HTMLmodalHeader += "Ecrire un pseudo-code permettant d’échanger les valeurs de deux variables A et B, et ce quel que soit leur contenu préalable.";
  HTMLmodalCorpse +=
  `
  <p>
    Variables A, B, Vtemp en Entier </br>
    Début </br>
    Avec un intermédiaire C:</br>
    A <- 30   B <- 25</br>
    Temp <- A; Temp = 30</br>
    A <- B; A = 25</br>
    B <- Temp; B = 30</br>
    Fin</br>
    </br>
    Sans intermédiaire :</br>
    Variables A, B en Entier </br>
    Début </br>
    A <- A + B : A= 55</br>
    B <- A - B : B= 30</br>
    A <- A - B : A= 25</br>
    Fin</br>
  </p>
  `;
  targetModal.innerHTML = HTMLmodalHeader;
  targetCorpse.innerHTML = HTMLmodalCorpse;
} 

function exo7(){
  var targetModal = document.getElementById("enonce");
  var targetCorpse = document.getElementById("aResoudre");
  var HTMLmodalCorpse = ""; //Variable pour envoyer dynamiquement le HTML
  var HTMLmodalHeader = "";
  HTMLmodalHeader += "Que produit l’algorithme suivant ?";
  HTMLmodalCorpse +=
  `
  <p>
  Variables A, B, C en Entier </br>
  Début </br>
  A ← "573" </br>
  B ← "19" </br>
  C ← A + B </br>
  Fin</br>

  Reponse : C = erreur en pseudocode ou "57319" dans certains languages.
  </p>
  `;
  targetModal.innerHTML = HTMLmodalHeader;
  targetCorpse.innerHTML = HTMLmodalCorpse;
}
function exo8(){
   var targetModal = document.getElementById("enonce");
  var targetCorpse = document.getElementById("aResoudre");
  var HTMLmodalCorpse = ""; //Variable pour envoyer dynamiquement le HTML
  var HTMLmodalHeader = "";
  HTMLmodalHeader += "Que produit l’algorithme suivant ?";
  HTMLmodalCorpse +=
  `
  <p>
  Variables A, B, C en Entier </br>
  Début </br>
  A ← "573" </br>
  B ← "19" </br>
  C ← A + B </br>
  Fin</br>

  Reponse : C = erreur en pseudocode ou "57319" dans certains languages.
  </p>
  `;
  targetModal.innerHTML = HTMLmodalHeader;
  targetCorpse.innerHTML = HTMLmodalCorpse;
}
function exo9(){
   var targetModal = document.getElementById("enonce");
  var targetCorpse = document.getElementById("aResoudre");
  var HTMLmodalCorpse = ""; //Variable pour envoyer dynamiquement le HTML
  var HTMLmodalHeader = "";
  HTMLmodalHeader += "Que produit l’algorithme suivant ?";
  HTMLmodalCorpse +=
  `
  <p>
  Variables A, B, C en Entier </br>
  Début </br>
  A ← "573" </br>
  B ← "19" </br>
  C ← A & B</br>
  A ← C </br>
  Fin</br>

  C = 17 si operation binaire sinon C = 579319 par concatenation.
  </p>
  `;
  targetModal.innerHTML = HTMLmodalHeader;
  targetCorpse.innerHTML = HTMLmodalCorpse; 
}