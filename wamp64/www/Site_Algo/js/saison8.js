// *********************************** EXERCICE 1 *****************************************
var aDamier = [];
var ligneRecup, colonneRecup;




function s8_exo1_js(){
    var aTab = new Array();
    var i, j;
    var sHTML = "";
    for(i=0 ; i < 6 ; i++)
    {
        aTab[i] = new Array();
        for(j=0 ; j < 12 ; j++)
        {
            aTab[i][j] = 0;
            sHTML += aTab[i][j] + ' ';
        }
        sHTML += "</br>";
    }
    document.getElementById('resultat8_1').innerHTML = sHTML;
}

function s8_exo1_jq(){
        var aTab = [];
        var i, j;
        var sHTML = "";
        for(i=0 ; i < 6 ; i++){
            aTab[i] = [];
            for(j=0 ; j < 12 ; j++){
                aTab[i][j] = 0;
                sHTML += aTab[i][j] + ' ';
            }
            sHTML += "</br>";
        }
        $('#resultat8_1_jq').html(sHTML);
}   






// *********************************** EXERCICE 2 *****************************************


function s8_exo2_js(){

    var aTab = [];
    var i, j;
    var iMax = 0;
    sTableau = "";
    for(i=0 ; i < 12 ; i++)
    {
        console.log('for 1');
        aTab[i] = []
        for(j=0; j < 8 ; j++)
        {
            console.log('for 2');

            aTab[i][j] = Math.floor((Math.random() * 100) + 1);
            //A la première itération et / ou si valeur récup > iMax on injecte la valeur à iMax
            if( (j == 0 && aTab[i][j] > iMax) || aTab[i][j] >  iMax)
            {
                iMax = aTab[i][j];
            }
            sTableau += aTab[i][j] + ' ';
        }
        sTableau += '</br>';
    }
    document.getElementById('resultat8_2').innerHTML = sTableau + '</br> </br> La plus grande valeur est : ' + iMax;
}



function s8_exo2_jq(){
    console.log('Hey');
    var aTab = [];
    var i, j;
    var iMax = 0;
    sTableau = "";
    for(i=0 ; i < 12 ; i++)
    {
        console.log('for 1');
        aTab[i] = []
        for(j=0; j < 8 ; j++)
        {
            console.log('for 2');

            aTab[i][j] = Math.floor((Math.random() * 100) + 1);
            //A la première itération ou si valeur récup > iMax
            if( (j == 0 && aTab[i][j] > iMax) || aTab[i][j] >  iMax)
            {
                iMax = aTab[i][j];
            }
            sTableau += aTab[i][j] + ' ';
        }
        sTableau += '</br>';
    }
    $('#resultat8_2_jq').html(sTableau + '</br> </br> La plus grande valeur est : ' + iMax);

}   





// *********************************** EXERCICE 3 *****************************************

function s8_exo3_js(){
    $('#btn_retry').hide();
    $('#btn_move').show();
    $('#resultatOUT_jq').hide();
    //Initialisation du Damier 8 par 8
        var i, j;
        var sHTML = "";
        for(i=0 ; i < 8 ; i++){
            aDamier[i] = [];
            for(j=0 ; j < 8 ; j++){
                aDamier[i][j] = '0';
                sHTML += aDamier[i][j] + ' ';
            }
            sHTML += "</br>";
        }
        document.getElementById('resultat8_3').innerHTML = sHTML;
}

function s8_exo3_pos_js(){
    var ligne = document.getElementById('ligne').value;
    var colonne = document.getElementById('colonne').value;
    ligneRecup = ligne;
    colonneRecup = colonne;
    console.log()
    //Ou se trouve le pion ?
    var i, j;
    var sHTML = "";
    for(i=0 ; i < 8 ; i++){
        aDamier[i] = [];
        for(j=0 ; j < 8 ; j++){
            aDamier[i][j] = '0';
            if(i == ligne && j == colonne )
            {
                aDamier[ligne][colonne] = 'X';
            }
            sHTML += aDamier[i][j] + ' ';
        }
        sHTML += "</br>";
    }
    document.getElementById('resultat8_3').innerHTML = sHTML;

}

function s8_exo3_move_js(){
    sHTML = "";
    sSortie = "OUT !";
    var move = document.getElementById('move').value;
    if(move == 0 && move >= 0 && move <= 7){
        if(ligneRecup >= 0 && colonneRecup >= 0 && ligneRecup <= 7 && colonneRecup <= 7){
            aDamier[ligneRecup][colonneRecup] = '0';
            aDamier[ligneRecup - 1][colonneRecup - 1] = 'X';
            ligneRecup--;
            colonneRecup--;
        }
        else
        {
            $('#resultatOUT_jq').show().html(sSortie);
            $('#btn_move').hide();
            $('#btn_retry').show();
        }
    }
    else if(move == 1 && move >= 0 && move <= 7){
        if(ligneRecup >= 0 && colonneRecup >= 0 && ligneRecup <= 7 && colonneRecup <= 7){
            aDamier[ligneRecup][colonneRecup] = '0';
            aDamier[ligneRecup - 1][colonneRecup + 1] = 'X';
            ligneRecup--;
            colonneRecup++;
        }
        else{
            $('#resultatOUT_jq').show().html(sSortie);
            $('#btn_move').hide();
            $('#btn_retry').show();
        }
    }
    else if(move == 2 && move >= 0 && move <= 7){
        if(ligneRecup >= 0 && colonneRecup >= 0 && ligneRecup <= 7 && colonneRecup <= 7){
            aDamier[ligneRecup][colonneRecup] = '0';
            aDamier[ligneRecup + 1][colonneRecup - 1] = 'X';
            ligneRecup++;
            colonneRecup--;
        }
        else{
            $('#resultatOUT_jq').show().html(sSortie);
            $('#btn_move').hide();
            $('#btn_retry').show();
        }
    }
    else if(move == 3 && move >= 0 && move <= 7){
        if(ligneRecup >= 0 && colonneRecup >= 0 && ligneRecup <= 7 && colonneRecup <= 7){
            aDamier[ligneRecup][colonneRecup] = '0';
            aDamier[ligneRecup + 1][colonneRecup + 1] = 'X';
            ligneRecup++;
            colonneRecup++;
        }
        else{
            $('#resultatOUT_jq').show().html(sSortie);
            $('#btn_move').hide();
            $('#btn_retry').show();
        }
    }
    //Actualisation du tableau
    for(i=0 ; i < 8 ; i++){
        for(j=0 ; j < 8 ; j++){
            sHTML += aDamier[i][j] + ' ';
        }
        sHTML += "</br>";
    }
    document.getElementById('resultat8_3').innerHTML = sHTML;
}





function s8_exo3_jq(){
    $('#btn_retry').hide();
    $('#btn_move').show();
    $('#resultatOUT_jq').hide();
    //Initialisation du Damier 8 par 8
        var i, j;
        var sHTML = "";
        for(i=0 ; i < 8 ; i++){
            aDamier[i] = [];
            for(j=0 ; j < 8 ; j++){
                aDamier[i][j] = '0';
                sHTML += aDamier[i][j] + ' ';
            }
            sHTML += "</br>";
        }
        $('#resultat8_3_jq').html(sHTML);
}

function s8_exo3_pos_jq(){
    //Ou se trouve le pion ?
    var ligne = +$('#ligne_jq').val();
    var colonne = +$('#colonne_jq').val();
    ligneRecup = ligne;
    colonneRecup = colonne;
    //Affectation de la position dans le tableau
    var i, j;
    var sHTML = "";
    for(i=0 ; i < 8 ; i++){
        aDamier[i] = [];
        for(j=0 ; j < 8 ; j++){
            aDamier[i][j] = '0';
            if(i == ligne && j == colonne )
            {
                aDamier[ligne][colonne] = 'X';
            }
            sHTML += aDamier[i][j] + ' ';
        }
        sHTML += "</br>";
    }
    //Affichage Tableau
    $('#resultat8_3_jq').html(sHTML);

}

function s8_exo3_move_jq(){
    sHTML = "";
    sSortie = "OUT !"
    var move = $('#move_jq').val();
    //Déplacement en Haut à Gauche
    if(move == 0 && move >= 0 && move <= 7){
        if(ligneRecup >= 0 && colonneRecup >= 0 && ligneRecup <= 7 && colonneRecup <= 7){
            aDamier[ligneRecup][colonneRecup] = '0';
            aDamier[ligneRecup - 1][colonneRecup - 1] = 'X';
            ligneRecup--;
            colonneRecup--;
        }
        else
        {
            $('#resultatOUT_jq').show().html(sSortie);
            $('#btn_move').hide();
            $('#btn_retry').show();
        }
    }
     //Déplacement en Haut à Droite
    else if(move == 1 && move >= 0 && move <= 7){
        if(ligneRecup >= 0 && colonneRecup >= 0 && ligneRecup <= 7 && colonneRecup <= 7){
            aDamier[ligneRecup][colonneRecup] = '0';
            aDamier[ligneRecup - 1][colonneRecup + 1] = 'X';
            ligneRecup--;
            colonneRecup++;
        }
        else{
            $('#resultatOUT_jq').show().html(sSortie);
            $('#btn_move').hide();
            $('#btn_retry').show();
        }
    }
    //Déplacement en Bas à Gauche
    else if(move == 2 && move >= 0 && move <= 7){
        if(ligneRecup >= 0 && colonneRecup >= 0 && ligneRecup <= 7 && colonneRecup <= 7){
            aDamier[ligneRecup][colonneRecup] = '0';
            aDamier[ligneRecup + 1][colonneRecup - 1] = 'X';
            ligneRecup++;
            colonneRecup--;
        }
        else{
            $('#resultatOUT_jq').show().html(sSortie);
            $('#btn_move').hide();
            $('#btn_retry').show();
        }
    }
    //Déplacement en Bas à Droite
    else if(move == 3 && move >= 0 && move <= 7){
        if(ligneRecup >= 0 && colonneRecup >= 0 && ligneRecup <= 7 && colonneRecup <= 7){
            aDamier[ligneRecup][colonneRecup] = '0';
            aDamier[ligneRecup + 1][colonneRecup + 1] = 'X';
            ligneRecup++;
            colonneRecup++;
        }
        else{
            $('#resultatOUT_jq').show().html(sSortie);
            $('#btn_move').hide();
            $('#btn_retry').show();
        }
    }
    //Actualisation du tableau
    for(i=0 ; i < 8 ; i++){
        for(j=0 ; j < 8 ; j++){
            sHTML += aDamier[i][j] + ' ';
        }
        sHTML += "</br>";
    }
    $('#resultat8_3_jq').html(sHTML);
}
