!function(t){
    //Effet au scroll
    "use strict";
    t('a.js-scroll-trigger[href*="#"]:not([href="#"])').click(
        function(){
            //Verifie les chemins
            console.log(location.pathname.replace(/^\//,""));
            //Vérifie hostname
            console.log(location.hostname);
            if(location.pathname.replace(/^\//,"")==this.pathname.replace(/^\//,"")&&location.hostname==this.hostname){
                var e=t(this.hash);
                // this.hash = #nom_href
                console.log(this.hash);
                if((e=e.length?e:t("[name="+this.hash.slice(1)+"]")).length)
                    return t("html, body").animate(
                        {
                            scrollTop:e.offset().top
                        },1e3,"easeInOutBack"),!1 //Modifier valeur de easeInOutBack dans jquery.easing.min pour changer le type d'animation
                    }
        }
    ),
    //Modification nav en fonction taille ecran
    t(".js-scroll-trigger").click(
        function(){
            t(".navbar-collapse").collapse("hide")
        }
    ),
    t("body").scrollspy({target:"#sideNav"})
}(jQuery);

function setButtonNavPosition(){
    console.log($('#nbtn-menu').css('order'));
    if($('#btn-menu').css('order') == 2)
    {
        $('#btn-menu').css('order', 0);
    }
    else
    {
        $('#btn-menu').css('order', 2);
    }
}
// function toggleNavbar(){
//     /* onresize si le display .navbar-expand-lg .navbar-toggler vaut none on set la navbar en vertical */
//     if(window.innerWidth>680){
//         $("#sideNav").css({
//             'text-align' : 'center',
//             height : '100vh',
//             position : 'fixed',
//             'flex-direction' : 'column',
//             top : '0',
//             left : '0',
//             width : '15rem',
//         });
//     }
//     else{
//         $("#sideNav").css({
//             position : 'fixed',
//             top : '0',
//             right : '0',
//             left : '0',
//             'z-index' : '1030',
//         });
//     }
// }