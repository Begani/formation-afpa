var aOfData = [];
aOfData[0] = "cornichon";
aOfData[1] = "poivre";
aOfData[2] = "paprika";
aOfData[3] = "champignon";
aOfData[4] = "pomme de terre";
aOfData[5] = "lard";
aOfData[6] = "jambonneau";


function constructDatalist(){
    var i;
    var sHTML = "";

    sHTML += "<label for='liste-course'>Liste de course : </label>"
    sHTML += "<input onkeyup='compare()' list='liste-course-item' id='liste-course' name='liste-course' />"
    sHTML += "<datalist id='liste-course-item'>"

    for(i = 0; i < aOfData.length; i++){
   
        sHTML += "<option value='"+ aOfData[i] +"' data-id='"+i+"'>";
    }

    sHTML += "</datalist>"

    $('#datalist_to_build').html(sHTML);
}

function compare(){

    var mot_a_comparer = $("#liste-course").val();
    console.log(mot_a_comparer.toString());

    //Boucle sur le tableau
    for(j=0; j < aOfData.length; j++){

        var motTab = aOfData[j];

        if (motTab == mot_a_comparer || mot_a_comparer == "")
        {
            $('#button-add').hide();
            return;
        }
        else
        {
            $('#button-add').show();
        }
    }

}

function add_new_item(){
    console.log($("#liste-course").val() );
    aOfData.push($("#liste-course").val());
    constructDatalist();
}