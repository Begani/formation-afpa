// *********************************** EXERCICE 1 *****************************************

function s6_exo1_js(){
    var tableau_1 = [0, 0, 0, 0, 0, 0, 0];
    document.getElementById('tableau_1').innerHTML = tableau_1;

}
function s6_exo1_jq(){
    var tableau_1 = [0, 0, 0, 0, 0, 0, 0];
    $('#tableau_1_jq').html(tableau_1);

}

// *********************************** EXERCICE 2 *****************************************

function s6_exo2_js(){
    var suite = [1,1];
    for(i=2; i<=7; i++){
        var newSuite = suite.push(suite[i-1] + suite[i-2]);
        newSuite;
    }
    document.getElementById('resultat_algo6').innerHTML = suite;
}

function s6_exo2_jq(){
    var suite = [1,1];
    for(i=2; i<=7; i++){
        var newSuite = suite.push(suite[i-1] + suite[i-2]);
        newSuite;
    }
    $('#resultat_algo6_jq').html(suite);
}

// *********************************** EXERCICE 3 *****************************************

function s6_exo3_js(){
    
    //Recup saisie utilisateur = nb input
    var saisie = document.getElementById('nbInput').value;
    console.log(saisie);
    //Crée un nb d'input égale à saisie
    for(i=0; i<saisie; i++){
        document.getElementById('newInput').innerHTML += '<input type="number" id="newInput_' + i + '" placeholder="Note ' + (i+1) + '" >'
    }
    //Déclaration tableau de notes + insertion de notes utilisateur
    //Cache input de choix d'input
    document.getElementById('nbInput').style.display = "none";
    
}

function moyenne_js(){
    var aNotes = [];
    var sommeNote = 0;
    var nbNotes = parseInt(document.getElementById('nbInput').value);
    // console.log(nbNotes, 'taille tableau');
    for(y=0; y<nbNotes; y++){
        var getId = parseInt(document.getElementById("newInput_" + y).value);
        console.log(getId);
        var getNote = aNotes.push(getId);
        getNote;
        // console.log(getNote, 'getnote');
        sommeNote += aNotes[y];
        // console.log(sommeNote, 'sommeNote')
        // console.log(nbNotes, 'nbNotes')
        var moyenne = sommeNote / nbNotes;
        // console.log(moyenne, 'moyenne');
    }
    
    document.getElementById('moyenne').innerHTML = 'La moyenne est de : ' + moyenne + '.';
}

function s6_exo3_jq(){
    //Recup saisie utilisateur = nb input
    var saisie = $('#nbInput_jq').val();
    console.log(saisie);
    //Crée un nb d'input égale à saisie
    for(i=0; i<saisie; i++){
        $('#newInput_jq').append('<input type="number" id="newInput_jq' + i + '" placeholder="Note ' + (i+1) + '" >');
    }
    //Déclaration tableau de notes + insertion de notes utilisateur
    //Cache input de choix d'input
    $('#nbInput_jq').hide();
    
}

function moyenne_jq(){
    var aNotes = [];
    var sommeNote = 0;
    var nbNotes = parseInt($('#nbInput_jq').val());
    console.log(nbNotes, 'taille tableau');
    for(y=0; y<nbNotes; y++){
        var getId = parseInt($("#newInput_jq" + y).val());
        console.log(getId);
        var getNote = aNotes.push(getId);
        getNote;
        console.log(getNote);
        sommeNote += aNotes[y];
        console.log(sommeNote, 'sommeNote')
        console.log(nbNotes, 'nbNotes')
        var moyenne = sommeNote / nbNotes;
        console.log(moyenne, 'moyenne');
    }
    
    $('#moyenne_jq').html('La moyenne est de : ' + moyenne + '.');
}

// *********************************** EXERCICE 4 *****************************************

function s6_exo4_js(){
    
    //Recup saisie utilisateur = nb input
    var saisie = document.getElementById('nbInput4').value;
    console.log(saisie);
    //Crée un nb d'input égale à saisie
    for(i=0; i<saisie; i++){
        document.getElementById('newInput4').innerHTML += '<input type="number" id="newInput4_' + i + '" placeholder="Note ' + i + '" >'
    }
    //Déclaration tableau de notes + insertion de notes utilisateur
    //Cache input de choix d'input
    document.getElementById('nbInput4').style.display = "none";
    
}

function tri_js(){
    var aNotes = [];
    var iPos = 0;
    var iNeg = 0;
    var nbNotes = parseInt(document.getElementById('nbInput4').value);
    // console.log(nbNotes, 'taille tableau');
    for(y=0; y<nbNotes-1; y++){
        var getId = parseInt(document.getElementById("newInput4_" + y).value);
        console.log(getId);
        var getNote = aNotes.push(getId);
        getNote;
        if(aNotes[y] >=0)
        {
            iPos++;
        }
        else
        {
            iNeg++;
        }
    }
    console.log(aNotes); 

    document.getElementById('tri').innerHTML = 'Nombre de positifs : ' + iPos + '.</br> Nombre de négatifs : ' + iNeg + '.';
}

function s6_exo4_jq(){
    
    //Recup saisie utilisateur = nb input
    var saisie = $('#nbInput4_jq').val();
    console.log(saisie);
    //Crée un nb d'input égale à saisie
    for(i=0; i<saisie; i++){
        $('#newInput4_jq').append('<input type="number" id="newInput4_jq' + i + '" placeholder="Note ' + i + '" >');
    }
    //Déclaration tableau de notes + insertion de notes utilisateur
    //Cache input de choix d'input
    $('#nbInput4').hide();
    
}

function tri_jq(){
    var aNotes = [];
    var nbNotes = parseInt($('#nbInput4_jq').val());
    var iPos = 0;
    var iNeg = 0;
    // console.log(nbNotes, 'taille tableau');
    for(y=0; y<nbNotes-1; y++){
        var getId = parseInt($("#newInput4_" + y).val());
        console.log(getId);
        var getNote = aNotes.push(getId);
        getNote;
        if(aNotes[y] >=0)
        {
            iPos++;
        }
        else
        {
            iNeg++;
        }
    }
    console.log(aNotes); 

    $('#tri_jq').html('Nombre de positifs : ' + iPos + '.</br> Nombre de négatifs : ' + iNeg + '.');
}


// *********************************** EXERCICE 5 *****************************************

function s6_exo5_js(){
    var aTableau_1 = [4, 8, 7, 9, 1, 5, 4, 6];
    var aTableau_2 = [7, 6, 5, 2, 1, 3, 7, 4];

    var aTableau_3 = [];
    for(i = 0; i < aTableau_1.length ; i++)
    {
        aTableau_3[i] = aTableau_1[i] + aTableau_2[i];
        console.log(aTableau_3);
    }

    document.getElementById('tableau_3').innerHTML = 'Tableau 3 : ' + aTableau_3;
}

function s6_exo5_jq(){
    var aTableau_1 = [4, 8, 7, 9, 1, 5, 4, 6];
    var aTableau_2 = [7, 6, 8, 2, 1, 3, 7, 4];

    var aTableau_3 = [];
    for(i = 0; i < aTableau_1.length ; i++)
    {
        aTableau_3[i] = aTableau_1[i] + aTableau_2[i];
        console.log(aTableau_3);
    }
    $('#tableau_3_jq').append(aTableau_3);
}

// *********************************** EXERCICE 6 *****************************************

function s6_exo6_js(){
        //Recup saisie utilisateur = nb input
    
    var saisie = document.getElementById('nbInput6').value;
    console.log(saisie);
    if(saisie != ""){
    
        //Crée un nb d'input égale à saisie
    for(i=0; i<saisie; i++)
    {
        document.getElementById('newInput6').innerHTML += '<input type="number" id="newInput6_' + i + '" placeholder="Note ' + i + '" >';
    }
    document.getElementById('nbInput6').style.display = "none";
    document.getElementById('nbNombre6').value = saisie;
    document.getElementById('nbInput6').value = "";
    }
    else
    {
        var aNombre = [];
        var iIndice = 0;
        var iMax = 0;
        var nbNotes = parseInt(document.getElementById('nbNombre6').value);
       
        for(y = 0; y < nbNotes ; y++)
        {
            var getId = parseInt(document.getElementById("newInput6_" + y).value);
            var getNombre = aNombre.push(getId);
            getNombre;
            console.log(aNombre[y]);
            if(y == 0 || aNombre[y] > iMax)
            {
                iMax = aNombre[y];
                iIndice = y;
            }
        }
        document.getElementById('resultat6_6').innerHTML = 'La plus grande valeur ' + iMax + ' est à l\'indice :' + iIndice;

    }

}

function s6_exo6_jq(){
        
    var saisie = $('#nbInput6_jq').val();
    console.log(saisie);
    if(saisie != ""){
    
        //Crée un nb d'input égale à saisie
    for(i=0; i<saisie; i++)
    {
        $('#newInput6_jq').append('<input type="number" id="newInput6_jq' + i + '" placeholder="Note ' + i + '" >');
    }
    $('#nbInput6_jq').hide();
    $('#nbNombre6_jq').val(saisie);
    $('#nbInput6_jq').val("");
    }
    else
    {
        var aNombre = [];
        var iIndice = 0;
        var iMax = 0;
        var nbNotes = parseInt($('#nbNombre6_jq').val());
       
        for(y = 0; y < nbNotes ; y++)
        {
            var getId = parseInt($("#newInput6_jq" + y).val());
            var getNombre = aNombre.push(getId);
            getNombre;
            console.log(aNombre[y]);
            if(y == 0 || aNombre[y] > iMax)
            {
                iMax = aNombre[y];
                iIndice = y;
            }
        }
        $('#resultat6_6_jq').html('La plus grande valeur ' + iMax + ' est à l\'indice :' + iIndice);

    }
}

// *********************************** EXERCICE 7 *****************************************


function s6_exo7_js(){

    var aTableau_100 = [];
    var iCompteur = 0 ;
    var sRes = "Vrai, les entiers sont consécutifs ";
    //Dans le cas ou l'utilisateur rempli le tableau, remplacer iCompteur par valeur utilisateur recup
    for (i = 0; i < 100; i++)
    {
        iCompteur++;
        aTableau_100[i] = iCompteur ;
       
        if((i >= 1) && ((aTableau_100[i] - aTableau_100[i - 1]) != 1 ))
        { 
            sRes = "Faux, les entiers ne sont pas consécutifs ";   
        }
        // Pour vérifier
        if(i==2)
        {
            iCompteur = 12;
        }
      
        console.log(aTableau_100);
    }
    document.getElementById('resultat6_7').innerHTML = sRes;
}

function s6_exo7_jq(){

    var aTableau_100 = [];
    var iCompteur = 0 ;
    var sRes = "Vrai, les entiers sont consécutifs ";

    //Initialisation d'un tableau consécutif de base
    //Dans le cas ou l'utilisateur rempli le tableau, remplacer iCompteur par valeur utilisateur recup
    for (i = 0; i < 100; i++)
    {
        iCompteur++;
        aTableau_100[i] = iCompteur ;   
    }

    //L'utilisateur change la valeur du tableau pour vérifier
    var iModif = $('#iModif').val();
    var iIndiceAModif = $('iIndice').val();
    if( iModif != "" && iIndiceAModif != ""){
        aTableau_100[iIndiceAModif] = iModif;
            if(((aTableau_100[iIndiceAModif] - aTableau_100[iIndiceAModif - 1]) != 1 ) && (( aTableau_100[iIndiceAModif + 1] - aTableau_100[iIndiceAModif]) != 1 ))
            { 
                sRes = "Faux, les entiers ne sont pas consécutifs ";   
            }
    }
    $('#resultat6_7_jq').html(sRes);

}

// *********************************** EXERCICE 8 *****************************************

function s6_exo8_js(){
    //Recup saisie utilisateur = nb input

    var saisie = document.getElementById('nbInput8').value;
    console.log(saisie);
    if(saisie != ""){

        //Crée un nb d'input égale à saisie
        for(i=0; i<saisie; i++)
        {
            document.getElementById('newInput8').innerHTML += '<input type="number" id="newInput8_' + i + '" placeholder="Nombre ' + i + '" >';
        }
        document.getElementById('nbInput8').style.display = "none";
        document.getElementById('nbNombre8').value = saisie;
        document.getElementById('nbInput8').value = "";
    }
    else
    {
        var aNumbers = [];
        var nbNotes = parseInt(document.getElementById('nbNombre8').value);
    //chaque valeur récupérée est insérée entre valPlusPetite et ValeurPlusGrandes
        for(j = 0; j < nbNotes ; j++)
        {   
            console.log(aNumbers, 'début de for');
            var getNumber = parseInt(document.getElementById("newInput8_" + j).value);
            console.log(getNumber, 'Nombre recup')
            iPosTrouve = -1;
            //Si tableau est vide, ajout 1er élément
            if(aNumbers.length == 0){
                aNumbers[j] = getNumber;
                console.log('1er valeur ajoutée au tableau');
            }
            //Si tableau contient 1 ou plusieurs valeurs
            else if(aNumbers.length >= 1){
                console.log('taille du tableau =' + aNumbers.length);


                for(k=0; k <= aNumbers.length-1; k++){
                    
                    console.log(aNumbers[k], 'aNumbers[k]');  
                    console.log(getNumber, 'getNumber');
                    if(getNumber < aNumbers[k] && iPosTrouve == -1)
                    {
                        iPosTrouve = k;
                        console.log('Indice trouvé : ' +iPosTrouve)
                        iNumTrouve = getNumber;
                        console.log(iNumTrouve, 'iNumTrouve');
                        console.log(aNumbers.length);
                        for(l=(aNumbers.length); l>iPosTrouve; l--)
                        {
                            aNumbers[l] = aNumbers[l-1];
                        }
                        aNumbers[iPosTrouve] = iNumTrouve;
                    }
                    else if(getNumber > aNumbers[aNumbers.length -1] && iPosTrouve == -1)
                    {
                       
                        aNumbers[j] = getNumber;
                    }
                }
            }
            console.log(aNumbers,'fin de for');
        } 
    
    document.getElementById('resultat6_8').innerHTML = aNumbers;
    }
   

}


function s6_exo8_jq(){
    sHTMl = "";

    var saisie =$('#nbInput8_jq').val();
    if(saisie != ""){

        //Crée un nb d'input égale à saisie
        for(i=0; i<saisie; i++)
        {
           $('#newInput8_jq').append('<input type="number" id="newInput8_jq' + i + '" placeholder="Nombre ' + i + '" >');
        }
       $('#nbInput8_jq').hide();
       $('#nbNombre8_jq').val(saisie);
       $('#nbInput8_jq').val("");
    }
    else
    {
        var aNumbers = [];
        var nbNotes = parseInt($('#nbNombre8_jq').val());
    //chaque valeur récupérée est insérée entre valPlusPetite et ValeurPlusGrandes
        for(j = 0; j < nbNotes ; j++)
        {   
            var getNumber = parseInt($("#newInput8_jq" + j).val());
            iPosTrouve = -1;
            //Si tableau est vide, ajout 1er élément
            if(aNumbers.length == 0){
                aNumbers[j] = getNumber;
            }
            //Si tableau contient 1 ou plusieurs valeurs
            else if(aNumbers.length >= 1){

                for(k=0; k <= aNumbers.length-1; k++){
                    
                    if(getNumber < aNumbers[k] && iPosTrouve == -1)
                    {
                        iPosTrouve = k;
                        iNumTrouve = getNumber;

                        for(l=(aNumbers.length); l>iPosTrouve; l--)
                        {
                            aNumbers[l] = aNumbers[l-1];
                        }
                        aNumbers[iPosTrouve] = iNumTrouve;
                    }
                    else if(getNumber > aNumbers[aNumbers.length -1] && iPosTrouve == -1)
                    {
                       
                        aNumbers[j] = getNumber;
                    }
                }
            }
            console.log(aNumbers,'fin de for');
        } 

        for(i=0; i < aNumbers.length; i++){
            sHTMl += " | " + aNumbers[i] ;
        }
    
   $('#resultat6_8_jq').html(sHTMl + " |");
    }
   

}


// ********************************* AFFICHAGE CODE EXO 1 ******************************

function afficheCode6_1js(){
    $("#showImage").html(`
    function s6_exo1_js(){
        var tableau_1 = [0, 0, 0, 0, 0, 0, 0];
        document.getElementById('tableau_1').innerHTML = tableau_1;
    }
    
    `)
};
function afficheCode6_1jq(){
    $("#showImage").html(`
    function s6_exo1_jq(){
        var tableau_1 = [0, 0, 0, 0, 0, 0, 0];
        $('#tableau_1_jq').html(tableau_1);
    
    }
    `)
};


function afficheCode6_1php(){
    $("#showImage").html(`
    $message11 = "";
    if( isset($_POST['cPartant_php']) && isset($_POST['cJoue_php']) )
    {
        $n = intval($_POST['cPartant_php']);
        $p = intval($_POST['cJoue_php']);
        $nF = 1;
        $pF = 1;
        $diffF = 1;
        $calc = $n - $p;;

        for($i=1; $i <= $n; $i++){
            $nF *= $i;
        }
        for($y=1; $y <= $p; $y++){
            $pF *= $y;
        }
        for($z=1; $z <= $calc; $z++){
            $diffF *= $z;
        }
        $X = intval($nF / $diffF);
        $Y = intval($nF / ($pF * $diffF));

        $message11 = 'Dans l\'ordre : une chance sur ' . $X . 'de gagner.</br>Dans le désordre : une chance sur ' . $Y . 'de gagner.';
        require 's5exercice11.html';
    }
    else{
        require 's5exercice11.html';
    }
    `)
};
// ********************************* AFFICHAGE CODE EXO 2 ******************************

function afficheCode6_2_js(){
    $("#showImage").html(`
    function s6_exo2_js(){
        var suite = [1,1];
        for(i=2; i <= 7; i++){
            var newSuite = suite.push(suite[i-1] + suite[i-2]);
            newSuite;
        }
        document.getElementById('resultat_algo6').innerHTML = suite;
    }
    `)
};
function afficheCode6_2_jq(){
    $("#showImage").html(`
    function s6_exo2_jq(){
        var suite = [1,1];
        for(i=2; i <= 7; i++){
            var newSuite = suite.push(suite[i-1] + suite[i-2]);
            newSuite;
        }
        $('#resultat_algo6_jq').html(suite);
    }
    `)
};

function afficheCode6_2_php(){
    $("#showImage").html(`
    $tableau_1 = [0, 0, 0, 0, 0, 0, 0];
    $message_1 = "";
    $suite = [1,1];

    for($i=2; $i <= 7; $i++){
        $suite[$i] = $suite[$i-1] + $suite[$i-2];
    }

    `)
};


// ********************************* AFFICHAGE CODE EXO 3 ******************************



function afficheCode6_3_js(){
    $("#showImage").html(`
    function s6_exo3_js(){
    
        //Recup saisie utilisateur = nb input
        var saisie = document.getElementById('nbInput').value;
        console.log(saisie);
        //Crée un nb d'input égale à saisie
        for(i=0; i < saisie; i++){
            document.getElementById('newInput').innerHTML += '<()input type="number" id="newInput_' + (i+1) + '" placeholder /="Note ' + i + '" >'
        }
        //Déclaration tableau de notes + insertion de notes utilisateur
        //Cache input de choix d'input
        document.getElementById('nbInput').style.display = "none";
    }
    
    function moyenne_js(){
        var aNotes = [];
        var sommeNote = 0;
        var nbNotes = parseInt(document.getElementById('nbInput').value);
        // console.log(nbNotes, 'taille tableau');
        for(y=0; y < nbNotes; y++){
            var getId = parseInt(document.getElementById("newInput_" + y).value);
            console.log(getId);
            var getNote = aNotes.push(getId);
            getNote;
            // console.log(getNote, 'getnote');
            sommeNote += aNotes[y];
            // console.log(sommeNote, 'sommeNote')
            // console.log(nbNotes, 'nbNotes')
            var moyenne = sommeNote / nbNotes;
            // console.log(moyenne, 'moyenne');
        }
        
        document.getElementById('moyenne').innerHTML = 'La moyenne est de : ' + moyenne + '.';
    }
    `)
};

function afficheCode6_3_jq(){
    $("#showImage").html(`
    function s6_exo3_jq(){
        //Recup saisie utilisateur = nb input
        var saisie = $('#nbInput_jq').val();
        console.log(saisie);
        //Crée un nb d'input égale à saisie
        for(i=0; i < saisie; i++){
            $('#newInput_jq').append('<()input type="number" id="newInput_jq' + i + '" placeholder="Note ' + (i+1) + '" >');
        }
        //Déclaration tableau de notes + insertion de notes utilisateur
        //Cache input de choix d'input
        $('#nbInput_jq').hide(); 
    }
    
    function moyenne_jq(){
        var aNotes = [];
        var sommeNote = 0;
        var nbNotes = parseInt($('#nbInput_jq').val());
        console.log(nbNotes, 'taille tableau');
        for(y=0; y < nbNotes; y++){
            var getId = parseInt($("#newInput_jq" + y).val());
            console.log(getId);
            var getNote = aNotes.push(getId);
            getNote;
            console.log(getNote);
            sommeNote += aNotes[y];
            console.log(sommeNote, 'sommeNote')
            console.log(nbNotes, 'nbNotes')
            var moyenne = sommeNote / nbNotes;
            console.log(moyenne, 'moyenne');
        }
        
        $('#moyenne_jq').html('La moyenne est de : ' + moyenne + '.');
    }
    `)
};

function afficheCode6_3_php(){
    $("#showImage").html(`

    $nbInput = 0;
    $addInput = "";
    $iTotal = 0;
    $iMoyenne = "0" ;
    $nbNoteInt = 0;
    if( !isset($_POST['newInput_php0']))
    {
        $nbInput = $_POST['nbInput_php'];
        for ($i = 0; $i < $nbInput; $i++)
        {
            $addInput .= '<()input type="number" id="newInput_php'. $i .'" name="newInput_php'. $i .'" placeholder="Note ' . ($i+1) . '" value="">';
            $nbNoteInt++;
        }
        var_dump($addInput);
        var_dump($nbInput);
        // Fin creation tableau de notes
        // Début recup des notes
        require 's6exercice3.html'; 
    }  
    else if(isset($_POST['newInput_php0']))
    {
        $aNotes = [];
        $nbNote = $_POST['nbNote_php'];
        for($y = 0;  $y < $nbNote; $y++)
        {
            $addTab = intval($_POST['newInput_php' . $y]);
            $aNotes[$y] = $addTab;
            $iTotal += $aNotes[$y];
        }
        var_dump($iTotal);
        var_dump($nbNote);
        $iMoyenne = 'La moyenne des notes est de : '. $iTotal/$nbNote;
        var_dump($iMoyenne);
    require 's6exercice3.html'; 
    }   
    else
    {
        require 's6exercice3.html';
    }
    `)
};

// ********************************* AFFICHAGE CODE EXO 4 ******************************
function afficheCode6_4_js(){
    $("#showImage").html(`
    function s6_exo4_js(){
    
        //Recup saisie utilisateur = nb input
        var saisie = document.getElementById('nbInput4').value;
        console.log(saisie);
        //Crée un nb d'input égale à saisie
        for(i=0; i<saisie; i++){
            document.getElementById('newInput4').innerHTML += '<input type="number" id="newInput4_' + i + '" placeholder="Note ' + i + '" >'
        }
        //Déclaration tableau de notes + insertion de notes utilisateur
        //Cache input de choix d'input
        document.getElementById('nbInput4').style.display = "none";
    }
    
    function tri_js(){
        var aNotes = [];
        var aPositif = [];
        var aNegatif = [];
        var nbNotes = parseInt(document.getElementById('nbInput4').value);
        // console.log(nbNotes, 'taille tableau');
        for(y=0; y<nbNotes; y++){
            var getId = parseInt(document.getElementById("newInput4_" + y).value);
            console.log(getId);
            var getNote = aNotes.push(getId);
            getNote;
            if(aNotes[y] >=0)
            {
                aPositif.push(aNotes[y]);
            }
            else
            {
                aNegatif.push(aNotes[y]);
            }
        }
        console.log(aNotes); 
    
        document.getElementById('tri').innerHTML = 'Les positifs : ' + aPositif + '.</br> Les négatifs : ' + aNegatif + '.';
    }
    `)
};

function afficheCode6_4_jq(){
    $("#showImage").html(`
    function s6_exo4_jq(){
        
        //Recup saisie utilisateur = nb input
        var saisie = $('#nbInput4_jq').val();
        console.log(saisie);
        //Crée un nb d'input égale à saisie
        for(i=0; i<saisie; i++){
            $('#newInput4_jq').append('<input type="number" id="newInput4_jq' + i + '" placeholder="Note ' + i + '" >');
        }
        //Déclaration tableau de notes + insertion de notes utilisateur
        //Cache input de choix d'input
        $('#nbInput4').hide();
    }
    function tri_jq(){
        var aNotes = [];
        var aPositif = [];
        var aNegatif = [];
        var nbNotes = parseInt($('#nbInput4_jq').val());
        // console.log(nbNotes, 'taille tableau');
        for(y=0; y<nbNotes; y++){
            var getId = parseInt($("#newInput4_" + y).val());
            console.log(getId);
            var getNote = aNotes.push(getId);
            getNote;
            if(aNotes[y] >=0)
            {
                aPositif.push(aNotes[y]);
            }
            else
            {
                aNegatif.push(aNotes[y]);
            }
        }
        console.log(aNotes); 
    
        $('#tri_jq').html('Les positifs : ' + aPositif + '.</br> Les négatifs : ' + aNegatif + '.');
    }
    
    `)
};

function afficheCode6_4_php(){
    $("#showImage").html(`
    $nbInput4 = 0;
    $addInput4 = "";
    $iTotal4 = 0;
    $iMoyenne4 = "0" ;
    $nbNoteInt4 = 0;
    $cptPos = 0;
    $cptNeg = 0;
    $aPositif = [];
        $aNegatif = [];
    if( !isset($_POST['newInput4_php0']))
    {
        $nbInput4 = $_POST['nbInput4_php'];
        for ($i = 0; $i < $nbInput4; $i++)
        {
            $addInput4 .= '<input type="number" id="newInput4_php'. $i .'" name="newInput4_php'. $i .'" placeholder="Nombre ' . $i . '" value="">';
            $nbNoteInt4++;
        }
        // Fin creation tableau de notes
        // Début recup des notes
        require 's6exercice4.html'; 
    }  
    else if(isset($_POST['newInput4_php0']))
    {
        $nbNote4 = $_POST['nbNote4_php'];
        for($y = 0;  $y < $nbNote4; $y++)
        {
            $addTab4 = intval($_POST['newInput4_php' . $y]);
            if($addTab4 >= 0)
            {
                $aPositif[$cptPos] = $addTab4;
                $cptPos++;
            }
            else
            {
                 $aNegatif[$cptNeg] = $addTab4;
                 $cptNeg++;
            }
        }
    require 's6exercice4.html'; 
    }   
    else
    {
        require 's6exercice4.html';
    }
    `)
};

// ********************************* AFFICHAGE CODE EXO 6 ******************************

function afficheCode6_5_js(){
    $("#showImage").html(`
    function s6_exo5_js(){
        var aTableau_1 = [4, 8, 7, 9, 1, 5, 4, 6];
        var aTableau_2 = [7, 6, 5, 2, 1, 3, 7, 4];
    
        var aTableau_3 = [];
        for(i = 0; i < aTableau_1.length ; i++)
        {
            aTableau_3[i] = aTableau_1[i] + aTableau_2[i];
            console.log(aTableau_3);
        }
    
        document.getElementById('tableau_3').innerHTML = 'Tableau 3 : ' + aTableau_3;
    }    
    `)
};

function afficheCode6_5_jq(){
    $("#showImage").html(`
    function s6_exo5_jq(){
        var aTableau_1 = [4, 8, 7, 9, 1, 5, 4, 6];
        var aTableau_2 = [7, 6, 8, 2, 1, 3, 7, 4];
    
        var aTableau_3 = [];
        for(i = 0; i < aTableau_1.length ; i++)
        {
            aTableau_3[i] = aTableau_1[i] + aTableau_2[i];
            console.log(aTableau_3);
        }
        $('#tableau_3_jq').append(aTableau_3);
    }
    `)
};

function afficheCode6_5_php(){
    $("#showImage").html(`
    $aTableau_1 = [4, 8, 7, 9, 1, 5, 4, 6];
    $aTableau_2 = [7, 6, 5, 2, 1, 3, 7, 4];
    $aTableau_3 = [];

    for($i = 0; $i < count($aTableau_1) ; $i++)
    {
        $aTableau_3[$i] = $aTableau_1[$i] + $aTableau_2[$i];
    }
    require 's6exercice5.html'
    `)
};

// ********************************* AFFICHAGE CODE EXO 6 ******************************

function afficheCode6_6_js(){
    $("#showImage").html(`
    function s6_exo6_js(){
        //Recup saisie utilisateur = nb input
    
    var saisie = document.getElementById('nbInput6').value;
    console.log(saisie);
    if(saisie != ""){
    
        //Crée un nb d'input égale à saisie
    for(i=0; i<saisie; i++)
    {
        document.getElementById('newInput6').innerHTML += '<input type="number" id="newInput6_' + i + '" placeholder="Note ' + i + '" >';
    }
    document.getElementById('nbInput6').style.display = "none";
    document.getElementById('nbNombre6').value = saisie;
    document.getElementById('nbInput6').value = "";
    }
    else
    {
        var aNombre = [];
        var iIndice = 0;
        var iMax = 0;
        var nbNotes = parseInt(document.getElementById('nbNombre6').value);
       
        for(y = 0; y < nbNotes ; y++)
        {
            var getId = parseInt(document.getElementById("newInput6_" + y).value);
            var getNombre = aNombre.push(getId);
            getNombre;
            console.log(aNombre[y]);
            if(y == 0 || aNombre[y] > iMax)
            {
                iMax = aNombre[y];
                iIndice = y;
            }
        }
        document.getElementById('resultat6_6').innerHTML = 'La plus grande valeur ' + iMax + ' est à l\'indice :' + iIndice;
    }
}
    `)
};

function afficheCode6_6_jq(){
    $("#showImage").html(`
    function s6_exo6_jq(){
        
        var saisie = $('#nbInput6_jq').val();
        console.log(saisie);
        if(saisie != ""){
        
            //Crée un nb d'input égale à saisie
        for(i=0; i<saisie; i++)
        {
            $('#newInput6_jq').append('<input type="number" id="newInput6_jq' + i + '" placeholder="Note ' + i + '" >');
        }
        $('#nbInput6_jq').hide();
        $('#nbNombre6_jq').val(saisie);
        $('#nbInput6_jq').val("");
        }
        else
        {
            var aNombre = [];
            var iIndice = 0;
            var iMax = 0;
            var nbNotes = parseInt($('#nbNombre6_jq').val());
           
            for(y = 0; y < nbNotes ; y++)
            {
                var getId = parseInt($("#newInput6_jq" + y).val());
                var getNombre = aNombre.push(getId);
                getNombre;
                console.log(aNombre[y]);
                if(y == 0 || aNombre[y] > iMax)
                {
                    iMax = aNombre[y];
                    iIndice = y;
                }
            }
            $('#resultat6_6_jq').html('La plus grande valeur ' + iMax + ' est à l\'indice :' + iIndice);
    
        }
    }
    `)
};

function afficheCode6_6_php(){
    $("#showImage").html(`
    $message6 = "";
    $iRep = 0;
    if(
        isset($_POST['iNbr6_php'])
    )
    {
        $iNbr6 = intval($_POST[('iNbr6_php')]); 
        $iNbrOriginal = $iNbr6;

        for($i=1; $i<=$iNbr6; $i++){
            $iRep += $i; 
            $message6 = 'La somme de tous les entiers de ' .$iNbrOriginal. ' est : ' . $iRep;
        }
        require 's6exercice6.html';
    }
    else
    {
        require 's6exercice6.html';
    }

    `)
};

// ********************************* AFFICHAGE CODE EXO 7 ******************************

function afficheCode6_7_js(){
    $("#showImage").html(`    
    function s6_exo7_js(){

        var aTableau_100 = [];
        var iCompteur = 0 ;
        var sRes = "Vrai, les entiers sont consécutifs ";
        //Dans le cas ou l'utilisateur rempli le tableau, remplacer iCompteur par valeur utilisateur recup
        for (i = 0; i < 100; i++)
        {
            iCompteur++;
            aTableau_100[i] = iCompteur ;
           
            if((i >= 1) && ((aTableau_100[i] - aTableau_100[i - 1]) != 1 ))
            { 
                sRes = "Faux, les entiers ne sont pas consécutifs ";   
            }
            //Pour vérifier
            // if(i==2)
            // {
            //     iCompteur = 12;
            // }
            console.log(aTableau_100);
        }
        document.getElementById('resultat6_7').innerHTML = sRes;
    }
    `)
};

function afficheCode6_7_jq(){
    $("#showImage").html(`
    function s6_exo7_jq(){
    
        var aTableau_100 = [];
        var iCompteur = 0 ;
        var sRes = "Vrai, les entiers sont consécutifs ";
    
        //Initialisation d'un tableau consécutif de base
        //Dans le cas ou l'utilisateur rempli le tableau, remplacer iCompteur par valeur utilisateur recup
        for (i = 0; i < 100; i++)
        {
            iCompteur++;
            aTableau_100[i] = iCompteur ;   
        }
    
        //L'utilisateur change la valeur du tableau pour vérifier

        var iModif = $('#iModif').val();
        var iIndiceAModif = $('iIndice').val();

        if( iModif != "" && iIndiceAModif != ""){
            aTableau_100[iIndiceAModif] = iModif;

                if(((aTableau_100[iIndiceAModif] - aTableau_100[iIndiceAModif - 1]) != 1 ) && (( aTableau_100[iIndiceAModif + 1] - aTableau_100[iIndiceAModif]) != 1 ))
                { 
                    sRes = "Faux, les entiers ne sont pas consécutifs ";   
                }
        }
        $('#resultat6_7_jq').html(sRes);
    
    `)
};

function afficheCode6_7_php(){
    $("#showImage").html(`
    $aTableau_100 = [];
    for ($i = 0; $i < 100; $i++)
    {
        $aTableau_100[$i] = random_int(0 , 100);
       
        if($i >= 1 && $aTableau_100[$i] > $aTableau_100[$i - 1])
        {
            $sMessage6_7 = "Vrai, les entiers sont consécutifs ";
        }
        else if($i >= 1 && $aTableau_100[$i] < $aTableau_100[$i - 1])
        {
            $sMessage6_7 = "Faux, les entiers ne sont pas consécutifs ";
        }
    }
    `)
};

// ********************************* AFFICHAGE CODE EXO 8 ******************************

function afficheCode6_8_js(){
    $("#showImage").html(`
    function s6_exo8_js(){
        //Recup saisie utilisateur = nb input
    
        var saisie = document.getElementById('nbInput8').value;
        console.log(saisie);
        if(saisie != ""){
    
            //Crée un nb d'input égale à saisie
            for(i=0; i<saisie; i++)
            {
                document.getElementById('newInput8').innerHTML += '<input type="number" id="newInput8_' + i + '" placeholder="Nombre ' + i + '" >';
            }
            document.getElementById('nbInput8').style.display = "none";
            document.getElementById('nbNombre8').value = saisie;
            document.getElementById('nbInput8').value = "";
        }
        else
        {
            var aNumbers = [];
            var nbNotes = parseInt(document.getElementById('nbNombre8').value);
        //chaque valeur récupérée est insérée entre valPlusPetite et ValeurPlusGrandes
            for(j = 0; j < nbNotes ; j++)
            {   
                console.log(aNumbers, 'début de for');
                var getNumber = parseInt(document.getElementById("newInput8_" + j).value);
                console.log(getNumber, 'Nombre recup')
                iPosTrouve = -1;
                //Si tableau est vide, ajout 1er élément
                if(aNumbers.length == 0){
                    aNumbers[j] = getNumber;
                    console.log('1er valeur ajoutée au tableau');
                }
                //Si tableau contient 1 ou plusieurs valeurs
                else if(aNumbers.length >= 1){
                    console.log('taille du tableau =' + aNumbers.length);
    
    
                    for(k=0; k <= aNumbers.length-1; k++){
                        
                        console.log(aNumbers[k], 'aNumbers[k]');  
                        console.log(getNumber, 'getNumber');
                        if(getNumber < aNumbers[k] && iPosTrouve == -1)
                        {
                            iPosTrouve = k;
                            console.log('Indice trouvé : ' +iPosTrouve)
                            iNumTrouve = getNumber;
                            console.log(iNumTrouve, 'iNumTrouve');
                            console.log(aNumbers.length);
                            for(l=(aNumbers.length); l>iPosTrouve; l--)
                            {
                                aNumbers[l] = aNumbers[l-1];
                            }
                            aNumbers[iPosTrouve] = iNumTrouve;
                        }
                        else if(getNumber > aNumbers[aNumbers.length -1] && iPosTrouve == -1)
                        {
                           
                            aNumbers[j] = getNumber;
                        }
                    }
                }
                console.log(aNumbers,'fin de for');
            } 
        
        document.getElementById('resultat6_8').innerHTML = aNumbers;
        }
       
    
    }
    `)
};

function afficheCode6_8_jq(){
    $("#showImage").html(`
    function s6_exo8_jq(){
        var iNombreRecup = parseInt($('#iNombreRecup_jq').val());
        var iNombreStocke = $('#iNombreStocke_jq').val();
        var compteur8 = parseInt($('#compteur8_jq').val());
        var conditionStock = (iNombreStocke < iNombreRecup);
        var compteurRes = $('#compteurRes_jq').val();
        compteur8++; 
        $("#compteur8_jq").val(compteur8);
        
        if(iNombreStocke == "" || conditionStock){
            iNombreStocke = iNombreRecup;
            $("#iNombreStocke_jq").val(iNombreStocke);
            compteurRes = compteur8;
            $("#compteurRes_jq").val(compteurRes);
        }
        else if(compteur8 == 10 || iNombreRecup == 0 ){
            var message = ' est le plus grand, c\'est le ' + compteurRes + 'è nombre renseigné';
            $('#resultat6_8_jq').html("Vous avez fini. " + iNombreStocke + message);
        }
    }
    `)
};

function afficheCode6_8_php(){
    $("#showImage").html(`
    $sMessage8 = "";
    $sHTML = "";
    $addInput8 = "";
    $nbNoteInt8;
    $saisie = 0;

    if( !isset($_POST['newInput8_php0']) )
    {
        
    $saisie = $_POST['nbInput8_php'];
    var_dump($saisie);
        //Crée un nb d'input égale à saisie
        for($i=0; $i<$saisie; $i++)
        {
            $addInput8 .= '<input type="number" id="newInput8_php'. $i .'" name="newInput8_php'. $i .'" placeholder="Nombre ' . $i . '" >';
            $nbNoteInt8++;
        }
        require 's6exercice8.html';
    }

    else
    {
        $aNumbers = [];
        $nbNote8 = $_POST['nbNumber_php'];
    //chaque valeur récupérée est insérée entre valPlusPetite et ValPlusGrande
        for($j = 0; $j < $nbNote8 ; $j++)
        {   
            $getNumber = $_POST["newInput8_php" . $j];
            $iPosTrouve = -1;

            if(count($aNumbers) == 0){
                $aNumbers[$j] = $getNumber;
            }
            else if(count($aNumbers)>= 1){

                for($k=0; $k <= count($aNumbers) - 1; $k++){
                    
                    if($getNumber < $aNumbers[$k] && $iPosTrouve == -1)
                    {
                        $iPosTrouve = $k;
                        $iNumTrouve = $getNumber;

                        for($l=(count($aNumbers)); $l > $iPosTrouve; $l--)
                        {
                            $aNumbers[$l] = $aNumbers[$l-1];
                        }
                        $aNumbers[$iPosTrouve] = $iNumTrouve;
                    }
                    else if($getNumber > $aNumbers[count($aNumbers) - 1] && $iPosTrouve == -1)
                    {
                        $aNumbers[$j] = $getNumber;
                    }
                }
            }
         
  
        }
       
    for($i=0; $i < count($aNumbers); $i++)
    {
        $sHTML .= " | " . $aNumbers[$i] ; 
    }    
    $sMessage8 =  $sHTML . " |";
    
    require 's6exercice8.html';
    }
    `)
};