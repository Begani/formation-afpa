
function saison2(){
    
    let getSelect = document.getElementById("saison2List");
    console.log("getSelect: " + getSelect);
    getOption = getSelect.options[getSelect.selectedIndex].value;
    console.log("getOption: " + getOption);
    var targetModal = document.getElementById("enonce");
    var targetCorpse = document.getElementById("aResoudre");
    var HTMLmodalCorpse = ""; //Variable pour envoyer dynamiquement le HTML
    var HTMLmodalHeader = "";
    HTMLmodalHeader += "";
    HTMLmodalCorpse +=
    `
    
    Choisis un exercice ci-dessous
   
    `;
    HTMLmodalFooter += `

        <select name="saison2List" id="saison2List" label="Saison2">
            <!-- Au click/Chargement -> appel fct modal()-->
            <option value="s2e1" onclick="s2exo1()" >Exercice 1</button>  
            <option value="s2e2" onclick="s2exo2()" >Exercice 2</option>
            <option value="s2e3" onclick="s2exo3()" >Exercice 3</option>
            <option value="s2e4" onclick="s2exo4()" >Exercice 4</option>
        </select>

    
    `
    targetFooter.innerHTML = HTMLmodalFooter;
    targetModal.innerHTML = HTMLmodalHeader;
    targetCorpse.innerHTML = HTMLmodalCorpse;
    console.log("Allez hop je vais chercher " + getOption);

      if(getOption == "s2e1"){
        s2exo1();
      }
      else if(getOption == "s2e2"){
        s2exo2();
      }
      else if(getOption == "s2e3"){
        s2exo3();
      }
      else if(getOption == "s2e4"){
        s2exo4();
      }
      else{
        console.log("j'ai pas compris");
      }
     
}

function s2exo1(){
    var targetModal = document.getElementById("enonce2");
    var targetCorpse = document.getElementById("aResoudre2");
    var HTMLmodalCorpse = ""; //Variable pour envoyer dynamiquement le HTML
    var HTMLmodalHeader = "";
    HTMLmodalHeader += "Quel résultat produit le résultat suivant ?";
    HTMLmodalCorpse +=
        `
        Variables val, double numériques</br>
        Début</br>
        Val ← 231</br>
        Double ← Val * 2</br>
        Ecrire Val</br>
        Ecrire Double</br>
        Fin</br>
        Réponse : Val = 231  / Double = 462
    <div class="row">
        <button onclick="s2exo1js()" class="btn btn-lg btn-primary">Executer</button>
        <button onclick="display_s2exo1js()" class="btn btn-lg btn-primary">Afficher</button>
    </div>
        `;
    targetModal.innerHTML = HTMLmodalHeader;
    targetCorpse.innerHTML = HTMLmodalCorpse;
    console.log("Exo2 en pseudo code OK");
}

function s2exo1js(){
    var Val = 231, Double = Val*2;
    alert("Val = "+ Val + " et Double = "+ Double);
}

let s2exo2 = function(){
    var targetModal = document.getElementById("enonce2");
    var targetCorpse = document.getElementById("aResoudre2");
    var HTMLmodalCorpse = ""; //Variable pour envoyer dynamiquement le HTML
    var HTMLmodalHeader = "";
    HTMLmodalHeader += "Ecrire un programme qui demande un nombre à l’utilisateur, puis qui calcule et affiche le carré de ce nombre.";
    HTMLmodalCorpse +=
        `
    Variables Nb, Carre en Entier</br>
	Debut</br>
		Ecrire "Entrez un nombre:";</br>
		Lire Nb;</br>
		Carre = Nb*Nb;</br>
		Ecrire "Son carré vaut :", Carre</br>
    Fin</br>
    <div class="row">
        <button onclick="s2exo2js()" class="btn btn-lg btn-primary">Javascript</button>
        <button onclick="display_s2exo2js()" class="btn btn-lg btn-primary">Afficher</button>
    </div>
        `;
    targetModal.innerHTML = HTMLmodalHeader;
    targetCorpse.innerHTML = HTMLmodalCorpse;
    console.log("Exo2 en pseudo code OK")
    //JS
}


function s2exo2js(){
    var Nombre=prompt("Entrez un nombre=");
    var Carre = Nombre * Nombre;
    alert("Le carré de " + Nombre + "=" + Carre);
}


function s2exo3(){
    var targetModal = document.getElementById("enonce2");
    var targetCorpse = document.getElementById("aResoudre2");
    var HTMLmodalCorpse = ""; //Variable pour envoyer dynamiquement le HTML
    var HTMLmodalHeader = "";
    HTMLmodalHeader += "Ecrire un programme qui lit le prix HT d’un article, le nombre d’articles et le taux de TVA, et qui fournit le prix total TTC correspondant. Faire en sorte que des libellés apparaissent clairement.";
    HTMLmodalCorpse +=
        `
        Variables prixHT, nbArticle, TVA, TTC</br>
        Debut</br>
            Ecrire "Renseigner le prix Hors-Taxe:"</br>
            Lire prixHT;</br>
            Ecrire "Nombre d'article:";</br>
            Lire nbArticle;</br>
            Ecrire "Renseigner le taux TVA:";</br>
            Lire TVA;</br>
            TTC <- prixHT*nbArticle*(1+TVA);</br>
            Ecrire "Le prix TTC est :", TTC;</br>
        Fin</br>
    
    <div class="row">
        <button onclick="s2exo3js()" class="btn btn-lg btn-primary">Executer</button>
        <button onclick="display_s2exo3js()" class="btn btn-lg btn-primary">Afficher</button>
    </div>
        `;
    targetModal.innerHTML = HTMLmodalHeader;
    targetCorpse.innerHTML = HTMLmodalCorpse;
    console.log("Exo2 en pseudo code OK")
    //JS
}



function s2exo3js(){
    var prixHT=prompt("Prix Hors Taxe:");
    var nbArticle=prompt("Nombre d'articles:");
    var TVA=prompt("Taux TVA:")/100;
    alert(TTC=prixHT*nbArticle*(1+TVA)+ "€");
}

function s2exo4(){
    var targetModal = document.getElementById("enonce2");
    var targetCorpse = document.getElementById("aResoudre2");
    var HTMLmodalCorpse = ""; //Variable pour envoyer dynamiquement le HTML
    var HTMLmodalHeader = "";
    HTMLmodalHeader += "Ecrire un algorithme utilisant des variables de type chaîne de caractères, et affichant quatre variantes possibles de la célèbre « belle marquise, vos beaux yeux me font mourir d’amour ». On ne se soucie pas de la ponctuation, ni des majuscules.";

    HTMLmodalCorpse +=
        `
        Variables t1, t2, t3, t4</br>
        Debut</br>
            t1 <- "belle marquise";</br>
            t2 <- "vos beaux yeux";</br>
            t3 <- "me font mourrir";</br>
            t4 <- "d'amour";</br>
            Ecrire t1 & t2 & t3 & t4;</br>
            Ecrire t2 & t3 & t4 & t1;</br>
            Ecrire t3 & t4 & t1 & t2;</br>
            Ecrire t4 & t1 & t2 & t3;</br>
        Fin</br>
    
    <div class="row">
        <button onclick="s2exo4js()" class="btn btn-lg btn-primary">Executer</button>
        <button onclick="display_s2exo4js()" class="btn btn-lg btn-primary">Afficher</button>
    </div>
        `;
    targetModal.innerHTML = HTMLmodalHeader;
    targetCorpse.innerHTML = HTMLmodalCorpse;
    console.log("Exo2 en pseudo code OK")
    //JS
}
function s2exo4js(){
    var t1 = "belle marquise";
    var t2 = "vos beaux yeux";
    var t3 = "me font mourrir";
    var t4 = "d'amour";

    alert(t1 + t2 + t3 + t4);
    alert(t2 + t3 + t4 + t1);
    alert(t3 + t4 + t1 + t2);
    alert(t4 + t1 + t2 + t3);
}   

function display_s2exo1js(){
    var targetCorpse = document.getElementById("aResoudre2");
    var HTMLmodalCorpse = "";
    HTMLmodalCorpse +=
    `
    function s2exo1js(){</br>
        var Val = 231, Double = Val*2;</br>
        alert("Val = "+ Val + " et Double = "+ Double);</br>
    }</br>
    <button onclick="s2exo1()" class="btn btn-lg btn-primary">Retour</button>
    `;
    targetCorpse.innerHTML = HTMLmodalCorpse;
    
}
function display_s2exo2js(){
    var targetCorpse = document.getElementById("aResoudre2");
    var HTMLmodalCorpse = "";
    HTMLmodalCorpse +=
    `
    function s2exo2js(){</br>
        var Nombre=prompt("Entrez un nombre=");</br>
        var Carre = Nombre * Nombre;</br>
        alert("Le carré de " + Nombre + "=" + Carre);</br>
    }</br>
    <button onclick="s2exo2()" class="btn btn-lg btn-primary">Retour</button>
    `;
    targetCorpse.innerHTML = HTMLmodalCorpse;
    
}
function display_s2exo3js(){
    var targetCorpse = document.getElementById("aResoudre2");
    var HTMLmodalCorpse = "";
    HTMLmodalCorpse +=
    `
    function s2exo3js(){</br>
        var prixHT=prompt("Prix Hors Taxe:");</br>
        var nbArticle=prompt("Nombre d'articles:");</br>
        var TVA=prompt("Taux TVA:")/100;</br>
        alert(TTC=prixHT*nbArticle*(1+TVA)+ "€");</br>
    }</br>
    <button onclick="s2exo3()" class="btn btn-lg btn-primary">Retour</button>
    `;
    targetCorpse.innerHTML = HTMLmodalCorpse;
    
}

function display_s2exo4js(){
    var targetCorpse = document.getElementById("aResoudre2");
    var HTMLmodalCorpse = "";
    HTMLmodalCorpse +=
    `
    function s2exo4js(){</br>
        var t1 = "belle marquise";</br>
        var t2 = "vos beaux yeux";</br>
        var t3 = "me font mourrir";</br>
        var t4 = "d'amour";</br>
        </br>
        alert(t1 + t2 + t3 + t4);</br>
        alert(t2 + t3 + t4 + t1);</br>
        alert(t3 + t4 + t1 + t2);</br>
        alert(t4 + t1 + t2 + t3);</br>
    }   </br>
    <button onclick="s2exo4()" class="btn btn-lg btn-primary">Retour</button>
    `;
    targetCorpse.innerHTML = HTMLmodalCorpse;
    
}