
            // DEBUT FONCTIONS TRAITEMENTS 
function s4exo1js() {
    console.log("s4exo1js");
    var tutu = document.getElementById("info1").value;
    console.log(tutu);
    var tata = document.getElementById("info2").value;
    console.log(tata);
    var toto = document.getElementById("info3").value;
    console.log(toto);

    if (tata != "OK" && tutu <= toto + 4) {
        tutu--;
        console.log("tutu--", tutu);
        document.getElementById("resultat").innerHTML = tutu;
    }
    else {
        tutu++;
        console.log("tutu++", tutu);
        document.getElementById("resultat").innerHTML = tutu;
    }

};

function s4exo1jq() {
    console.log("s4exo1jq");
    var tutu = $("#info1jq").val();
    console.log(tutu);
    var tata = $("#info2jq").val();
    console.log(tata);
    var toto = $("#info3jq").val();
    console.log(toto);

    if (tata != "OK" && tutu <= toto + 4) {
        tutu--;
        console.log("tutu--", tutu);
        $("#resultatjq").html(tutu);
    }
    else {
        tutu++;
        console.log("tutu++", tutu);
        $("#resultatjq").html(tutu);
    }

};

function s4exo2js() {
    console.log('s4exo2js');
    var hour = document.getElementById("info2js1").value;
    console.log(hour);
    var min = document.getElementById("info2js2").value;
    console.log(min);
    min++;
    console.log(min, "2")
    if (hour < 24 && min < 60) {
        document.getElementById("resultat2").innerHTML = "Dans une minute il sera: " + hour + "h" + min + "min.";
    }
    else if (hour < 23 && min == 60) {
        hour++;
        min = 0;
        document.getElementById("resultat2").innerHTML = "Dans une minute il sera: " + hour + "h" + min + "min.";
    }
    else if (hour == 24) {
        hour = 0;
        document.getElementById("resultat2").innerHTML = "Dans une minute il sera: " + hour + "h" + min + "min.";
    }
    else {
        hour = 0;
        min = 0;
        document.getElementById("resultat2").innerHTML = "Dans une minute il sera: " + hour + "h" + min + "min.";
    }
};

function s4exo2jq(){
    console.log('s4exo2jq');
    var hourJq = $('#info2jq1').val();
    console.log(hourJq);
    var minJq = $('#info2jq2').val();
    console.log(minJq);
    minJq++
    if (hourJq < 23 && minJq < 60) {
        console.log("cas1");
    }
    else if (hourJq < 23 && minJq == 60) {
        hourJq++;
        minJq = 0;
        console.log("cas2");
    }
    else if (hourJq == 24) {
        console.log("cas");
        hourJq = 0;
    }
    else {
        console.log("Euh...")
        hourJq = 0;
        minJq = 0;
        
    }
    $("#resultat2jq").html("Dans une minute il sera: " + hourJq + "h" + minJq + "min.");
};


function s4exo3js() {
    console.log('s4exo3js');
    var hour = document.getElementById("info3js1").value;
    console.log(hour);
    var min = document.getElementById("info3js2").value;
    console.log(min);
    var sec = document.getElementById("info3js3").value;
    console.log(min);
    sec++;
    if (hour < 24 && min < 60) {
       
    }
    else if (hour < 23 && min == 60) {
        hour++;
        min = 0;
    }
    else if (hour == 24) {
        hour = 0;
    }
    else {
        hour = 0;
        min = 0; 
    }
    
    if (sec < 60) {
        
    }
    else {
        min++;
        sec = 0;

        if (min == 60) {
            hour++;
            min = 0;
            if (hour < 24) {
              
            }
            else {
                hour = 0;
               
            }
        }
        else {
           
        }
    }
    document.getElementById("resultat3").innerHTML = "Dans une seconde il sera: " + hour + "h " + min + "min " + sec + "secondes.";
};

function s4exo3jq() {
    console.log('s4exo3js');
    var hour = $("#info3jq1").val();
    console.log(hour);
    var min = $("#info3jq2").val();
    console.log(min);
    var sec = $("#info3jq3").val();
    console.log(sec);
    sec++;

    if (hour < 24 && min < 60) {
       console.log("cas1");
    }
    else if (hour < 23 && min == 60) {
        hour++;
        min = 0;
    }
    else if (hour == 24) {
        hour = 0;
    }
    else {
        hour = 0;
        min = 0; 
    }
    console.log("je vais voir les secondes maintenant")
    if (sec < 60) {
        console.log("chillaxMax");
    }
    else {
        min++;
        sec = 0;

        if (min == 60) {
            hour++;
            min = 0;
            if (hour < 24) {
              
            }
            else {
                hour = 0;
               
            }
        }
    }
    console.log("J'affiche");
    $("#resultat3jq").html("Dans une seconde il sera: " + hour + "h " + min + "min " + sec + "secondes.");
};


function s4exo4js() {
    var nbPhotoc = document.getElementById("info4js1").value;
    var prix = 0;
    console.log("nbPhotoc type =" + typeof(nbPhotoc));

    if (nbPhotoc <= 10) {
        prix = nbPhotoc * 0.1;   
    }
    else if (nbPhotoc <= 30) {
        prix = 1 + (nbPhotoc - 10) * 0.09;
    }
    else {
        prix = 1 + 1.8 + (nbPhotoc - 30) * 0.08;
    }
    document.getElementById("resultat4").innerHTML = "Cela vous coutera " + prix + "€.";

}

function s4exo4jq() {
    var nbPhotoc = $("#info4jq1").val();
    var prix = 0;
    console.log("nbPhotoc type =" + typeof(nbPhotoc));

    if (nbPhotoc <= 10) {
        prix = nbPhotoc * 0.1;   
    }
    else if (nbPhotoc <= 30) {
        prix = 1 + (nbPhotoc - 10) * 0.09;
    }
    else {
        prix = 1 + 1.8 + (nbPhotoc - 30) * 0.08;
    }
    $("#resultat4jq").html("Cela vous coutera " + prix + "€.");

}

function s4exo5js() {
    var age = parseInt(document.getElementById("info5js1").value);
    var sexe = document.getElementById("info5js2").value;
    console.log(age, sexe);
    var condition1 = (sexe == "H") && (age > 20);
    var condition2 = (sexe == "F") && ((age >= 18) && (age <= 35));

    if (condition1 || condition2) {     
       document.getElementById("resultat5").innerHTML = "Vous êtes imposables.";
    }
    else {
        document.getElementById("resultat5").innerHTML = "Vous n\'êtes pas imposables";
    }
}

function s4exo5jq() {
    var age = parseInt($("#info5jq1").val());
    var sexe = $("#info5jq2").val();
    console.log(age, sexe);
    var condition1 = (sexe == "H") && (age > 20);
    var condition2 = (sexe == "F") && ((age >= 18) && (age <= 35));

    if (condition1 || condition2) {
       $("#resultat5jq").html("Vous êtes imposables.");
    }
    else {
        $("#resultat5jq").html("Vous n\'êtes pas imposables");
    }
}

function s4exo6js() {
    c1 = document.getElementById("info6js1").value;
    c2 = document.getElementById("info6js2").value;
    c3 = document.getElementById("info6js3").value;
    c4 = document.getElementById("info6js4").value;

    var elu1 = c1 > 50;
    var ballotage = (c1 < 50) && (c1 >= 12.5);
    var ballotageF = (c1 > c2) && (c1 > c3) && (c1 > c4);
    var perdu = (c2 > 50) || (c3 > 50) || (c4 > 50);
    

    if (elu1) {
        document.getElementById('resultat6').innerHTML = "Le candidat 1 est élu au 1er tour avec plus de 50% des voix";
    }
    else if (perdu) {
        document.getElementById('resultat6').innerHTML = "Un autre candidat a reçu plus de 50% des voix";
    }
    else if (ballotage) {
        if (ballotageF) {
            document.getElementById('resultat6').innerHTML = "Le candidat 1 est en ballotage favorable pour le second tour";
        }
        else {
            document.getElementById('resultat6').innerHTML = "Le candidat 1 est en ballotage défavorable pour le second tour";
        }
    }
    else {
        document.getElementById('resultat6').innerHTML ="Le candidat perd au 1er tour avec moins de 12.5% des voix";
    }
}

function s4exo6jq() {
    c1 = $("#info6jq1").val();
    c2 = $("#info6jq2").val();
    c3 = $("#info6jq3").val();
    c4 = $("#info6jq4").val();

    var elu1 = c1 > 50;
    var ballotage = (c1 < 50) && (c1 >= 12.5);
    var ballotageF = (c1 > c2) && (c1 > c3) && (c1 > c4);
    var perdu = (c2 > 50) || (c3 > 50) || (c4 > 50);

    if (elu1) {
        $('#resultat6jq').html("Le candidat 1 est élu au 1er tour avec plus de 50% des voix");
    }
    else if (perdu) {
        $('#resultat6jq').html("Un autre candidat a reçu plus de 50% des voix");
    }
    else if (ballotage) {
        if (ballotageF) {
            $('#resultat6jq').html("Le candidat 1 est en ballotage favorable pour le second tour");
        }
        else {
            $('#resultat6jq').html("Le candidat 1 est en ballotage défavorable pour le second tour");
        }
    }
    else {
        $('#resultat6jq').html("Le candidat perd au 1er tour avec moins de 12.5% des voix");
    }
}

function s4exo7js() {
    var age = parseInt(document.getElementById("info7js1").value);
    var permis =  parseInt(document.getElementById("info7js2").value);
    var accident = parseInt(document.getElementById("info7js3").value);
    var fidelite = parseInt(document.getElementById("info7js4").value);
    console.log(age, permis, accident, fidelite);
    var assur5 = fidelite > 5;
    var age25 = age >= 25;
    var permis2 = permis >= 2;
    var situation = "";


    if (!age25 && !permis2) {
        if (accident == 0) {
            situation = "Rouge";
        }
        else {
            situation = "Refusé";
            document.getElementById("resultat7").innerHTML = situation + ": Nous ne pouvons pas vous assurer : trop d'accident pour un jeune permis";
        }
    }
    else if ((!age25 && permis2) || (age25 && !permis2)) {
        if (accident == 0) {
            situation = "Orange";
        }
        else if (accident == 1) {
            situation = "Rouge";
        }
        else {
            situation = "Réfusé";
            document.getElementById("resultat7").innerHTML = situation + "Nous ne pouvons pas vous assurer : trop d'accident ! ";
        }
        document.getElementById("resultat7").innerHTML = "Vous êtes assuré sous le régime : ." + situation;
    }
    else {
        if (accident == 0) {
            situation = "Vert";

        }
        else if (accident == 1) {
            situation = "Orange";

        }
        else if (accident == 2) {
            situation = "Rouge";

        }
        else {
            situation = "Refusé";
            document.getElementById("resultat7").innerHTML = situation + "Nous ne pouvons pas vous assurer."
        }
        document.getElementById("resultat7").innerHTML = "Vous êtes assuré sous le régime : ." + situation;
    }

    if (assur5) {
        console.log('assur5', assur5)
        if (situation == "Vert") {
            situation = "Bleu";
            document.getElementById("resultat7").innerHTML = "Grace à votre fidélité, vous pouvez être assuré sous le régime : " + situation;
        }
        else if (situation == "Orange") {
            situation = "Vert";
            document.getElementById("resultat7").innerHTML = "Grace à votre fidélité, vous pouvez être assuré sous le régime : " + situation;
        }
        else {
            situation = "Orange";
            document.getElementById("resultat7").innerHTML = "Grace à votre fidélité, vous pouvez être assuré sous le régime : " + situation;
        }
    }
    else {
        var fideliteManquant = 5 - fidelite;
        document.getElementById("resultat7").innerHTML = "Vous bénéficiez de l'offre : " + situation + ". Vous devez restez encore " + fideliteManquant + " ans pour bénéficier de notre offre fidélité.";
    }

};

function s4exo7jq() {
    var age = parseInt($("#info7jq1").val());
    var permis =  parseInt($("#info7jq2").val());
    var accident = parseInt($("#info7jq3").val());
    var fidelite = parseInt($("#info7jq4").val());
    var assur5 = fidelite > 5;
    var age25 = age >= 25;
    var permis2 = permis >= 2;
    var situation = "";


    if (!age25 && !permis2) {
        if (accident == 0) {
            situation = "Rouge";
        }
        else {
            situation = "Refusé";
            $("#resultat7jq").html(situation + ": Nous ne pouvons pas vous assurer : trop d'accident pour un jeune permis");
        }
    }
    else if ((!age25 && permis2) || (age25 && !permis2)) {
        if (accident == 0) {
            situation = "Orange";
        }
        else if (accident == 1) {
            situation = "Rouge";
        }
        else {
            situation = "Réfusé";
            $("#resultat7jq").html(situation + "Nous ne pouvons pas vous assurer : trop d'accident ! ");
        }
    }
    else {
        if (accident == 0) {
            situation = "Vert";

        }
        else if (accident == 1) {
            situation = "Orange";

        }
        else if (accident == 2) {
            situation = "Rouge";

        }
        else {
            situation = "Refusé";
            $("#resultat7jq").html(situation + "Nous ne pouvons pas vous assurer.");
        }
    }

    if (assur5) {

        if (situation == "Vert") {
            situation = "Bleu";
            $("#resultat7jq").html("Grace à votre fidélité, vous pouvez être assuré sous le régime : " + situation);
        }
        else if (situation == "Orange") {
            situation = "Vert";
            $("#resultat7jq").html("Grace à votre fidélité, vous pouvez être assuré sous le régime : " + situation);
        }
        else {
            situation = "Orange";
            $("#resultat7jq").html("Grace à votre fidélité, vous pouvez être assuré sous le régime : " + situation);
        }
    }
    else {
        var fideliteManquant = 5 - fidelite;
        $("#resultat7jq").html("Vous bénéficiez de l'offre : " + situation + ". Vous devez restez encore " + fideliteManquant + "ans pour bénéficier de notre offre fidélité.");
    }

};

function s4exo8js() {
    var J = parseInt(document.getElementById("info8js1").value);
    var M = parseInt(document.getElementById("info8js2").value);
    var A = parseInt(document.getElementById("info8js3").value);

    var bis = (A % 4 == 0 && !(A % 100 == 0)) || A % 400 == 0; // année bissextile oui
    var Jmax = 0;
    var VM = M >= 1 && M <= 12;
    

    if (VM) {
        // Vérifie si l'année est bissextile
        if (M == 2 && bis) {
            Jmax = 29;
            console.log(Jmax, 'bissextile')
        }
        else if (M == 2 && !bis) {
            Jmax = 28;
            console.log(Jmax, ' non bissextile')
        }
        else if ((M == 4) || (M == 6) || (M == 9) || (M == 11)) {
            Jmax = 30;
        }
        else {
            Jmax = 31;
        }
    };
    var VJ = J >= 1 && J <= Jmax;
    console.log(VJ, VM);
    if (VM && VJ) {
        document.getElementById("resultat8").innerHTML = "La date est valide : nous sommes le : " + J + "/" + M + "/" + A;
    }
    else {
        document.getElementById("resultat8").innerHTML = "La date n'est pas valide.";
    }
}

function s4exo8jq() {
    var J = parseInt($("#info8jq1").val());
    var M = parseInt($("#info8jq2").val());
    var A = parseInt($("#info8jq3").val());

    var bis = (A % 4 == 0 && !(A % 100 == 0)) || A % 400 == 0; // année bissextile oui
    var VM = M >= 1 && M <= 12;
    var Jmax = 0;

    if (VM) {
        
        if (M == 2 && !bis) {
            Jmax = 28;
        }// Vérifie si l'année est bissextile
        else if (M == 2 && bis) {
            Jmax = 29;
        }
        else if ((M == 4) || (M == 6) || (M == 9) || (M == 11)) {
            Jmax = 30;
        }
        else {
            Jmax = 31;
        }
    }
    var VJ = J >= 1 && J <= Jmax;
    if (VM && VJ) {
        $("#resultat8jq").html("La date est valide : nous sommes le : " + J + "/" + M + "/" + A);
    }
    else {
        $("#resultat8jq").html("La date n'est pas valide.");
    }
}


            // FIN FONCTIONS TRAITEMENTS
            //DEBUT DES FONCTIONS AFFICHAGE DU CODE
function afficheCode1js(){
    $("#showImage").html(`
    function s4exo1js(){
        console.log("s4exo1js");
        var tutu = document.getElementById("info1").value;
        console.log(tutu);
        var tata = document.getElementById("info2").value;
        console.log(tata);
        var toto = document.getElementById("info3").value;
        console.log(toto);
     
        if(tata != "OK" && tutu <= toto +4){
           tutu--;
           console.log("tutu--", tutu);
           document.getElementById("resultat").innerHTML = tutu;
        }
        else{
           tutu++;
           console.log("tutu++",tutu);
           document.getElementById("resultat").innerHTML = tutu;
        }
        
     }
    `)
};
function afficheCode1jq(){
    $("#showImage").html(`
    function s4exo1jq() {
        console.log("s4exo1jq");
        var tutu = $("#info1jq").val();
        console.log(tutu);
        var tata = $("#info2jq").val();
        console.log(tata);
        var toto = $("#info3jq").val();
        console.log(toto);
    
        if (tata != "OK" && tutu <= toto + 4) {
            tutu--;
            console.log("tutu--", tutu);
            $("#resultatjq").html(tutu);
        }
        else {
            tutu++;
            console.log("tutu++", tutu);
            $("#resultatjq").html(tutu);
        }
    
    };
    `)
};

function afficheCode1php(){
    $("#showImage").html(`

    $res= 0;
        if( isset($_POST['info1php']) && isset($_POST['info2php']) && isset($_POST['info3php'])) 
        {
        $tutu = intval($_POST['info1php']);
        $tata = $_POST['info2php'];
        $toto = intval($_POST['info3php']);
  
        if($tata != "OK" && $tutu <= $toto +4){
          $tutu--;
          $res = $tutu;
        }else
        {
          $tutu++;
          $res = $tutu;
         
  
        }
       
    } else {
     
    }

    `)
};

function afficheCode2js(){
    $("#showImage2").html(`
    function s4exo2js() {
        console.log('s4exo2js');
        var hour = document.getElementById("info2js1").value;
        console.log(hour);
        var min = document.getElementById("info2js2").value;
        console.log(min);
        min++;
        console.log(min, "2")
        if (hour < 24 && min < 60) {
            document.getElementById("resultat2").innerHTML = "Dans une minute il sera: " + hour + "h" + min + "min.";
        }
        else if (hour < 23 && min == 60) {
            hour++;
            min = 0;
            document.getElementById("resultat2").innerHTML = "Dans une minute il sera: " + hour + "h" + min + "min.";
        }
        else if (hour == 24) {
            hour = 0;
            document.getElementById("resultat2").innerHTML = "Dans une minute il sera: " + hour + "h" + min + "min.";
        }
        else {
            hour = 0;
            min = 0;
            document.getElementById("resultat2").innerHTML = "Dans une minute il sera: " + hour + "h" + min + "min.";
        }
    };
    `)
};
function afficheCode2jq(){
    $("#showImage2").html(`
    function s4exo2jq(){
        var hourJq = $("#info2jq1").val();
        var minJq = $("#info2jq2").val();
        minJq++

        if (hourJq < 23 && minJq < 60) {
        }
        else if (hourJq < 23 && minJq == 60) {
            hourJq++;
            minJq = 0;
        }
        else if (hourJq == 24) {
            hourJq = 0;
        }
        else {
            console.log("Euh...")
            hourJq = 0;
            minJq = 0;
        }
        $("#resultat2jq").html("Dans une minute il sera: " + hourJq + "h" + minJq + "min.");
    };
    `)
};

function afficheCode2php(){
    $("#showImage2").html(`
    $res2 ="";
    if(
        isset($_POST['info2php1']) && isset($_POST['info2php2']))
        {
            $hour = intval($_POST['info2php1']);
            $min =  intval($_POST['info2php2']);
    
            if ($hour < 23 && $min < 60) {  
            }
            else if ($hour < 23 && $min == 60) {
                $hour++;
                $min = 0; 
            }
            else if ($hour == 24) {
                $hour = 0;
            }
            else {
                $hour = 0;
                $min = 0;
                
            }
            $res2 = "Dans une minute il sera: " . $hour . "h" . $min + "min.";
            require 's4exercice2.html';
           
        } else{
            
        }
    
    `)
};

function afficheCode3js(){
    $("#showImage3").html(`
    function s4exo3js() {
        var hour = document.getElementById("info3js1").value;
        var min = document.getElementById("info3js2").value;
        var sec = document.getElementById("info3js3").value;
        sec++;
        if (hour < 24 && min < 60) {
        }
        else if (hour < 23 && min == 60) {
            hour++;
            min = 0;
        }
        else if (hour == 24) {
            hour = 0;
        }
        else {
            hour = 0;
            min = 0; 
        }
        if (sec < 60) {
        }
        else {
            min++;
            sec = 0;
    
            if (min == 60) {
                hour++;
                min = 0;
                if (hour < 24) {
                  
                }
                else {
                    hour = 0;
                   
                }
            }
            else {
               
            }
        }
        document.getElementById("resultat3").innerHTML = "Dans une seconde il sera: " + hour + "h " + min + "min " + sec + "secondes.";
    };
    `)
};
function afficheCode3jq(){
    $("#showImage3").html(`
    function s4exo3jq() {
        var hour = $("#info3jq1").val();
        var min = $("#info3jq2").val();
        var sec = $("#info3jq3").val();
        sec++;
    
        if (hour < 24 && min < 60) {

        }
        else if (hour < 23 && min == 60) {
            hour++;
            min = 0;
        }
        else if (hour == 24) {
            hour = 0;
        }
        else {
            hour = 0;
            min = 0; 
        }
        if (sec < 60) {
        }
        else {
            min++;
            sec = 0;
    
            if (min == 60) {
                hour++;
                min = 0;
                if (hour < 24) {
                  
                }
                else {
                    hour = 0;
                   
                }
            }
        }
        $("#resultat3jq").html("Dans une seconde il sera: " + hour + "h " + min + "min " + sec + "secondes.");
    };
    `)
};

function afficheCode3php(){
    $("#showImage3").html(`
    $res3 ="";
    
    if(isset($_POST['info3php1']) && isset($_POST['info3php2']) && isset($_POST['info3php3']))
    {
        $hour = intval($_POST['info3php1']);
        $min = intval($_POST['info3php2']);
        $sec = intval($_POST['info3php3']);

        $sec++;
        if(($hour < 23) && ($min < 60)) {  
        }
        else if ($hour < 23 && $min == 60) {
            $hour++;
            $min = 0; 
        }
        else if ($hour == 24) {
            $hour = 0;
        }
        else {
            $hour = 0;
            $min = 0;
        }
        if ($sec < 60) {
        }
        else {
            $min++;
            $sec = 0;
    
            if ($min == 60) {
                $hour++;
                $min = 0;
                if ($hour < 24) {
                  
                }
                else {
                    $hour = 0;
                   
                }
            }
        
        }
        $res3 = "Dans une minute il sera: " . $hour . "h" . $min . " min". $sec. " secondes";
        require 's4exercice3.html';
    }
    else 
    {
        require_once 'saison4.html';
        echo $_POST['info3php1'];
        echo $_POST['info3php2'];
        echo $_POST['info3php3'];
    }
    `);
};

function afficheCode4js(){
    $("#showImage4").html(`
    function s4exo4js() {
        var nbPhotoc = document.getElementById("info4js1").value;
        var prix = 0;
    
        if (nbPhotoc <= 10) {
            prix = nbPhotoc * 0.1;   
        }
        else if (nbPhotoc <= 30) {
            prix = 1 + (nbPhotoc - 10) * 0.09;
        }
        else {
            prix = 1 + 1.8 + (nbPhotoc - 30) * 0.08;
        }
        document.getElementById("resultat4").innerHTML = "Cela vous coutera " + prix + "€.";
    
    }
    `)
};
function afficheCode4jq(){
    $("#showImage4").html(`
    function s4exo4jq() {
        var nbPhotoc = $("#info4js1").value;
        var prix = 0;
    
        if (nbPhotoc <= 10) {
            prix = nbPhotoc * 0.1;   
        }
        else if (nbPhotoc <= 30) {
            prix = 1 + (nbPhotoc - 10) * 0.09;
        }
        else {
            prix = 1 + 1.8 + (nbPhotoc - 30) * 0.08;
        }
        $("#resultat4jq").html("Cela vous coutera " + prix + "€.");
    
    }
    `)
};

function afficheCode4php(){
    $("#showImage4").html(`
    if(
        isset($_POST['info4php1']))
        {
            $photocop = intval($_POST['info4php1']);
            $prix = 0;
            if ($Photoc <= 10) {
                $prix = $Photoc * 0.1;   
            }
            else if ($Photoc <= 30) {
                $prix = 1 + ($Photoc - 10) * 0.09;
            }
            else {
                $prix = 1 + 1.8 + ($Photoc - 30) * 0.08;
            }
    
            $res4 = "Cela vous coutera:" . $prix . "pour " . $photocop . "photocopies";
            require 's4exercice4.html';
    `);
};

function afficheCode5js(){
    $("#showImage5").html(`
    function s4exo5js() {
        var age = parseInt(document.getElementById("info5js1").value);
        var sexe = document.getElementById("info5js2").value;
        var condition1 = (sexe == "H") && (age > 20);
        var condition2 = (sexe == "F") && ((age >= 18) && (age <= 35));
    
        if (condition1 || condition2) {     
            document.getElementById("resultat5").innerHTML = "Vous êtes imposables.";
        }
        else {
            document.getElementById("resultat5").innerHTML = "Vous n\'êtes pas imposables";
        }
    }
    `)
};
function afficheCode5jq(){
    $("#showImage5").html(`
    function s4exo5jq() {
        var age = parseInt($("#info5jq1").val());
        var sexe = $("#info5jq2").val();
        var condition1 = (sexe == "H") && (age > 20);
        var condition2 = (sexe == "F") && ((age >= 18) && (age <= 35));
    
        if (condition1 || condition2) {
            $("#resultat5jq").html("Vous êtes imposables.");
        }
        else {
            $("#resultat5jq").html("Vous n\'êtes pas imposables");
        }
    }
    `)
};

function afficheCode5php(){
    $("#showImage5").html(`
    if(isset($_POST['info5php1']) && isset($_POST['info5php2']))
    {
        $res5 =""
        $age = $_POST['info5php1'];
        $sexe = $_POST['info5php2']
        $condition1 = ($sexe == "H") && ($age > 20);
        $condition2 = ($sexe == "F") && (($age >= 18) && ($age <= 35));
    
        if ($condition1 || $condition2) {
            $res = "Vous êtes imposables."
        }
        else {
            $res = "Vous n'êtes pas imposables."
        }
    }
    

    `);
};

function afficheCode6js(){
    $("#showImage6").html(`
    function s4exo6js() {
        c1 = document.getElementById("info6js1").value;
        c2 = document.getElementById("info6js2").value;
        c3 = document.getElementById("info6js3").value;
        c4 = document.getElementById("info6js4").value;
    
        var elu1 = c1 > 50;
        var ballotage = (c1 < 50) && (c1 >= 12.5);
        var ballotageF = (c1 > c2) && (c1 > c3) && (c1 > c4);
        var perdu = (c2 > 50) || (c3 > 50) || (c4 > 50);
        var resultat6 = document.getElementById('resultat6');
    
        if (elu1) {
            resultat6.innerHTML = "Le candidat 1 est élu au 1er tour avec plus de 50% des voix";
        }
        else if (perdu) {
            resultat6.innerHTML = "Un autre candidat a reçu plus de 50% des voix";
        }
        else if (ballotage) {
            if (ballotageF) {
                resultat6.innerHTML ="Le candidat 1 est en ballotage favorable pour le second tour";
            }
            else {
                resultat6.innerHTML ="Le candidat 1 est en ballotage défavorable pour le second tour";
            }
        }
        else {
            resultat6.innerHTML = "Le candidat perd au 1er tour avec moins de 12.5% des voix";
        }
    }
    `)
};

function afficheCode6jq(){
    $("#showImage6").html(`
    function s4exo6jq() {
        c1 = $("#info6jq1").val();
        c2 = $("#info6jq2").val();
        c3 = $("#info6jq3").val();
        c4 = $("#info6jq4").val();
    
        var elu1 = c1 > 50;
        var ballotage = (c1 < 50) && (c1 >= 12.5);
        var ballotageF = (c1 > c2) && (c1 > c3) && (c1 > c4);
        var perdu = (c2 > 50) || (c3 > 50) || (c4 > 50);
    
        if (elu1) {
            $('#resultat6jq').html("Le candidat 1 est élu au 1er tour avec plus de 50% des voix");
        }
        else if (perdu) {
            $('#resultat6jq').html("Un autre candidat a reçu plus de 50% des voix");
        }
        else if (ballotage) {
            if (ballotageF) {
                $('#resultat6jq').html("Le candidat 1 est en ballotage favorable pour le second tour");
            }
            else {
                $('#resultat6jq').html("Le candidat 1 est en ballotage défavorable pour le second tour");
            }
        }
        else {
            $('#resultat6jq').html("Le candidat perd au 1er tour avec moins de 12.5% des voix");
        }
    }
    `)
};

function afficheCode6php(){
    $("#showImage6").html(`
    $res6="";
    
    if(isset($_POST['info6php1']) && isset($_POST['info6php2']) && isset($_POST['info6php3']) && isset($_POST['info6php4']))
    {
        $c1 = intval($_POST['info6php1']);
        $c2 = intval($_POST['info6php2']);
        $c3 = intval($_POST['info6php3']);
        $c4 = intval($_POST['info6php4']);
        var_dump($c1);
        var_dump($c2);
        var_dump($c2);
        var_dump($c2);
        $elu1 = $c1 > 50;
        $ballotage = ($c1 < 50) && ($c1 >= 12.5);
        $ballotageF = ($c1 > $c2) && ($c1 > $c3) && ($c1 > $c4);
        $perdu = ($c2 > 50) || ($c3 > 50) || ($c4 > 50);

        if ($elu1) {
            $res6 = "Le candidat 1 est élu au 1er tour avec plus de 50% des voix";
        }
        else if ($perdu) {
            $res6 = "Un autre candidat a reçu plus de 50% des voix";
        }
        else if ($ballotage) {
            if ($ballotageF) {
                $res6 = "Le candidat 1 est en ballotage favorable pour le second tour";
            }
            else {
                $res6 = "Le candidat 1 est en ballotage défavorable pour le second tour";
            }
        }
        else {
            $res6 = "Le candidat perd au 1er tour avec moins de 12.5% des voix";
        }
        
    require 's4exercice6.html';
    }
    else {
        require 's4exercice6.html';
    }
    `);
};

function afficheCode7js(){
    $("#showImage7").html(`
    function s4exo7js() {
        var age = parseInt(document.getElementById("info7js1").value);
        var permis =  parseInt(document.getElementById("info7js2").value);
        var accident = parseInt(document.getElementById("info7js3").value);
        var fidelite = parseInt(document.getElementById("info7js4").value);
        console.log(age, permis, accident, fidelite);
        var assur5 = fidelite > 5;
        var age25 = age >= 25;
        var permis2 = permis >= 2;
        var situation = "";
    
    
        if (!age25 && !permis2) {
            if (accident == 0) {
                situation = "Rouge";
            }
            else {
                situation = "Refusé";
                document.getElementById("resultat7").innerHTML = situation + ": Nous ne pouvons pas vous assurer : trop d'accident pour un jeune permis";
            }
        }
        else if ((!age25 && permis2) || (age25 && !permis2)) {
            if (accident == 0) {
                situation = "Orange";
            }
            else if (accident == 1) {
                situation = "Rouge";
            }
            else {
                situation = "Réfusé";
                document.getElementById("resultat7").innerHTML = situation + "Nous ne pouvons pas vous assurer : trop d'accident ! ";
            }
            document.getElementById("resultat7").innerHTML = "Vous êtes assuré sous le régime : ." + situation;
        }
        else {
            if (accident == 0) {
                situation = "Vert";
    
            }
            else if (accident == 1) {
                situation = "Orange";
    
            }
            else if (accident == 2) {
                situation = "Rouge";
    
            }
            else {
                situation = "Refusé";
                document.getElementById("resultat7").innerHTML = situation + "Nous ne pouvons pas vous assurer."
            }
            document.getElementById("resultat7").innerHTML = "Vous êtes assuré sous le régime : ." + situation;
        }
    
        if (assur5) {
            console.log('assur5', assur5)
            if (situation == "Vert") {
                situation = "Bleu";
                document.getElementById("resultat7").innerHTML = "Grace à votre fidélité, vous pouvez être assuré sous le régime : " + situation;
            }
            else if (situation == "Orange") {
                situation = "Vert";
                document.getElementById("resultat7").innerHTML = "Grace à votre fidélité, vous pouvez être assuré sous le régime : " + situation;
            }
            else {
                situation = "Orange";
                document.getElementById("resultat7").innerHTML = "Grace à votre fidélité, vous pouvez être assuré sous le régime : " + situation;
            }
        }
        else {
            var fideliteManquant = 5 - fidelite;
            document.getElementById("resultat7").innerHTML = "Vous bénéficiez de l'offre : " + situation + ". Vous devez restez encore " + fideliteManquant + " ans pour bénéficier de notre offre fidélité.";
        }
    
    };
    `)
};

function afficheCode7jq(){
    $("#showImage7").html(`
    function s4exo7jq() {
        var age = parseInt($("#info7jq1").val());
        var permis =  parseInt($("#info7jq2").val());
        var accident = parseInt($("#info7jq3").val());
        var fidelite = parseInt($("#info7jq4").val());
        var assur5 = fidelite > 5;
        var age25 = age >= 25;
        var permis2 = permis >= 2;
        var situation = "";
    
    
        if (!age25 && !permis2) {
            if (accident == 0) {
                situation = "Rouge";
            }
            else {
                situation = "Refusé";
                $("#resultat7jq").html(situation + ": Nous ne pouvons pas vous assurer : trop d'accident pour un jeune permis");
            }
        }
        else if ((!age25 && permis2) || (age25 && !permis2)) {
            if (accident == 0) {
                situation = "Orange";
            }
            else if (accident == 1) {
                situation = "Rouge";
            }
            else {
                situation = "Réfusé";
                $("#resultat7jq").html(situation + "Nous ne pouvons pas vous assurer : trop d'accident ! ");
            }
        }
        else {
            if (accident == 0) {
                situation = "Vert";
    
            }
            else if (accident == 1) {
                situation = "Orange";
    
            }
            else if (accident == 2) {
                situation = "Rouge";
    
            }
            else {
                situation = "Refusé";
                $("#resultat7jq").html(situation + "Nous ne pouvons pas vous assurer.");
            }
        }
    
        if (assur5) {
    
            if (situation == "Vert") {
                situation = "Bleu";
                $("#resultat7jq").html("Grace à votre fidélité, vous pouvez être assuré sous le régime : " + situation);
            }
            else if (situation == "Orange") {
                situation = "Vert";
                $("#resultat7jq").html("Grace à votre fidélité, vous pouvez être assuré sous le régime : " + situation);
            }
            else {
                situation = "Orange";
                $("#resultat7jq").html("Grace à votre fidélité, vous pouvez être assuré sous le régime : " + situation);
            }
        }
        else {
            var fideliteManquant = 5 - fidelite;
            $("#resultat7jq").html("Vous bénéficiez de l'offre : " + situation + ". Vous devez restez encore " + fideliteManquant + "ans pour bénéficier de notre offre fidélité.");
        }
    
    };
    `)
};

function afficheCode7php(){
    $("#showImage7").html(`
    if(
        isset($_POST["info7php1"]) && isset($_POST["info7php2"]) && isset($_POST["info7php3"]) && isset($_POST["info7php4"])
    ){

    $age = intval($_POST[("info7php1")]);
    $permis =  intval($_POST[("info7php2")]);
    $accident = intval($_POST[("info7php3")]);
    $fidelite = intval($_POST[("info7php4")]);
    $assur5 = $fidelite > 5;
    $age25 = $age >= 25;
    $permis2 = $permis >= 2;
    $situation = "";
    $res7 = "";


    if (!$age25 && !$permis2) {
        if ($accident == 0) {
            $situation = "Rouge";
        }
        else {
            $situation = "Refusé";
            $res7 = $situation . ": Nous ne pouvons pas vous assurer : trop d'accident pour un jeune permis");
        }
    }
    else if ((!$age25 && $permis2) || ($age25 && !$permis2)) {
        if ($accident == 0) {
            $situation = "Orange";
        }
        else if ($accident == 1) {
            $situation = "Rouge";
        }
        else {
            $situation = "Réfusé";
            $res7 = ($situation . ": Nous ne pouvons pas vous assurer : trop d'accident ! ");
        }
    }
    else {
        if ($accident == 0) {
            $situation = "Vert";

        }
        else if ($accident == 1) {
            $situation = "Orange";

        }
        else if ($accident == 2) {
            $situation = "Rouge";

        }
        else {
            $situation = "Refusé";
            $res7 = $situation . ": Nous ne pouvons pas vous assurer.";
        }
    }
    if ($assur5) {
        if ($situation == "Vert") {
            $situation = "Bleu";
            $res7 = "Grace à votre fidélité, vous pouvez être assuré sous le régime : " . $situation)
        }
        else if ($situation == "Orange") {
            $situation = "Vert";
            $res7 = "Grace à votre fidélité, vous pouvez être assuré sous le régime : " . $situation;
        }
        else {
            $situation = "Orange";
            $res7 = "Grace à votre fidélité, vous pouvez être assuré sous le régime : " . $situation);
        }
        }
        else {
            $fideliteManquant = 5 - $fidelite;
            $res7 = "Vous bénéficiez de l'offre : " . $situation . " Vous devez restez encore " . $fideliteManquant . " pour bénéficier de notre offre fidélité.";
        }
        require "s4exercice7.html";
}

    `);
};

function afficheCode8js(){
    $("#showImage8").html(`
    function s4exo8js() {
        var J = parseInt(document.getElementById("info8js1").value);
        var M = parseInt(document.getElementById("info8js2").value);
        var A = parseInt(document.getElementById("info8js3").value);
    
        var bis = (A % 4 == 0 && !(A % 100 == 0)) || A % 400 == 0; // année bissextile oui
        var Jmax = 0;
        var VM = M >= 1 && M <= 12;
        
    
        if (VM) {
            // Vérifie si l'année est bissextile
            if (M == 2 && bis) {
                Jmax = 29;
                console.log(Jmax, 'bissextile')
            }
            else if (M == 2 && !bis) {
                Jmax = 28;
                console.log(Jmax, ' non bissextile')
            }
            else if ((M == 4) || (M == 6) || (M == 9) || (M == 11)) {
                Jmax = 30;
            }
            else {
                Jmax = 31;
            }
        };
        var VJ = J >= 1 && J <= Jmax;
        console.log(VJ, VM);
        if (VM && VJ) {
            document.getElementById("resultat8").innerHTML = "La date est valide : nous sommes le : " + J + "/" + M + "/" + A;
        }
        else {
            document.getElementById("resultat8").innerHTML = "La date n'est pas valide.";
        }
    }
    `)
};

function afficheCode8jq(){
    $("#showImage8").html(`
    function s4exo8jq() {
        var J = parseInt($("#info8jq1").val());
        var M = parseInt($("#info8jq2").val());
        var A = parseInt($("#info8jq3").val());
    
        var bis = (A % 4 == 0 && !(A % 100 == 0)) || A % 400 == 0; // année bissextile oui
        var VM = M >= 1 && M <= 12;
        var Jmax = 0;
    
        if (VM) {
            
            if (M == 2 && !bis) {
                Jmax = 28;
            }// Vérifie si l'année est bissextile
            else if (M == 2 && bis) {
                Jmax = 29;
            }
            else if ((M == 4) || (M == 6) || (M == 9) || (M == 11)) {
                Jmax = 30;
            }
            else {
                Jmax = 31;
            }
        }
        var VJ = J >= 1 && J <= Jmax;
        if (VM && VJ) {
            $("#resultat8jq").html("La date est valide : nous sommes le : " + J + "/" + M + "/" + A);
        }
        else {
            $("#resultat8jq").html("La date n'est pas valide.");
        }
    }
    `)
};

function afficheCode8php(){
    $("#showImage8").html(`
    $res8 = "";
    if(
        isset($_POST["info8php1"]) && isset($_POST["info8php2"]) && isset($_POST["info8php3"])
    ){

        $J = intval($_POST["info8php1"]);
        $M = intval($_POST["info8php2"]);
        $A = intval($_POST["info8php3"]);

    $bis = ($A % 4 == 0 && !($A % 100 == 0)) || $A % 400 == 0; // année bissextile oui
    $VM = $M >= 1 && $M <= 12;
    $Jmax = 0;

    if ($VM) {
        if ($M == 2 && !$bis) {
            $Jmax = 28;
        }// Vérifie si l'année est bissextile
        else if ($M == 2 && $bis) {
            $Jmax = 29;
        }
        else if (($M == 4) || ($M == 6) || ($M == 9) || ($M == 11)) {
            $Jmax = 30;
        }
        else {
            $Jmax = 31;
        }
    }
    $VJ = $J >= 1 && $J <= $Jmax;
    if ($VM && $VJ) {
       $res8 =  "La date est valide : nous sommes le : " + $Jmax + "/" + $M + "/" + $A;
       echo $res8;
    }
    else {
        $res8 = "La date n'est pas valide.";
        echo $res8;
    }
    require 's4exercice8.html';
}
    `);
};

            //FIN AFFICHAGE DU CODE DES FONCTIONS




            
            
