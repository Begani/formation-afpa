//Les fonctions exercices doivent modifier la valeur de value de select
//Changer la couleur du fond Header en fct de Js/ Jq
function s3exojq(){
    $(".modal-header").css("background-color", "green");
    var targetModal = $("#enonce3");
    var targetCorpse = $("#aResoudre3");
    var targetFooter = $("#selection");
    var HTMLmodalFooter="";
    console.log(targetModal);
    console.log(targetCorpse);
    console.log(targetFooter);

    HTMLmodalFooter += `
        <button onclick="s3exojs()" class="btn btn-lg btn-primary" id="buttonJs">Javascript</button>
        <select name="saison2List" id="saison3List" label="Saison2">
                <option value="s3e1" onclick="s3exo1()" >Exercice 1</button>  
                <option value="s3e2" onclick="s3exo2()" >Exercice 2</option>
                <option value="s3e3" onclick="s3exo3()" >Exercice 3</option>
                <option value="s3e4" onclick="s3exo4()" >Exercice 4</option>
                <option value="s3e5" onclick="s3exo5()" >Exercice 5</option>
                <option value="s3e6" onclick="s3exo6()" >Exercice 6</option>
            </select>  
        <button onclick="s3exojq()" class="btn btn-lg btn-success buttonJq"">Jquery</button>
        `
    targetFooter.html(HTMLmodalFooter);
}

function s3exojs(){
    buttonStyle = document.getElementById("couleurHeader"), 
    c=buttonStyle.style;
    c.backgroundColor = "blue";
    console.log("s3exojs executed");
    let getSelect = document.getElementById("saison3List");
    console.log("getSelect: " + getSelect);
    getOption = getSelect.options[getSelect.selectedIndex].value;
    console.log("getOption: " + getOption);
    var targetModal = document.getElementById("enonce3");
    var targetCorpse = document.getElementById("aResoudre3");
    var targetFooter = document.getElementById("selection");
   
    console.log(targetModal);
    console.log(targetFooter);
    var HTMLmodalFooter="";
    HTMLmodalFooter += `

    <button onclick="s3exojs()" class="btn btn-lg btn-primary">Javascript</button>
    <select name="saison3List" id="saison3List" label="Saison2">
            <option value="s3e1" onclick="s3exo1()" >Exercice 1</button>  
            <option value="s3e2" onclick="s3exo2()" >Exercice 2</option>
            <option value="s3e3" onclick="s3exo3()" >Exercice 3</option>
            <option value="s3e4" onclick="s3exo4()" >Exercice 4</option>
            <option value="s3e5" onclick="s3exo5()" >Exercice 5</option>
            <option value="s3e6" onclick="s3exo6()" >Exercice 6</option>
    </select>
    <button onclick="s3exojq()" class="btn btn-lg btn-success">Jquery</button>
    `
    targetFooter.innerHTML = HTMLmodalFooter;
    console.log(targetModal);
    console.log(targetCorpse);
    console.log(targetFooter);

}
       
function s3exo1(){
    var targetModal = document.getElementById("enonce3");
    var targetCorpse = document.getElementById("aResoudre3");
    var targetFooter = document.getElementById("selection");
    var HTMLmodalCorpse = ""; //Variable pour envoyer dynamiquement le HTML
    var HTMLmodalHeader = "";
    HTMLmodalHeader += "Ecrire un algorithme qui demande un nombre à l’utilisateur, et l’informe ensuite si ce nombre est positif ou négatif (on laisse de côté le cas où le nombre vaut zéro).";
    HTMLmodalCorpse +=
        `
        Variable : Nb en Numerique</br>
	    Debut </br>
		Ecrire "Entrez un entier positif ou négatif</br>
		Lire Nb</br>
        </br>
		Si Nb > 0 Alors</br>
			Ecrire "Ce nombre vaut:", Nb, ". Il est strictement positif."</br>
		   Sinon Nb < 0 Alors</br>
			Ecrire "Ce nombre vaut:", Nb, ". Il est strictement négatif."</br>
		   Fin si</br>
		Fin si</br>
        Fin</br>
        <button onclick="s3exo1algo()" class="btn btn-lg btn-primary">Essayez</button>
        <button onclick="display_s3exo1js()" class="btn btn-lg btn-primary">Afficher</button>
        `;
    targetModal.innerHTML = HTMLmodalHeader;
    targetCorpse.innerHTML = HTMLmodalCorpse;

}


function s3exo1algo(){
    
    var nb = +prompt("Entrez un nombre :")
    console.log(typeof(nb));
    if(nb>0){
        var res = alert(nb + " est un nombre strictement positif.");
    }
    else{
        var res = alert(nb + " est un nombre négatif.");
    }
  
}


function s3exo2(){
    var targetModal = document.getElementById("enonce3");
    var targetCorpse = document.getElementById("aResoudre3");
    var HTMLmodalCorpse = ""; //Variable pour envoyer dynamiquement le HTML
    var HTMLmodalHeader = "";
    HTMLmodalHeader += "Ecrire un algorithme qui demande deux nombres à l’utilisateur et l’informe ensuite si leur produit est négatif ou positif (on laisse de côté le cas où le produit est nul). Attention toutefois : on ne doit pas calculer le produit des deux nombres.";
    HTMLmodalCorpse +=
        `
        Variables : Nb, Nb1, Nb2 en Numérique</br>
	    Debut</br>
		Ecrire "Entrer un nombre:"</br>
		Lire Nb1</br>
		Ecrire "Entrer un nombre:"</br>
		Lire Nb2</br>
        </br>
		Si (Nb1>0 ET Nb2>0) OU Nb1 <0 ET Nb2 <0) Alors</br>
			Ecrire "Le produit est positif"</br>
		Sinon</br>
			Si (!=0 ET Nb2 !=0) Alors</br>
				Ecrire "le produit est négatif"</br>
			Fin si</br>
		Fin si</br>	</br>
        Fin</br>
        <button onclick="s3exo2algo()" class="btn btn-lg btn-default">Essayez</button>
        `;
    targetModal.innerHTML = HTMLmodalHeader;
    targetCorpse.innerHTML = HTMLmodalCorpse;
}


function s3exo2algo(){
    
    var  nb1 = +prompt("Entrer un nombre :");
    var nb2 = +prompt("Entrer un deuxieme nombre :");
    console.log(typeof(nb1));
    console.log(typeof(nb2));
    if((nb1>0 && nb2>0) || (nb1<0 && nb2<0)){
        alert("Le produit de ces deux nombres est positif.");
    }
    else if(nb1 != 0 && nb2 != 0){
        alert("Le produit de ces deux nombres est négatif.");
    }else{
        alert("Un des deux nombres vaut 0 !");
    }
    
}


function s3exo3(){
    var targetModal = document.getElementById("enonce3");
    var targetCorpse = document.getElementById("aResoudre3");
    var HTMLmodalCorpse = ""; //Variable pour envoyer dynamiquement le HTML
    var HTMLmodalHeader = "";
    var HTMLmodalFooter="";
    HTMLmodalHeader += "Ecrire un algorithme qui demande trois noms à l’utilisateur et l’informe ensuite s’ils sont rangés ou non dans l’ordre alphabétique.";
    HTMLmodalCorpse +=
        `
            Variables : Nom1, Nom2, Nom3 en String</br>
             Debut</br>
            Ecrire "Ecrire à la suite trois noms:"</br>
            Lire Nom1, Nom2, Nom3</br>
            </br>
            Si Nom1 < Nom2 et Nom2 < Nom3 alors</br>
                Ecrire "Vos noms sont rangés dans l'ordre alphabétique."</br>
            Sinon</br>
                Ecrire "Ces noms ne sont pas rangés dans l'ordre alphabétique."</br>
            Fin si</br>
             Fin</br>
             <button onclick="s3exo3algo()" class="btn btn-lg btn-primary">Essayez</button>

        `;
    targetModal.innerHTML = HTMLmodalHeader;
    targetCorpse.innerHTML = HTMLmodalCorpse;
    console.log(HTMLmodalFooter);
}

function s3exo3algo(){
    var nom1 = prompt("Nom1:");
    var nom2 = prompt("Nom2:");
    var nom3 = prompt("Nom3:");
    console.log(typeof(nom1));
    console.log(typeof(nom2));
    console.log(typeof(nom3));
    if(nom1<nom2 && nom2 < nom3){
        alert("Vos noms sont rangés dans l'ordre alphabétique.");
    }else{
        alert("Vos noms ne sont pas rangés dans l'ordre alphabétique.");
    }
}

function s3exo4(){
    var targetModal = document.getElementById("enonce3");
    var targetCorpse = document.getElementById("aResoudre3");
    var targetFooter = document.getElementById("selection");
    var HTMLmodalCorpse = ""; //Variable pour envoyer dynamiquement le HTML
    var HTMLmodalHeader = "";
    var HTMLmodalFooter="";
    HTMLmodalHeader += "Ecrire un algorithme qui demande un nombre à l’utilisateur, et l’informe ensuite si ce nombre est positif ou négatif (on inclut cette fois le traitement du cas où le nombre vaut zéro).";
    HTMLmodalCorpse +=
        `
            Variable : Nb en Numerique
            Debut 
            Ecrire "Entrez un entier positif ou négatif;
            Lire Nb
    
            Si Nb > 0 Alors</br>
                Ecrire "Ce nombre vaut:", Nb, ". Il est strictement positif."</br>
            SinonSi Nb < 0 Alors</br>
                Ecrire "Ce nombre vaut:", Nb, ". Il est strictement négatif."</br>
            Sinon </br>
                Ecrire "Ce nombre est nul:"</br>
            Fin si</br>
             Fin</br>
             <button onclick="s3exo4algo()" class="btn btn-lg btn-primary">Essayez</button>
        `;
    targetModal.innerHTML = HTMLmodalHeader;
    targetCorpse.innerHTML = HTMLmodalCorpse;

    console.log(HTMLmodalFooter);
}

function s3exo4algo(){
    var nb = +prompt("Entrez un nombre :")
    console.log(typeof(nb));
    if(nb>0){
        alert(nb + " est un nombre strictement positif.");
    }
    else if(nb<0){
        alert(nb + " est un nombre strictement négatif.");
    }
    else{
        alert(nb + " est nul.");
    }
}

function s3exo5(){
    var targetModal = document.getElementById("enonce3");
    var targetCorpse = document.getElementById("aResoudre3");
    var targetFooter = document.getElementById("selection");
    var HTMLmodalCorpse = ""; //Variable pour envoyer dynamiquement le HTML
    var HTMLmodalHeader = "";
    var HTMLmodalFooter="";
    

    HTMLmodalHeader += "Ecrire un algorithme qui demande deux nombres à l’utilisateur et l’informe ensuite si le produit est négatif ou positif (on inclut cette fois le traitement du cas où le produit peut être nul). Attention toutefois, on ne doit pas calculer le produit !";
    HTMLmodalCorpse +=
        `
            Variables : Nb, Nb1, Nb2 en Numérique
            Debut
            Ecrire "Entrer un nombre:"</br>
            Lire Nb1</br>
            Ecrire "Entrer un nombre:"</br>
            Lire Nb2</br>
    </br>
            Si (Nb1 == 0 OU Nb2 == 0) Alors</br>
                Ecrire "Le produit est nul."</br>
            SinonSi (Nb1>0 ET Nb2>0) OU Nb1 <0 ET Nb2 <0) Alors</br>
                Ecrire "le produit est positif</br>
            Sinon</br>
                Ecrire "Le produit est négatif."</br>
            Fin si	</br>
        Fin</br>
    <button onclick="s3exo5algo()" class="btn btn-lg btn-primary">Essayez</button>
        `;
    targetModal.innerHTML = HTMLmodalHeader;
    targetCorpse.innerHTML = HTMLmodalCorpse;
    console.log(HTMLmodalFooter);

}

function s3exo5algo(){
    var nb1 = +prompt("Entrer un nombre :");
    var nb2 = +prompt("Entrer un deuxieme nombre :");
    console.log(typeof(nb1));
    console.log(typeof(nb2));
    if((nb1>0 && nb2>0) || (nb1<0 && nb2<0)){
        alert("Le produit de ces deux nombres est positif.");
    }
    else if(nb1 != 0 && nb2 != 0){
        alert("Le produit de ces deux nombres est négatif.");
    }else{
        alert("Le produit est nul !");
    }
    
}

function s3exo6(){
    var targetModal = document.getElementById("enonce3");
    var targetCorpse = document.getElementById("aResoudre3");
    var targetFooter = document.getElementById("selection");
    var HTMLmodalCorpse = ""; //Variable pour envoyer dynamiquement le HTML
    var HTMLmodalHeader = "";
    var HTMLmodalFooter="";
    HTMLmodalHeader += 'Ecrire un algorithme qui demande l’âge d’un enfant à l’utilisateur. Ensuite, il l’informe de sa catégorie :</br>"Poussin" de 6 à 7 ans</br>"Pupille" de 8 à 9 ans</br>"Minime" de 10 à 11 ans</br>"Cadet" après 12 ans</br>Peut-on concevoir plusieurs algorithmes équivalents menant à ce résultat ?';
    HTMLmodalCorpse +=
        `
        Variable : Age en Numerique</br>
        Debut</br>
            Ecrire "Quel est ton âge?"</br>
            Lire Age</br>
            </br>
            Si (Age < 6) alors</br>
                Ecrire "Tu es trop jeune !"</br>
            SinonSi (Age>=6 ET Age <8) alors</br>
                Ecrire "Tu es un poussin"</br>
            SinonSi (Age>=8 ET Age <10) alors</br>
                Ecrire "tu es une pupille"</br>
            SinonSi (Age>=10 ET Age <12)</br>
                Ecrire "Tu es un minime"</br>
            Sinon</br>
                Ecrire "Tu es un cadet"</br>
    
            Fin Si</br>
        Fin	</br>
        <button onclick="s3exo6algo()" class="btn btn-lg btn-primary">Essayez</button>
        `;  
    targetModal.innerHTML = HTMLmodalHeader;
    targetCorpse.innerHTML = HTMLmodalCorpse;
    console.log(HTMLmodalFooter);
}

let s3exo6algo = function(){
    var age = +prompt("Quel est votre âge :");
    console.log(typeof(age));
    if(age<6){
        alert("Tu es trop jeune.");
    }
    else if(age>=6 && age<8){
        alert("Tu es un poussin.");
    }else if(age>=8 && age<10){
        alert("Tu es une pupille.");
    }else if(age>=10 && age<12){
        alert("Tu es un minime.");
    }else{
        alert("Tu es un cadet.");
    }
    
}

function display_s3exo1js(){
    var targetCorpse = document.getElementById("aResoudre3");
    var HTMLmodalCorpse = "";
    HTMLmodalCorpse +=
    `
    function s3exo1algo(){
    
        var nb = +prompt("Entrez un nombre :")
        console.log(typeof(nb));
        if(nb>0){
            var res = alert(nb + " est un nombre strictement positif.");
        }
        else{
            var res = alert(nb + " est un nombre négatif.");
        }
      
    }
    <button onclick="s3exo1()" class="btn btn-lg btn-primary">Retour</button>
    `;
    targetCorpse.innerHTML = HTMLmodalCorpse;
    
}
function display_s3exo2js(){
    var targetCorpse = document.getElementById("aResoudre2");
    var HTMLmodalCorpse = "";
    HTMLmodalCorpse +=
    `
    function s2exo4js(){</br>
        var t1 = "belle marquise";</br>
        var t2 = "vos beaux yeux";</br>
        var t3 = "me font mourrir";</br>
        var t4 = "d'amour";</br>
        </br>
        alert(t1 + t2 + t3 + t4);</br>
        alert(t2 + t3 + t4 + t1);</br>
        alert(t3 + t4 + t1 + t2);</br>
        alert(t4 + t1 + t2 + t3);</br>
    }   </br>
    <button onclick="s2exo4()" class="btn btn-lg btn-primary">Retour</button>
    `;
    targetCorpse.innerHTML = HTMLmodalCorpse;
    
}
function display_s3exo3js(){
    var targetCorpse = document.getElementById("aResoudre2");
    var HTMLmodalCorpse = "";
    HTMLmodalCorpse +=
    `
    function s2exo4js(){</br>
        var t1 = "belle marquise";</br>
        var t2 = "vos beaux yeux";</br>
        var t3 = "me font mourrir";</br>
        var t4 = "d'amour";</br>
        </br>
        alert(t1 + t2 + t3 + t4);</br>
        alert(t2 + t3 + t4 + t1);</br>
        alert(t3 + t4 + t1 + t2);</br>
        alert(t4 + t1 + t2 + t3);</br>
    }   </br>
    <button onclick="s2exo4()" class="btn btn-lg btn-primary">Retour</button>
    `;
    targetCorpse.innerHTML = HTMLmodalCorpse;
    
}
function display_s3exo4js(){
    var targetCorpse = document.getElementById("aResoudre2");
    var HTMLmodalCorpse = "";
    HTMLmodalCorpse +=
    `
    function s2exo4js(){</br>
        var t1 = "belle marquise";</br>
        var t2 = "vos beaux yeux";</br>
        var t3 = "me font mourrir";</br>
        var t4 = "d'amour";</br>
        </br>
        alert(t1 + t2 + t3 + t4);</br>
        alert(t2 + t3 + t4 + t1);</br>
        alert(t3 + t4 + t1 + t2);</br>
        alert(t4 + t1 + t2 + t3);</br>
    }   </br>
    <button onclick="s2exo4()" class="btn btn-lg btn-primary">Retour</button>
    `;
    targetCorpse.innerHTML = HTMLmodalCorpse;
    
}