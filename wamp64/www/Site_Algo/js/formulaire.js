var iKeepIndice = 0;
var aOfPersonnes = [];

aOfPersonnes[0] = [];
aOfPersonnes[0]["champJoue"] = "Leona";
aOfPersonnes[0]["versus"] = "Nautilus";
aOfPersonnes[0]["position"] = "Support";
aOfPersonnes[0]["kill"] = "4";
aOfPersonnes[0]["death"] = "2";
aOfPersonnes[0]["assist"] = "12";
aOfPersonnes[0]["kda"] =  Math.round(((parseInt(aOfPersonnes[0]["kill"]) + parseInt(aOfPersonnes[0]["assist"])) / parseInt(aOfPersonnes[0]["death"]))*100) / 100;
aOfPersonnes[0]["victoires"] = "25";
aOfPersonnes[0]["defaites"] = "43";
aOfPersonnes[0]["winRate"] = Math.round( (parseInt(aOfPersonnes[0]["victoires"]) / (parseInt(aOfPersonnes[0]["defaites"]) + parseInt(aOfPersonnes[0]["victoires"]))*100)*100)/100 + "%";

aOfPersonnes[1] = [];
aOfPersonnes[1]["champJoue"] = "Kayn";
aOfPersonnes[1]["versus"] = "Lee Sin";
aOfPersonnes[1]["position"] = "Jungle";
aOfPersonnes[1]["kill"] = "4";
aOfPersonnes[1]["death"] = "2";
aOfPersonnes[1]["assist"] = "12";
aOfPersonnes[1]["kda"] = Math.round(((parseInt(aOfPersonnes[1]["kill"]) + parseInt(aOfPersonnes[1]["assist"])) / parseInt(aOfPersonnes[1]["death"]))*100) / 100;
aOfPersonnes[1]["victoires"] = "52";
aOfPersonnes[1]["defaites"] = "43";
aOfPersonnes[1]["winRate"] = Math.round( (parseInt(aOfPersonnes[1]["victoires"]) / (parseInt(aOfPersonnes[1]["defaites"]) + parseInt(aOfPersonnes[1]["victoires"]))*100)*100)/100 + "%";


aOfPersonnes[2] = [];
aOfPersonnes[2]["champJoue"] = "Senna";
aOfPersonnes[2]["versus"] = "Lucian";
aOfPersonnes[2]["position"] = "ADC";
aOfPersonnes[2]["kill"] = "4";
aOfPersonnes[2]["death"] = "2";
aOfPersonnes[2]["assist"] = "22";
aOfPersonnes[2]["kda"] = Math.round(((parseInt(aOfPersonnes[2]["kill"]) + parseInt(aOfPersonnes[2]["assist"])) / parseInt(aOfPersonnes[2]["death"]))*100) / 100;
aOfPersonnes[2]["victoires"] = "52";
aOfPersonnes[2]["defaites"] = "21";
aOfPersonnes[2]["winRate"] = Math.round( (parseInt(aOfPersonnes[2]["victoires"]) / (parseInt(aOfPersonnes[2]["defaites"]) + parseInt(aOfPersonnes[2]["victoires"]))*100)*100)/100 + "%";


aOfPersonnes[3] = [];
aOfPersonnes[3]["champJoue"] = "Wukong";
aOfPersonnes[3]["versus"] = "Renekton";
aOfPersonnes[3]["position"] = "Top";
aOfPersonnes[3]["kill"] = "4";
aOfPersonnes[3]["death"] = "3";
aOfPersonnes[3]["assist"] = "33";
aOfPersonnes[3]["kda"] = Math.round(((parseInt(aOfPersonnes[iKeepIndice]["kill"]) + parseInt(aOfPersonnes[iKeepIndice]["assist"])) / parseInt(aOfPersonnes[iKeepIndice]["death"]))*100) / 100;
aOfPersonnes[3]["victoires"] = "52";
aOfPersonnes[3]["defaites"] = "102";
aOfPersonnes[3]["winRate"] = Math.round( (parseInt(aOfPersonnes[3]["victoires"]) / (parseInt(aOfPersonnes[3]["defaites"]) + parseInt(aOfPersonnes[3]["victoires"]))*100)*100)/100 + "%";

function constructTable() {
    var i;

    var sHTML = "";
    sHTML += "<thead>";
    sHTML += "<tr role='row'>";
    sHTML += "<th class=\"sorting_asc\"  tabindex=\"0\" aria-controls=\"example\" rowspan=\"1\" colspan=\"1\" style=\"width: 9%;\" aria-sort=\"ascending\" aria-label=\"Champion: activate to sort column descending\">Champion</td>";
    sHTML += "<th class=\"sorting\" tabindex=\"0\" aria-controls=\"example\" rowspan=\"1\" colspan=\"1\" style=\"width: 9%;\" aria-label=\"Versus: activate to sort column ascending\">Versus</td>";
    sHTML += "<th class=\"sorting\" tabindex=\"0\" aria-controls=\"example\" rowspan=\"1\" colspan=\"1\" style=\"width: 9%;\" aria-label=\"Position: activate to sort column ascending\">Position</td>";
    sHTML += "<th class=\"sorting\" tabindex=\"0\" aria-controls=\"example\" rowspan=\"1\" colspan=\"1\" style=\"width: 9%;\" aria-label=\"Kill: activate to sort column ascending\">Kill</td>";
    sHTML += "<th class=\"sorting\" tabindex=\"0\" aria-controls=\"example\" rowspan=\"1\" colspan=\"1\" style=\"width: 9%;\" aria-label=\"Death: activate to sort column ascending\">Death</td>";
    sHTML += "<th class=\"sorting\" tabindex=\"0\" aria-controls=\"example\" rowspan=\"1\" colspan=\"1\" style=\"width: 9%;\" aria-label=\"Assist: activate to sort column ascending\">Assist</td>";
    sHTML += "<th class=\"sorting\" tabindex=\"0\" aria-controls=\"example\" rowspan=\"1\" colspan=\"1\" style=\"width: 9%;\" aria-label=\"KDA: activate to sort column ascending\">KDA</td>";
    sHTML += "<th class=\"sorting\" tabindex=\"0\" aria-controls=\"example\" rowspan=\"1\" colspan=\"1\" style=\"width: 9%;\" aria-label=\"Victoires: activate to sort column ascending\">Victoires</td>";
    sHTML += "<th class=\"sorting\" tabindex=\"0\" aria-controls=\"example\" rowspan=\"1\" colspan=\"1\" style=\"width: 9%;\" aria-label=\"Défaites: activate to sort column ascending\">Défaites</td>";
    sHTML += "<th class=\"sorting\" tabindex=\"0\" aria-controls=\"example\" rowspan=\"1\" colspan=\"1\" style=\"width: 9%;\" aria-label=\"Winrate: activate to sort column ascending\">Winrate</td>";
    sHTML += "<th>Editer</td>";
    sHTML += "<th>Supprimer</td>";
    sHTML += "</tr>";
    sHTML += "</thead>";
    sHTML += "<tbody>";

    for (i = 0; i < aOfPersonnes.length; i++) {
        sHTML += "<tr>";
        sHTML += "<td class=\"bg-success\">" + aOfPersonnes[i]["champJoue"] + "</td>";
        sHTML += "<td>" + aOfPersonnes[i]["versus"] + "</td>";
        sHTML += "<td>" + aOfPersonnes[i]["position"] + "</td>";
        sHTML += "<td>" + aOfPersonnes[i]["kill"] + "</td>";
        sHTML += "<td class=\"bg-danger\">" + aOfPersonnes[i]["death"] + "</td>";
        sHTML += "<td>" + aOfPersonnes[i]["assist"] + "</td>";
        sHTML += "<td class=\"bg-success\">" + aOfPersonnes[i]["kda"] + "</td>";
        sHTML += "<td>" + aOfPersonnes[i]["victoires"] + "</td>";
        sHTML += "<td>" + aOfPersonnes[i]["defaites"] + "</td>";
        sHTML += "<td class=\"bg-success\">" + aOfPersonnes[i]["winRate"] + "</td>";
        sHTML += "<td><a href=\"#modif\" class='btn btn-primary' onClick=\"editStat(" + i + ")\">Editer</a></td>";
        sHTML += "<td><a href=\"#modif\" class='btn btn-danger' onClick=\"startSuppr(" + i + ")\">Supprimer</a></td>";
        sHTML += "</tr>";
    }

    sHTML += "</tbody>";
    $('#table_champions').html(sHTML);
}

function ajoutStat() {
    var iLongueur = aOfPersonnes.length;
    aOfPersonnes[iLongueur] = [];
    aOfPersonnes[iLongueur]["champJoue"] = $('#champ').val();
    aOfPersonnes[iLongueur]["versus"] = $('#versus').val();
    aOfPersonnes[iLongueur]["position"] = $('#position').val();
    aOfPersonnes[iLongueur]["kill"] = $('#kill').val();
    aOfPersonnes[iLongueur]["death"] = $('#death').val();
    aOfPersonnes[iLongueur]["assist"] = $('#assist').val();
    aOfPersonnes[iLongueur]["kda"] = Math.round(((parseInt(aOfPersonnes[iLongueur]["kill"]) + parseInt(aOfPersonnes[iLongueur]["assist"])) / parseInt(aOfPersonnes[iLongueur]["death"]))*100) / 100;
    aOfPersonnes[iLongueur]["victoires"] = $('#victoires').val();
    aOfPersonnes[iLongueur]["defaites"] = $('#defaites').val();
    aOfPersonnes[iLongueur]["winRate"] = Math.round( (parseInt(aOfPersonnes[iLongueur]["victoires"]) / (parseInt(aOfPersonnes[iLongueur]["defaites"]) + parseInt(aOfPersonnes[iLongueur]["victoires"]))*100)*100)/100 + "%";
    tables.clear();
    tables.destroy();
    constructTable();
    tables = $('#table_champions').DataTable(configuration);
    
}
//Variable pour stocker l'indice du tableau à modifier récupéré par editStat

function majStat() {
    console.log(iKeepIndice, 'Indice à Modifier')
    aOfPersonnes[iKeepIndice]["champJoue"] = $('#champ').val();
    aOfPersonnes[iKeepIndice]["versus"] = $('#versus').val();
    aOfPersonnes[iKeepIndice]["position"] = $('#position').val();
    aOfPersonnes[iKeepIndice]["kill"] = $('#kill').val();
    aOfPersonnes[iKeepIndice]["death"] = $('#death').val();
    aOfPersonnes[iKeepIndice]["assist"] = $('#assist').val();
    aOfPersonnes[iKeepIndice]["kda"] = Math.round(((parseInt(aOfPersonnes[iKeepIndice]["kill"]) + parseInt(aOfPersonnes[iKeepIndice]["assist"])) / parseInt(aOfPersonnes[iKeepIndice]["death"]))*100) / 100;
    aOfPersonnes[iKeepIndice]["victoires"] = $('#victoires').val();
    aOfPersonnes[iKeepIndice]["defaites"] = $('#defaites').val();
    aOfPersonnes[iKeepIndice]["winRate"] = Math.round( (parseInt(aOfPersonnes[iKeepIndice]["victoires"]) / (parseInt(aOfPersonnes[iKeepIndice]["defaites"]) + parseInt(aOfPersonnes[iKeepIndice]["victoires"]))*100)*100)/100 + "%";
    tables.clear();
    tables.destroy();
    constructTable();
    tables = $('#table_champions').DataTable(configuration);
    $('#btn_ajouter').show();
    $('#btn_modifier').hide();
    $('#btn_supprimer').hide();
}

function supprimStat() {
    if(confirm('Etes vous sûr de vous supprimer le contenu du champion ' + aOfPersonnes[iKeepIndice]['champJoue'])){
        aOfPersonnes.splice(iKeepIndice, 1);
        constructTable();
        $('#btn_ajouter').show();
        $('#btn_modifier').hide();
        $('#btn_supprimer').hide();
    }
    else{
        $('#btn_ajouter').show();
        $('#btn_modifier').hide();
        $('#btn_supprimer').hide();
    }
    tables.clear();
    tables.destroy();
    constructTable();
    tables = $('#table_champions').DataTable(configuration);
}

function editStat(iIndiceEdit) {
    iKeepIndice = iIndiceEdit;
    console.log(iKeepIndice, "Indice récupéré");
    $('#champ').val(aOfPersonnes[iIndiceEdit]["champJoue"]);
    $('#versus').val(aOfPersonnes[iIndiceEdit]["versus"]);
    $('#position').val(aOfPersonnes[iIndiceEdit]["position"]);
    $('#kill').val(aOfPersonnes[iIndiceEdit]["kill"]);
    $('#death').val(aOfPersonnes[iIndiceEdit]["death"]);
    $('#assist').val(aOfPersonnes[iIndiceEdit]["assist"]);
    $('#victoires').val(aOfPersonnes[iIndiceEdit]["victoires"]);
    $('#defaites').val(aOfPersonnes[iIndiceEdit]["defaites"]);
    //affichage
    $('#btn_ajouter').hide();
    $('#btn_supprimer').hide();
    $('#btn_modifier').show();
}

function startSuppr(iIndiceEdit) {
    iKeepIndice = iIndiceEdit;
    $('#btn_ajouter').hide();
    $('#btn_modifier').hide();
    $('#btn_supprimer').show();
}

// CONFIGURATION DATATABLE
const configuration = {
    "stateSave": false,
    "order": [[2, "asc"]],
    "pagingType": "simple_numbers",
    "searching": true,
    "lengthChange": true,
    "lengthMenu": [[10, 25, 50, -1], [10, 25, 50, "Tous"]],
    "language": {
        "info": "Champions _START_ à _END_ sur _TOTAL_ sélectionné(s)",
        "emptyTable": "Aucun utilisateur",
        "lengthMenu": "_MENU_ Champions par page",
        "search": "Rechercher : ",
        "zeroRecords": "Aucun résultat de recherche",
        "paginate": {
            "previous": "Précédent",
            "next": "Suivant"
        },
        "sInfoFiltered": "(filtr&eacute; de _MAX_ &eacute;l&eacute;ments au total)",
        "sInfoEmpty": "Champion 0 à 0 sur 0 sélectionné",
    },
    "columns": [
        {
            "orderable": true
        },
        {
            "orderable": true
        },
        {
            "orderable": true
        },
        {
            "orderable": true
        },
        {
            "orderable": true
        },
        {
            "orderable": true
        },
        {
            "orderable": true
        },
        {
            "orderable": true
        },
        {
            "orderable": true
        },
        {
            "orderable": true
        },
        {
            "orderable": false
        },
        {
            "orderable": false
        },
    ],
    'retrieve': true,
    'responsive' : true,
    'dom' : 'Bfrtip',
    'buttons' : ['columnsToggle'],
    

};


var tables;
$(document).ready(function () {
    // INIT DATATABLE
    // Si je souhaite avoir par défaut autre que les 10 résultats par défaut au chargement
    // tables.page.len(10).draw();
    constructTable();
    tables = $('#table_champions').DataTable(configuration);
});
