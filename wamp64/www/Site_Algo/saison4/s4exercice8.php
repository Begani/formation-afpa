<?php

    $res8 = "";
    if(
        isset($_POST["info8php1"]) && isset($_POST["info8php2"]) && isset($_POST["info8php3"])
    ){
       
        $J = intval($_POST["info8php1"]);
        $M = intval($_POST["info8php2"]);
        $A = intval($_POST["info8php3"]);

        $bis = ($A % 4 == 0 && !($A % 100 == 0)) || $A % 400 == 0; // année bissextile oui
        $VM = $M >= 1 && $M <= 12;
        $Jmax = 0;

        if ($VM) {
            if ($M == 2 && !$bis) {
                $Jmax = 28;
            }// Vérifie si l'année est bissextile
            else if ($M == 2 && $bis) {
                $Jmax = 29;
            }
            else if (($M == 4) || ($M == 6) || ($M == 9) || ($M == 11)) {
                $Jmax = 30;
            }
            else {
                $Jmax = 31;
            }
        }
        $VJ = $J >= 1 && $J <= $Jmax;
        if ($VM && $VJ) {
        $res8 =  "La date est valide : nous sommes le : " . $J . "/" . $M . "/" . $A;
        
        }
        else {
            $res8 = "La date n'est pas valide.";
            
        }
        
        require 's4exercice8.html';
    }
    else
    {
        require 's4exercice8.html';

    }

?>