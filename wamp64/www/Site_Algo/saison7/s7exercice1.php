
<?php 
    $sMessage7_1 = "";
    $sHTML7_1 = "";
    $addInput7_1 = "";
    $nbNoteInt7_1 = 0;
    $saisie = 0;
    $nbInput7_1_php = 0;


    if(!isset($_POST['nbInput7_1_php'])){
        require 's7exercice1.html';
    }
    else if( !isset($_POST['newInput7_1_php0']) && isset($_POST['nbInput7_1_php'] ))
    {
        
    $saisie = $_POST['nbInput7_1_php'];
        //Crée un nb d'input égale à saisie
        for($i=0; $i<$saisie; $i++)
        {
            $addInput7_1 .= '<input type="number" id="newInput7_1_php'. $i .'" name="newInput7_1_php'. $i .'" placeholder="Nombre ' . $i . '" >';
            $nbNoteInt7_1++;
        }
        require 's7exercice1.html';
    }

    else
    {
        $aNumbers = [];
        $nbNote8 = $_POST['nbNumber7_1_php'];
    //chaque valeur récupérée est insérée entre valPlusPetite et ValeurPlusGrandes
        for($j = 0; $j < $nbNote8 ; $j++)
        {   
            $getNumber = $_POST["newInput7_1_php" . $j];
            $iPosTrouve = -1;
            //Si tableau est vide, ajout 1er élément
            if(count($aNumbers) == 0){
                $aNumbers[$j] = $getNumber;
            }
            //Si tableau contient 1 ou plusieurs valeurs
            else if(count($aNumbers)>= 1){

                for($k=0; $k <= count($aNumbers) - 1; $k++){
                    
                    if($getNumber < $aNumbers[$k] && $iPosTrouve == -1)
                    {
                        $iPosTrouve = $k;
                        $iNumTrouve = $getNumber;

                        for($l=(count($aNumbers)); $l > $iPosTrouve; $l--)
                        {
                            $aNumbers[$l] = $aNumbers[$l-1];
                        }
                        $aNumbers[$iPosTrouve] = $iNumTrouve;
                    }
                    else if($getNumber > $aNumbers[count($aNumbers) - 1] && $iPosTrouve == -1)
                    {
                        $aNumbers[$j] = $getNumber;
                    }
                }
                
            }
        }
        for($m=0; $m < count($aNumbers)-1; $m++ )
        {
            $bDiff = ($aNumbers[$m + 1] - $aNumbers[$m]) == 1;
            if($bDiff === true){
                $sMessage7_1 .= "1.";
            }
            else if($bDiff === false){
                $sMessage7_1 .= "0.";
            }

        }
  
       
 
        $pos = strpos($sMessage7_1, "0.");
        if($pos === false)
        {
            $sHTML7_1 = "Les nombres sont consécutifs !";
        }
        else
        {
            $sHTML7_1 = "Les nombres ne sont pas consécutifs !";
        }
    
    require 's7exercice1.html';
    }
?>