<?php 

    $sMessage7_4 = "";
    $aDico = ['ane', 'belette', 'chèvre', 'dindon', 'elephant', 'faon', 'girafe', 'hibou', 'iguane', 'jaguar', 'koala', 'lama', 'léopard', 'macaque', 'nyctale de Tengmalm', 'ours', 'panda', 'quokka', 'ragondin', 'serpent', 'truite', 'urubu à tête rouge', 'vautour', 'wallaby', 'xerus', 'yack', 'zébu'];
    $sMot = "";
    $Sup = count($aDico) - 1;
    $Inf = 0;
    $bTrouve = false;


    if(isset($_POST['Input7_4_php'])){
        $sMot = $_POST['Input7_4_php'];
        while($bTrouve != true)
        {
            $Middle = round(($Sup + $Inf)/2);
            if($sMot > $aDico[$Middle])
            {
                $Inf = $Middle + 1; //Inf prend la valeur de la moitié du tableau, Sup reste le max
            }
            else
            {
                $Sup = $Middle - 1;//Inf prend la valeur de la moitié du tableau, Inf reste le min
            }

            if(($sMot == $aDico[$Middle]) || ($Sup < $Inf))
            {
                $bTrouve = true;
            }
        }
        if($sMot == $aDico[$Middle])
        {
            $sMessage7_4 = "Le mot existe, il est en position : " . $Middle;
        }
        else
        {
            $sMessage7_4 = "Le mot n'existe pas !";
        }
        
        require 's7exercice4.html';
    }
    else
    {
        require 's7exercice4.html';
    }

?>