<?php
    $sText = "";
    $A = [1, 4, 10, 15, 17];
    $B = [0, 2, 5, 8, 9, 12, 16];
    $C = [];
    $iA = 0;
    $iB = 0;
    $iC = -1;

    //Booleen qui vérifie si on a récupérer toutes les valeurs de chaque tableau
    $bAFini = false;
    $bBFini = false;
    //Evite le passage dans B si B est fini mais que les valeurs de A sont > à la dernière valeur de B
    $bCheat = true;

    while($iC <= count($B)-1 + count($A)-1){
        $iC++;
        //Si A ne contient plus de valeur on passe ici ou si B n'est pas fini et que B[indiceB] > A[indiceA]
        //Alors c'est B qui est injecté dans tableau C
        if($bAFini == true || ($bCheat && ($A[$iA] > $B[$iB])))
        {
            $C[$iC] = $B[$iB];
            $iB++;
            //Si l'indice est plus grand que la taille du tableau alors B a été complètement injecté dans C
            if($iB > count($B)-1){
                $bBFini = true;
                $bCheat = false;
            }
        }
        else if($bBFini == true || $A[$iA] < $B[$iB])
        {
            $C[$iC] = $A[$iA];
            $iA++;

            
            if($iA > count($A)-1){
                $bAFini = true;
            }
        }
    }
    for($i=0; $i < count($C); $i++)
    {
       $sText .= ' | ' . $C[$i];
    }
    $sText = $sText . ' |';
    
    require 's7exercice5.html';
?>