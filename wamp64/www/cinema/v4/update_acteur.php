<?php

	require "configuration.php";
	$GLOBALS_INI= getGlobalsINI();

	$version= "v4";
	require "database_v4.php";

	$numero_de_connexion= connectBDD($GLOBALS_INI["DB_HOST"], $GLOBALS_INI["DB_NAME"], $GLOBALS_INI["DB_LOGIN"], $GLOBALS_INI["DB_PSW"]);

	$spathSQL= $GLOBALS_INI["PATH_HOME"] . $GLOBALS_INI["PATH_SQL"] . "update_acteur.sql";
	$resultat= treatDatas($numero_de_connexion, $spathSQL, array(
																"id_acteur" => $_POST["id_acteur"], 
																"nom_acteur" => $_POST["nom_acteur"], 
																"prenom_acteur" => $_POST["prenom_acteur"], 
																"nationalite_acteur" => $_POST["nationalite_acteur"],
																"age_acteur" => $_POST["age_acteur"]
																));
	disconnectBDD($numero_de_connexion);
	
	require $GLOBALS_INI["PATH_HOME"] . $GLOBALS_INI["PATH_FILES"] . $version . "/update_acteur.html";

?>




