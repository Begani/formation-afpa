<?php
require "acteur_service.php";

/**
* Class Liste_acteur | file liste_acteur.php
*
* In this class, we show the interface "liste_acteur.html".
* With this interface, we'll be able to list all the actors stored in database
*
* List of classes needed for this class
*
* require "acteur_service.php";
*
* @package Cinema Project
* @subpackage configuration
* @author @Afpa Lab Team
* @copyright  1920-2080 The Afpa Lab Team Group Corporation World Company
* @version v1.0
*/
class Liste_acteur	{
	
	/**
	* public $resultat is used to store all datas needed for HTML Templates
	* @var array
	*/
	public $resultat;

	/**
	* init variables resultat
	*
	* execute main function
	*/
	public function __construct()	{
		// init variables resultat
		$this->resultat= [];

		// execute main function
		$this->main();
	}

	/**
	* Get list of all actors
	*/
	function main()	{
		// List 'em all !!
		$obj_liste_acteur= new acteur_service();
		$obj_liste_acteur->liste_acteur();
		
		// Get elements for the view
		$this->resultat= $obj_liste_acteur->resultat;
		$this->VARS_HTML= $obj_liste_acteur->VARS_HTML;
		
		// kill object
		unset($obj_liste_acteur);
	}
}

?>
