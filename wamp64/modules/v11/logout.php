<?php
/**
 * Class Logout | file Logout.php
 *
 * In this class, we show the interface "Logout.html".
 * With this interface, we'll be able to add a new movie
 *
 * @package Cinema Project
 * @subpackage Logout
 * @author @Afpa Lab Team
 * @copyright  1920-2080 The Afpa Lab Team Group Corporation World Company
 * @version v1.0
 */
class Logout	{
	
	/**
	 * public $resultat is used to store all datas needed for HTML Templates
	 * @var array
	 */
	public $resultat;

	/**
	 * init variables resultat
	 *
	 * execute main function
	 */
	public function __construct()	{
		// init variables resultat
		$this->resultat= [];

		// execute main function
		$this->main();
	}

	/**
	 * Login in
	 */
	function main()	{
		$_SESSION= array();
		session_destroy();
		header('location: route.php?page=login');
	}
}

?>
