<?php
require "initialize.php";

/**
* Class acteur_service | file acteur_service.php
*
* In this class, we have methods for :
* - adding an actor with method save_acteur()
* - updating an actor with method update_acteur()
* - deleting an actor with method supprime_acteur()
* - listing all actors with method liste_acteur()
* - editing an actor with method edit_acteur()
* With this interface, we'll be able to list all the actors stored in database
*
* List of classes needed for this class
*
* require_once "acteur_service.php";
*
* @package Cinema Project
* @subpackage configuration
* @author @Afpa Lab Team
* @copyright  1920-2080 The Afpa Lab Team Group Corporation World Company
* @version v1.0
*/
class Acteur_service extends Initialize	{
	
    /**
    * public $resultat is used to store all datas needed for HTML Templates
    * @var array
    */
    public $resultat;

    /**
    * Call the parent constructor
    *
    * init variables resultat
    */
    public function __construct()	{
        // Call Parent Constructor
        parent::__construct();

        // init variables resultat
        $this->resultat= [];
    }

    /**
    * Call the parent destructor
    */
    public function __destruct()	{
        // Call Parent destructor
        parent::__destruct();
    }

    /**
    * Method save_acteur()
    *
    * Insert a new actor in database
    */
    public function save_acteur()	{
        // Here I can Access to :
        // $this->GLOBALS_INI
        // $this->oBdd
        // $this->VARS_HTML
        $spathSQL= $this->GLOBALS_INI["PATH_HOME"] . $this->GLOBALS_INI["PATH_SQL"] . "insert_acteur.sql";
        $this->resultat["save_acteur"]= $this->oBdd->treatDatas($spathSQL, array(
                                                                                "nom_acteur" => $VARS_HTML["nom_acteur"], 
                                                                                "prenom_acteur" => $VARS_HTML["prenom_acteur"], 
                                                                                "nationalite_acteur" => $VARS_HTML["nationalite_acteur"],
                                                                                "age_acteur" => $VARS_HTML["age_acteur"]
                                                                    ));
        $this->resultat["id_acteur_created"]= $this->oBdd->getLastInsertId();
    }
	
    /**
    * Method liste_acteur()
    *
    * List all actors in database
    */
    public function liste_acteur()	{
        $spathSQL= $this->GLOBALS_INI["PATH_HOME"] . $this->GLOBALS_INI["PATH_SQL"] . "select_acteur.sql";
        $this->resultat["liste_acteur"]= $this->oBdd->getSelectDatas($spathSQL, array());
    }

    /**
    * Method edit_acteur()
    *
    * Edit the actor with param id_acteur from the database
    */
    public function edit_acteur(){
        // Here I can Access to :
        // $this->GLOBALS_INI
        // $this->oBdd
        // $this->VARS_HTML
        $spathSQL= $this->GLOBALS_INI["PATH_HOME"] . $this->GLOBALS_INI["PATH_SQL"] . "select_acteur_single.sql";
        $this->resultat["edit_acteur"]= $this->oBdd->getSelectDatas($spathSQL, array( "id_acteur" => $this->VARS_HTML["id_acteur"] ));

    }

    /**
    * Method supprime_acteur()
    *
    * Delete a actor in database
    */
    public function supprime_acteur(){
        // Here I can Access to :
        // $this->GLOBALS_INI
        // $this->oBdd
        // $this->VARS_HTML
        $spathSQL= $this->GLOBALS_INI["PATH_HOME"] . $this->GLOBALS_INI["PATH_SQL"] . "delete_acteur.sql";
        $this->resultat= $this->oBdd->treatDatas($spathSQL, array(
                                                                    "id_acteur" => $this->VARS_HTML["id_acteur"]
                                                                    ));

    }

    /**
    * Method update_acteur()
    *
    * Update the actor with param id_acteur in database
    */
    public function update_acteur(){
        // Here I can Access to :
        // $this->GLOBALS_INI
        // $this->oBdd
        // $this->VARS_HTML
        $spathSQL= $this->GLOBALS_INI["PATH_HOME"] . $this->GLOBALS_INI["PATH_SQL"] . "update_acteur.sql";
        $this->resultat= $this->oBdd->treatDatas($spathSQL, array(
                                                                "id_acteur" => $VARS_HTML["id_acteur"], 
                                                                "nom_acteur" => $VARS_HTML["nom_acteur"], 
                                                                "prenom_acteur" => $VARS_HTML["prenom_acteur"], 
                                                                "nationalite_acteur" => $VARS_HTML["nationalite_acteur"],
                                                                "age_acteur" => $VARS_HTML["age_acteur"]
                                                                    ));

    }
}

?>
