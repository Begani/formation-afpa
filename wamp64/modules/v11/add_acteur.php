<?php
/**
* Class Add_acteur | file add_acteur.php
*
* In this class, we show the interface "add_acteur.html".
* With this interface, we'll be able to add a new actor
*
* @package Cinema Project
* @subpackage configuration
* @author @Afpa Lab Team
* @copyright  1920-2080 The Afpa Lab Team Group Corporation World Company
* @version v1.0
*/


class Add_acteur	{
	
	/**
	* public $resultat is used to store all datas needed for HTML Templates
	* @var array
	*/
	public $resultat;

	/**
	* init variables resultat
	*
	* execute main function
	*/
	public function __construct()	{
		// init variables resultat
		$this->resultat= [];

		// execute main function
		$this->main();
	}

	/**
	* Get interface to add
	*/
	function main()	{
	}
}

?>
