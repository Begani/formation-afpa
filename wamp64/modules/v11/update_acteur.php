<?php
require "acteur_service.php";

/**
* Class Update_acteur | file Update_acteur.php
*
* In this class, we show the interface "Update_acteur.html".
* With this interface, we'll be able to update a movie with its id
*
* @package Cinema Project
* @subpackage configuration
* @author @Afpa Lab Team
* @copyright  1920-2080 The Afpa Lab Team Group Corporation World Company
* @version v1.0
*/
class Update_acteur	{
	
	/**
	* public $resultat is used to store all datas needed for HTML Templates
	* @var array
	*/
	public $resultat;

	/**
	* init variables resultat
	*
	* execute main function
	*/
	public function __construct()	{
		// init variables resultat
		$this->resultat= [];

		// execute main function
		$this->main();
	}

	/**
	* Update a movie with its id
	*/
	function main()	{
		$objet_update_acteur = new acteur_service();
		$objet_update_acteur->update_acteur();

		$this->resultat= $objet_update_acteur->resultat;
	}
}
?>
