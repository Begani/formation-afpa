<?php
require_once "film_service.php";

/**
 * Class Save_film | file Save_film.php
 *
 * In this class, we show the interface "Save_film.html".
 * With this interface, we'll be able to save a new movie
 *
 * @package Cinema Project
 * @subpackage configuration
 * @author @Afpa Lab Team
 * @copyright  1920-2080 The Afpa Lab Team Group Corporation World Company
 * @version v1.0
 */
class Save_film	{
	
	/**
	 * public $resultat is used to store all datas needed for HTML Templates
	 * @var array
	 */
	public $resultat;

	/**
	 * init variables resultat
	 *
	 * execute main function
	 */
	public function __construct()	{
		// init variables resultat
		$this->resultat= [];

		// execute main function
		$this->main();
	}

	/**
	 * Add a movie in the database
	 */
	function main()	{
		// Create a new film
		$obj_save_film = new Film_service();
		$obj_save_film->save_film();

		$this->resultat= $obj_save_film->resultat;
		var_dump($this->resultat);
		$this->VARS_HTML= $obj_save_film->VARS_HTML;
	}
}

?>
