#------------------------------------------------------------
#        Script MySQL.
#------------------------------------------------------------


#------------------------------------------------------------
# Table: Client
#------------------------------------------------------------

CREATE TABLE Client(
        id_client Int  Auto_increment  NOT NULL ,
        nom       Varchar (100) NOT NULL ,
        prenom    Varchar (100) NOT NULL ,
        adresse   Varchar (255) NOT NULL ,
        ville     Varchar (255) NOT NULL ,
        cp        Integer NOT NULL ,
        pays      Varchar (50) NOT NULL ,
        tel       Integer NOT NULL ,
        email     Varchar (255) NOT NULL
	,CONSTRAINT Client_PK PRIMARY KEY (id_client)
)ENGINE=InnoDB;


#------------------------------------------------------------
# Table: Catégorie
#------------------------------------------------------------

CREATE TABLE Categorie(
        id_categorie   Int  Auto_increment  NOT NULL ,
        code_categorie Varchar (11) NOT NULL ,
        description    Varchar (255) NOT NULL
	,CONSTRAINT Categorie_PK PRIMARY KEY (id_categorie)
)ENGINE=InnoDB;


#------------------------------------------------------------
# Table: Classe
#------------------------------------------------------------

CREATE TABLE Classe(
        id_classe        Int  Auto_increment  NOT NULL ,
        nbre_etoiles     Integer NOT NULL ,
        caracteristiques Varchar (255) NOT NULL
	,CONSTRAINT Classe_PK PRIMARY KEY (id_classe)
)ENGINE=InnoDB;


#------------------------------------------------------------
# Table: Hotel
#------------------------------------------------------------

CREATE TABLE Hotel(
        id_hotel      Int  Auto_increment  NOT NULL ,
        num_hotel     Varchar (11) NOT NULL ,
        nom_hotel     Varchar (100) NOT NULL ,
        adresse_hotel Varchar (255) NOT NULL ,
        cp_hotel      Integer NOT NULL ,
        id_classe     Int NOT NULL
	,CONSTRAINT Hotel_PK PRIMARY KEY (id_hotel)

	,CONSTRAINT Hotel_Classe_FK FOREIGN KEY (id_classe) REFERENCES Classe(id_classe)
)ENGINE=InnoDB;


#------------------------------------------------------------
# Table: Chambre
#------------------------------------------------------------

CREATE TABLE Chambre(
        id_chambre    Int  Auto_increment  NOT NULL ,
        num_chambre   Integer NOT NULL ,
        tel_chambre   Integer NOT NULL ,
        disponibilite Integer NOT NULL ,
        id_hotel      Int NOT NULL
	,CONSTRAINT Chambre_PK PRIMARY KEY (id_chambre)

	,CONSTRAINT Chambre_Hotel_FK FOREIGN KEY (id_hotel) REFERENCES Hotel(id_hotel)
)ENGINE=InnoDB;


#------------------------------------------------------------
# Table: Réservation
#------------------------------------------------------------

CREATE TABLE Reservation(
        id_reservation    Int  Auto_increment  NOT NULL ,
        num_reservation   Varchar (11) NOT NULL ,
        date_debut        Date NOT NULL ,
        date_fin          Date NOT NULL ,
        date_payes_arrhes Date NOT NULL ,
        montant_arrhes    Integer NOT NULL ,
        id_chambre        Int NOT NULL ,
        id_client         Int NOT NULL
	,CONSTRAINT Reservation_PK PRIMARY KEY (id_reservation)

	,CONSTRAINT Reservation_Chambre_FK FOREIGN KEY (id_chambre) REFERENCES Chambre(id_chambre)
	,CONSTRAINT Reservation_Client0_FK FOREIGN KEY (id_client) REFERENCES Client(id_client)
)ENGINE=InnoDB;


#------------------------------------------------------------
# Table: Prestation
#------------------------------------------------------------

CREATE TABLE Prestation(
        id_prestation Int  Auto_increment  NOT NULL ,
        code          Varchar (11) NOT NULL ,
        designation   Varchar (255) NOT NULL
	,CONSTRAINT Prestation_PK PRIMARY KEY (id_prestation)
)ENGINE=InnoDB;


#------------------------------------------------------------
# Table: Facture
#------------------------------------------------------------

CREATE TABLE Facture(
        id_facture      Int  Auto_increment  NOT NULL ,
        num_facture     Varchar (11) NOT NULL ,
        montant_facture Integer NOT NULL ,
        date_facture    Date NOT NULL ,
        id_client       Int NOT NULL
	,CONSTRAINT Facture_PK PRIMARY KEY (id_facture)

	,CONSTRAINT Facture_Client_FK FOREIGN KEY (id_client) REFERENCES Client(id_client)
)ENGINE=InnoDB;


#------------------------------------------------------------
# Table: prestation__client
#------------------------------------------------------------

CREATE TABLE prestation__client(
        id_presta_client      Int  Auto_increment NOT NULL ,
        id_prestation         Int NOT NULL ,
        id_client             Int NOT NULL ,
        date_heure_prestation Datetime NOT NULL
	,CONSTRAINT prestation__client_PK PRIMARY KEY (id_presta_client,id_prestation,id_client)

	,CONSTRAINT prestation__client_Prestation_FK FOREIGN KEY (id_prestation) REFERENCES Prestation(id_prestation)
	,CONSTRAINT prestation__client_Client0_FK FOREIGN KEY (id_client) REFERENCES Client(id_client)
)ENGINE=InnoDB;

#------------------------------------------------------------
# Table: prestation__hotel
#------------------------------------------------------------

CREATE TABLE prestation__hotel(
        id_prestation   Int NOT NULL ,
        id_hotel        Int NOT NULL ,
        prix_prestation Integer NOT NULL
	,CONSTRAINT prestation__hotel_PK PRIMARY KEY (id_prestation,id_hotel)

	,CONSTRAINT prestation__hotel_Prestation_FK FOREIGN KEY (id_prestation) REFERENCES Prestation(id_prestation)
	,CONSTRAINT prestation__hotel_Hotel0_FK FOREIGN KEY (id_hotel) REFERENCES Hotel(id_hotel)
)ENGINE=InnoDB;


#------------------------------------------------------------
# Table: categorie__classe__chambre
#------------------------------------------------------------

CREATE TABLE categorie__classe__chambre(
        id_chambre   Int NOT NULL ,
        id_categorie Int NOT NULL ,
        id_classe    Int NOT NULL ,
        prix_chambre Integer NOT NULL
	,CONSTRAINT categorie__classe__chambre_PK PRIMARY KEY (id_chambre,id_categorie,id_classe)

	,CONSTRAINT categorie__classe__chambre_Chambre_FK FOREIGN KEY (id_chambre) REFERENCES Chambre(id_chambre)
	,CONSTRAINT categorie__classe__chambre_Categorie0_FK FOREIGN KEY (id_categorie) REFERENCES Categorie(id_categorie)
	,CONSTRAINT categorie__classe__chambre_Classe1_FK FOREIGN KEY (id_classe) REFERENCES Classe(id_classe)
)ENGINE=InnoDB;
