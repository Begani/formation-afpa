-- phpMyAdmin SQL Dump
-- version 4.9.2
-- https://www.phpmyadmin.net/
--
-- Hôte : 127.0.0.1:3308
-- Généré le :  lun. 18 mai 2020 à 16:33
-- Version du serveur :  8.0.18
-- Version de PHP :  7.3.12

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de données :  `exam7`
--

-- --------------------------------------------------------

--
-- Structure de la table `categorie`
--

DROP TABLE IF EXISTS `categorie`;
CREATE TABLE IF NOT EXISTS `categorie` (
  `id_categorie` int(11) NOT NULL AUTO_INCREMENT,
  `code_categorie` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL,
  `description` varchar(255) NOT NULL,
  PRIMARY KEY (`id_categorie`)
) ENGINE=InnoDB AUTO_INCREMENT=10 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

--
-- Déchargement des données de la table `categorie`
--

INSERT INTO `categorie` (`id_categorie`, `code_categorie`, `description`) VALUES
(1, 'Suite Royale', 'Belle Batisse'),
(2, 'Suite', 'Bien Belle Batisse mais moins que la suite royale'),
(3, 'Chambre 5 étoiles', 'Chambre 5 étoiles'),
(4, 'Chambre 4 étoiles', 'Chambre 4 étoiles'),
(5, 'Chambre 3 étoiles', 'Chambre 3 étoiles'),
(6, 'Chambre 2 étoiles', 'Chambre 2 étoiles'),
(7, 'Chambre 1 étoiles', 'Chambre 1 étoiles'),
(8, 'F1', 'F1'),
(9, 'F2', 'F2');

-- --------------------------------------------------------

--
-- Structure de la table `categorie__classe__chambre`
--

DROP TABLE IF EXISTS `categorie__classe__chambre`;
CREATE TABLE IF NOT EXISTS `categorie__classe__chambre` (
  `id_chambre` int(11) NOT NULL,
  `id_categorie` int(11) NOT NULL,
  `id_classe` int(11) NOT NULL,
  `prix_chambre` int(11) NOT NULL,
  PRIMARY KEY (`id_chambre`,`id_categorie`,`id_classe`),
  KEY `categorie__classe__chambre_Categorie0_FK` (`id_categorie`),
  KEY `categorie__classe__chambre_Classe1_FK` (`id_classe`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

--
-- Déchargement des données de la table `categorie__classe__chambre`
--

INSERT INTO `categorie__classe__chambre` (`id_chambre`, `id_categorie`, `id_classe`, `prix_chambre`) VALUES
(11, 2, 2, 50),
(12, 3, 3, 75),
(15, 2, 2, 100),
(17, 1, 1, 150);

-- --------------------------------------------------------

--
-- Structure de la table `chambre`
--

DROP TABLE IF EXISTS `chambre`;
CREATE TABLE IF NOT EXISTS `chambre` (
  `id_chambre` int(11) NOT NULL AUTO_INCREMENT,
  `num_chambre` int(11) NOT NULL,
  `tel_chambre` int(11) NOT NULL,
  `disponibilite` int(11) NOT NULL,
  `id_hotel` int(11) NOT NULL,
  PRIMARY KEY (`id_chambre`),
  KEY `Chambre_Hotel_FK` (`id_hotel`)
) ENGINE=InnoDB AUTO_INCREMENT=19 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

--
-- Déchargement des données de la table `chambre`
--

INSERT INTO `chambre` (`id_chambre`, `num_chambre`, `tel_chambre`, `disponibilite`, `id_hotel`) VALUES
(10, 1, 101, 1, 1),
(11, 2, 102, 0, 1),
(12, 1, 201, 0, 2),
(13, 2, 202, 0, 2),
(14, 1, 301, 0, 3),
(15, 1, 401, 0, 4),
(16, 1, 501, 0, 5),
(17, 2, 502, 0, 5),
(18, 1, 601, 0, 6);

-- --------------------------------------------------------

--
-- Structure de la table `classe`
--

DROP TABLE IF EXISTS `classe`;
CREATE TABLE IF NOT EXISTS `classe` (
  `id_classe` int(11) NOT NULL AUTO_INCREMENT,
  `nbre_etoiles` varchar(4) NOT NULL,
  `caracteristiques` varchar(255) NOT NULL,
  PRIMARY KEY (`id_classe`)
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

--
-- Déchargement des données de la table `classe`
--

INSERT INTO `classe` (`id_classe`, `nbre_etoiles`, `caracteristiques`) VALUES
(1, '****', 'Super Classe'),
(2, '***', 'Classe'),
(3, '**', 'Moins Classe'),
(4, '*', 'Bof');

-- --------------------------------------------------------

--
-- Structure de la table `client`
--

DROP TABLE IF EXISTS `client`;
CREATE TABLE IF NOT EXISTS `client` (
  `id_client` int(11) NOT NULL AUTO_INCREMENT,
  `nom` varchar(100) NOT NULL,
  `prenom` varchar(100) NOT NULL,
  `adresse` varchar(255) NOT NULL,
  `ville` varchar(255) NOT NULL,
  `cp` int(11) NOT NULL,
  `pays` varchar(50) NOT NULL,
  `tel` int(11) NOT NULL,
  `email` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL,
  PRIMARY KEY (`id_client`)
) ENGINE=InnoDB AUTO_INCREMENT=325 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

--
-- Déchargement des données de la table `client`
--

INSERT INTO `client` (`id_client`, `nom`, `prenom`, `adresse`, `ville`, `cp`, `pays`, `tel`, `email`) VALUES
(152, 'Lepaté', 'André', 'random152', 'ville152', 15200, 'France', 405042152, 'client152@afpa.fr'),
(187, 'Gaudreau', 'Jessamine', '38 rue Michel Ange', 'Le Havre', 76600, 'France', 244135177, 'JessamineGaudreau@dayrep.com'),
(324, 'Bowen', 'Abbie', 'Rostsestraat 16', 'Freloux', 4347, 'Belgique', 486169571, 'AbbieBowen@teleworm.us');

-- --------------------------------------------------------

--
-- Structure de la table `facture`
--

DROP TABLE IF EXISTS `facture`;
CREATE TABLE IF NOT EXISTS `facture` (
  `id_facture` int(11) NOT NULL AUTO_INCREMENT,
  `num_facture` varchar(11) NOT NULL,
  `montant_facture` int(11) NOT NULL,
  `date_facture` date NOT NULL,
  `id_client` int(11) NOT NULL,
  PRIMARY KEY (`id_facture`),
  KEY `Facture_Client_FK` (`id_client`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

--
-- Déchargement des données de la table `facture`
--

INSERT INTO `facture` (`id_facture`, `num_facture`, `montant_facture`, `date_facture`, `id_client`) VALUES
(1, 'H2C12CL152', 152, '2020-05-25', 152),
(2, 'H1C10CL187', 64, '2020-06-24', 187),
(3, 'H4C15CL324', 55, '2020-01-13', 324);

-- --------------------------------------------------------

--
-- Structure de la table `hotel`
--

DROP TABLE IF EXISTS `hotel`;
CREATE TABLE IF NOT EXISTS `hotel` (
  `id_hotel` int(11) NOT NULL AUTO_INCREMENT,
  `num_hotel` varchar(11) NOT NULL,
  `nom_hotel` varchar(100) NOT NULL,
  `adresse_hotel` varchar(255) NOT NULL,
  `cp_hotel` int(11) NOT NULL,
  `id_classe` int(11) NOT NULL,
  PRIMARY KEY (`id_hotel`),
  KEY `Hotel_Classe_FK` (`id_classe`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

--
-- Déchargement des données de la table `hotel`
--

INSERT INTO `hotel` (`id_hotel`, `num_hotel`, `nom_hotel`, `adresse_hotel`, `cp_hotel`, `id_classe`) VALUES
(1, '1001', 'Malibu', 'adresseH1', 1, 2),
(2, '1002', 'Palazio', 'adresseH2', 2, 1),
(3, '1003', 'Tokyo', 'adresseH3', 3, 3),
(4, '1004', 'Mercure', 'adresseH4', 4, 4),
(5, '1005', 'Bondodo', 'adresseH5', 5, 4),
(6, '1006', 'PetitSommeil', 'adresseH6', 6, 2);

-- --------------------------------------------------------

--
-- Structure de la table `prestation`
--

DROP TABLE IF EXISTS `prestation`;
CREATE TABLE IF NOT EXISTS `prestation` (
  `id_prestation` int(11) NOT NULL AUTO_INCREMENT,
  `code` varchar(11) NOT NULL,
  `designation` varchar(255) NOT NULL,
  PRIMARY KEY (`id_prestation`)
) ENGINE=InnoDB AUTO_INCREMENT=26 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

--
-- Déchargement des données de la table `prestation`
--

INSERT INTO `prestation` (`id_prestation`, `code`, `designation`) VALUES
(10, 'Bière', '1664'),
(15, 'Pepsi', 'Cola'),
(20, 'Nettoyage', 'Nettoyage'),
(25, 'Repas', 'RepasMidi');

-- --------------------------------------------------------

--
-- Structure de la table `prestation__client`
--

DROP TABLE IF EXISTS `prestation__client`;
CREATE TABLE IF NOT EXISTS `prestation__client` (
  `id_presta_client` int(11) NOT NULL AUTO_INCREMENT,
  `id_prestation` int(11) NOT NULL,
  `id_client` int(11) NOT NULL,
  `date_heure_prestation` datetime NOT NULL,
  PRIMARY KEY (`id_presta_client`,`id_prestation`,`id_client`),
  KEY `prestation__client_Prestation_FK` (`id_prestation`),
  KEY `prestation__client_Client0_FK` (`id_client`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

--
-- Déchargement des données de la table `prestation__client`
--

INSERT INTO `prestation__client` (`id_presta_client`, `id_prestation`, `id_client`, `date_heure_prestation`) VALUES
(1, 10, 152, '2020-05-21 11:45:00'),
(2, 10, 152, '2020-05-21 15:20:00'),
(3, 15, 187, '2020-06-20 17:22:00'),
(4, 20, 187, '2020-06-24 20:30:00'),
(5, 25, 324, '2020-01-11 12:15:00');

-- --------------------------------------------------------

--
-- Structure de la table `prestation__hotel`
--

DROP TABLE IF EXISTS `prestation__hotel`;
CREATE TABLE IF NOT EXISTS `prestation__hotel` (
  `id_prestation` int(11) NOT NULL,
  `id_hotel` int(11) NOT NULL,
  `prix_prestation` int(11) NOT NULL,
  PRIMARY KEY (`id_prestation`,`id_hotel`),
  KEY `prestation__hotel_Hotel0_FK` (`id_hotel`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

--
-- Déchargement des données de la table `prestation__hotel`
--

INSERT INTO `prestation__hotel` (`id_prestation`, `id_hotel`, `prix_prestation`) VALUES
(10, 1, 10),
(10, 2, 15),
(15, 1, 25),
(20, 1, 12),
(25, 4, 30);

-- --------------------------------------------------------

--
-- Structure de la table `reservation`
--

DROP TABLE IF EXISTS `reservation`;
CREATE TABLE IF NOT EXISTS `reservation` (
  `id_reservation` int(11) NOT NULL AUTO_INCREMENT,
  `num_reservation` varchar(11) NOT NULL,
  `date_debut` date NOT NULL,
  `date_fin` date NOT NULL,
  `date_payes_arrhes` date NOT NULL,
  `montant_arrhes` int(11) NOT NULL,
  `id_chambre` int(11) NOT NULL,
  `id_client` int(11) NOT NULL,
  PRIMARY KEY (`id_reservation`),
  KEY `Reservation_Chambre_FK` (`id_chambre`),
  KEY `Reservation_Client0_FK` (`id_client`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

--
-- Déchargement des données de la table `reservation`
--

INSERT INTO `reservation` (`id_reservation`, `num_reservation`, `date_debut`, `date_fin`, `date_payes_arrhes`, `montant_arrhes`, `id_chambre`, `id_client`) VALUES
(1, '1', '2020-05-20', '2020-05-25', '2020-05-17', 25, 12, 152),
(2, '2', '2020-06-18', '2020-06-24', '2020-05-16', 52, 10, 187),
(3, '3', '2020-01-10', '2020-01-13', '2020-01-05', 15, 15, 324);

--
-- Contraintes pour les tables déchargées
--

--
-- Contraintes pour la table `categorie__classe__chambre`
--
ALTER TABLE `categorie__classe__chambre`
  ADD CONSTRAINT `categorie__classe__chambre_Categorie0_FK` FOREIGN KEY (`id_categorie`) REFERENCES `categorie` (`id_categorie`),
  ADD CONSTRAINT `categorie__classe__chambre_Chambre_FK` FOREIGN KEY (`id_chambre`) REFERENCES `chambre` (`id_chambre`),
  ADD CONSTRAINT `categorie__classe__chambre_Classe1_FK` FOREIGN KEY (`id_classe`) REFERENCES `classe` (`id_classe`);

--
-- Contraintes pour la table `chambre`
--
ALTER TABLE `chambre`
  ADD CONSTRAINT `Chambre_Hotel_FK` FOREIGN KEY (`id_hotel`) REFERENCES `hotel` (`id_hotel`);

--
-- Contraintes pour la table `facture`
--
ALTER TABLE `facture`
  ADD CONSTRAINT `Facture_Client_FK` FOREIGN KEY (`id_client`) REFERENCES `client` (`id_client`);

--
-- Contraintes pour la table `hotel`
--
ALTER TABLE `hotel`
  ADD CONSTRAINT `Hotel_Classe_FK` FOREIGN KEY (`id_classe`) REFERENCES `classe` (`id_classe`);

--
-- Contraintes pour la table `prestation__client`
--
ALTER TABLE `prestation__client`
  ADD CONSTRAINT `prestation__client_Client0_FK` FOREIGN KEY (`id_client`) REFERENCES `client` (`id_client`),
  ADD CONSTRAINT `prestation__client_Prestation_FK` FOREIGN KEY (`id_prestation`) REFERENCES `prestation` (`id_prestation`);

--
-- Contraintes pour la table `prestation__hotel`
--
ALTER TABLE `prestation__hotel`
  ADD CONSTRAINT `prestation__hotel_Hotel0_FK` FOREIGN KEY (`id_hotel`) REFERENCES `hotel` (`id_hotel`),
  ADD CONSTRAINT `prestation__hotel_Prestation_FK` FOREIGN KEY (`id_prestation`) REFERENCES `prestation` (`id_prestation`);

--
-- Contraintes pour la table `reservation`
--
ALTER TABLE `reservation`
  ADD CONSTRAINT `Reservation_Chambre_FK` FOREIGN KEY (`id_chambre`) REFERENCES `chambre` (`id_chambre`),
  ADD CONSTRAINT `Reservation_Client0_FK` FOREIGN KEY (`id_client`) REFERENCES `client` (`id_client`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
