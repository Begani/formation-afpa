-- phpMyAdmin SQL Dump
-- version 4.9.2
-- https://www.phpmyadmin.net/
--
-- Hôte : 127.0.0.1:3308
-- Généré le :  jeu. 07 mai 2020 à 14:25
-- Version du serveur :  8.0.18
-- Version de PHP :  7.3.12

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de données :  `tpavion`
--

-- --------------------------------------------------------

--
-- Structure de la table `affectevol`
--

DROP TABLE IF EXISTS `affectevol`;
CREATE TABLE IF NOT EXISTS `affectevol` (
  `Pasnum` int(11) NOT NULL COMMENT 'cle etrangere passager',
  `Volnum` int(11) NOT NULL COMMENT 'clé étrangere vol',
  `DateVol` date NOT NULL,
  `NumPlace` int(11) DEFAULT NULL,
  `Prix` int(11) DEFAULT NULL,
  PRIMARY KEY (`Pasnum`,`Volnum`,`DateVol`),
  KEY `Pasnum` (`Pasnum`),
  KEY `Volnum` (`Volnum`)
) ENGINE=INNODB DEFAULT CHARSET=utf8;

--
-- Déchargement des données de la table `affectevol`
--

INSERT INTO `affectevol` (`Pasnum`, `Volnum`, `DateVol`, `NumPlace`, `Prix`) VALUES
(3, 103, '2008-11-05', 23, 135),
(5, 100, '2008-11-01', 55, 180),
(6, 100, '2008-11-01', 54, 180),
(7, 100, '2008-11-01', 66, 180),
(1, 103, '2008-11-05', 12, 135);

-- --------------------------------------------------------

--
-- Structure de la table `avion`
--

DROP TABLE IF EXISTS `avion`;
CREATE TABLE IF NOT EXISTS `avion` (
  `Avnum` int(11) NOT NULL,
  `Marque` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL,
  `TypeAvion` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL,
  `Capacite` int(11) DEFAULT NULL,
  `Localisation` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL,
  `DateMiseEnService` date DEFAULT NULL,
  PRIMARY KEY (`Avnum`)
) ENGINE=INNODB DEFAULT CHARSET=utf8;

--
-- Déchargement des données de la table `avion`
--

INSERT INTO `avion` (`Avnum`, `Marque`, `TypeAvion`, `Capacite`, `Localisation`, `DateMiseEnService`) VALUES
(100, 'Airbus', 'A320', 381, 'Nice', '1987-03-20'),
(101, 'Boeing', 'B707', 250, 'Paris', '1984-02-27'),
(102, 'Airbus', 'A320', 522, 'Toulouse', '1988-01-24'),
(103, 'Caravelle', 'Caravelle', 240, 'Toulouse', '1964-01-01'),
(104, 'Boeing', 'B747', 400, 'Paris', '1988-01-01'),
(105, 'Airbus', 'A320', 423, 'Grenoble', '1998-05-01'),
(106, 'ATR', 'ATR42', 50, 'Paris', '1990-01-01'),
(107, 'Boeing', 'B727', 300, 'Lyon', '1988-01-01'),
(108, 'Boeing', 'B727', 300, 'Nantes', '1988-01-01'),
(109, 'Airbus', 'A340', 350, 'Bastia', '1995-01-01'),
(120, 'Caravelle', 'Caravelle', 240, 'Grenoble', '1960-01-01'),
(150, 'Airbus', 'A340', 345, 'Brive', '2000-01-01'),
(151, 'Boeing', 'B707', 250, 'Bastia', '1986-02-02'),
(155, 'Airbus', 'A340', 600, 'Toulouse', '1998-06-03'),
(160, 'Airbus', 'A340', 600, 'Paris', '1988-02-06');

-- --------------------------------------------------------

--
-- Structure de la table `passager`
--

DROP TABLE IF EXISTS `passager`;
CREATE TABLE IF NOT EXISTS `passager` (
  `Pasnum` int(11) NOT NULL,
  `Nom` varchar(50) DEFAULT NULL,
  `Prenom` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL,
  `Ville` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL,
  PRIMARY KEY (`Pasnum`)
) ENGINE=INNODB DEFAULT CHARSET=utf8;

--
-- Déchargement des données de la table `passager`
--

INSERT INTO `passager` (`Pasnum`, `Nom`, `Prenom`, `Ville`) VALUES
(1, 'MAUSSE', 'Fabien', 'Toulouse'),
(2, 'MERLHIOT', 'Pascal', 'Paris'),
(3, 'JEAN', 'Patrick', 'Nice'),
(4, 'PEREIRA', 'Joao', 'Limoges'),
(5, 'FREEMAN', 'Cathy', 'Paris'),
(6, 'MINETTE', 'Sophie', 'Grenoble'),
(7, 'MALHERBE', 'Fred', 'Lyon'),
(8, 'FERDINAND', 'Gilles', 'Fort De France'),
(9, 'BOST', 'Vincent', 'Brive');

-- --------------------------------------------------------

--
-- Structure de la table `pilote`
--

DROP TABLE IF EXISTS `pilote`;
CREATE TABLE IF NOT EXISTS `pilote` (
  `Pilnum` int(11) NOT NULL AUTO_INCREMENT,
  `Nom` varchar(50) DEFAULT NULL,
  `CodePostal` char(5) DEFAULT NULL,
  `Ville` varchar(50) DEFAULT NULL,
  `DateNaissance` date DEFAULT NULL,
  `DateDebutActivite` date DEFAULT NULL,
  `DateFinActivite` date DEFAULT NULL,
  `SalaireBrut` varchar(10) DEFAULT NULL,
  PRIMARY KEY (`Pilnum`)
) ENGINE=InnoDB AUTO_INCREMENT=22 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

--
-- Déchargement des données de la table `pilote`
--

INSERT INTO `pilote` (`Pilnum`, `Nom`, `CodePostal`, `Ville`, `DateNaissance`, `DateDebutActivite`, `DateFinActivite`, `SalaireBrut`) VALUES
(1, 'Serge', '37000', 'Tours', '1955-01-13', '1980-01-01', NULL, '1829.40'),
(2, 'Jean', '75010', 'Paris', '1955-11-07', '1978-02-01', NULL, '8766.00'),
(3, 'Roger', '38000', 'Grenoble', '1960-03-01', '1990-04-01', NULL, '2439.14'),
(4, 'Robert', '44000', 'Nantes', '1960-03-03', '1993-06-01', NULL, '5686.45'),
(5, 'Michel', '75010', 'Paris', '1956-11-08', '2000-01-01', NULL, '2744.00'),
(7, 'Bertrand', '69001', 'Lyon', '1962-02-02', '1988-01-11', NULL, '6791.67'),
(8, 'Hervé', '20000', 'Bastia', '1960-01-01', '1987-01-01', NULL, '3811.11'),
(9, 'Luc', '75018', 'Paris', '1956-11-07', '1985-01-01', NULL, '7581.40'),
(19, 'Driss', '75006', 'Paris', '1956-05-16', '1990-12-01', NULL, '7502.90'),
(20, 'Sylvain', '31000', 'Toulouse', '1975-11-08', '2000-01-01', NULL, '4709.39'),
(21, 'Serge', '31000', 'Toulouse', '1965-07-30', '1995-10-03', NULL, '5540.43');

-- --------------------------------------------------------

--
-- Structure de la table `vol`
--

DROP TABLE IF EXISTS `vol`;
CREATE TABLE IF NOT EXISTS `vol` (
  `Volnum` int(11) NOT NULL,
  `NumAvion` int(11) DEFAULT NULL COMMENT 'clé étrangere avion',
  `NumPilote` int(11) DEFAULT NULL COMMENT 'clé étrangere pilote',
  `VilleDepart` varchar(50) DEFAULT NULL,
  `VilleArrivee` varchar(50) DEFAULT NULL,
  `HeureDepart` decimal(10,0) DEFAULT NULL,
  `HeureArrivee` decimal(10,0) DEFAULT NULL,
  PRIMARY KEY (`Volnum`),
  KEY `FK_Vol_Avion` (`NumAvion`),
  KEY `FK_NumPilote_Pilnum` (`NumPilote`)
) ENGINE=INNODB DEFAULT CHARSET=utf8;

--
-- Déchargement des données de la table `vol`
--

INSERT INTO `vol` (`Volnum`, `NumAvion`, `NumPilote`, `VilleDepart`, `VilleArrivee`, `HeureDepart`, `HeureArrivee`) VALUES
(100, 100, 1, 'Nice', 'Paris', '7', '10'),
(101, 100, 2, 'Paris', 'Toulouse', '11', '12'),
(102, 101, 1, 'Paris', 'Nice', '12', '14'),
(103, 105, 3, 'Grenoble', 'Toulouse', '9', '11'),
(104, 105, 3, 'Toulouse', 'Grenoble', '17', '19'),
(105, 107, 7, 'Lyon', 'Paris', '6', '7'),
(106, 109, 8, 'Bastia', 'Paris', '10', '13'),
(107, 106, 9, 'Paris', 'Brive', '7', '8'),
(108, 106, 9, 'Brive', 'Paris', '19', '20'),
(109, 107, 7, 'Paris', 'Lyon', '18', '19'),
(110, 102, 2, 'Toulouse', 'Paris', '15', '16'),
(111, 108, 5, 'Nice', 'Paris', '14', '16'),
(112, 109, 2, 'Bastia', 'Paris', '10', '13'),
(113, 105, 2, 'Toulouse', 'Grenoble', '17', '19'),
(114, 150, 2, 'Paris', 'Marseille', '10', '12'),
(115, 155, 2, 'Paris', 'Lille', '11', '12'),
(116, 101, 4, 'Nice', 'Nantes', '17', '19'),
(714, 104, 1, 'Moulinsart', 'Sydney', '1', '23');
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
