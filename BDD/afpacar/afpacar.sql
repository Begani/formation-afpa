#------------------------------------------------------------
#        Script MySQL.
#------------------------------------------------------------


#------------------------------------------------------------
# Table: ville
#------------------------------------------------------------

CREATE TABLE ville(
        id_ville    Int  Auto_increment  NOT NULL ,
        code_postal Varchar (5) NOT NULL ,
        ville       Varchar (20) NOT NULL
	,CONSTRAINT ville_PK PRIMARY KEY (id_ville)
)ENGINE=InnoDB;


#------------------------------------------------------------
# Table: couleur
#------------------------------------------------------------

CREATE TABLE couleur(
        id_couleur      Int  Auto_increment  NOT NULL ,
        code_couleur    Varchar(6) NULL ,
        libelle_couleur Varchar(15) NOT NULL
	,CONSTRAINT couleur_PK PRIMARY KEY (id_couleur)
)ENGINE=InnoDB;


#------------------------------------------------------------
# Table: marque
#------------------------------------------------------------

CREATE TABLE marque(
        id_marque      Int  Auto_increment  NOT NULL ,
        libelle_marque Varchar(15) NOT NULL
	,CONSTRAINT marque_PK PRIMARY KEY (id_marque)
)ENGINE=InnoDB;


#------------------------------------------------------------
# Table: modele
#------------------------------------------------------------

CREATE TABLE modele(
        id_modele      Int  Auto_increment  NOT NULL ,
        libelle_modele Varchar(20) NOT NULL
	,CONSTRAINT modele_PK PRIMARY KEY (id_modele)
)ENGINE=InnoDB;


#------------------------------------------------------------
# Table: droit
#------------------------------------------------------------

CREATE TABLE droit(
        id_droit      Int  Auto_increment  NOT NULL ,
        libelle_droit Varchar (50) NOT NULL ,
        statut_droit  TinyINT NOT NULL
	,CONSTRAINT droit_PK PRIMARY KEY (id_droit)
)ENGINE=InnoDB;


#------------------------------------------------------------
# Table: utilisateur
#------------------------------------------------------------

CREATE TABLE utilisateur(
        id_utilisateur             Int  Auto_increment  NOT NULL ,
        nom_utilisateur            Varchar (50) NOT NULL ,
        prenom_utilisateur         Varchar (50) NOT NULL ,
        url_photo_utilisateur      Varchar (50) NOT NULL ,
        n_beneficiaire_utilisateur Integer NOT NULL ,
        date_naissance_utilisateur Date NOT NULL ,
        mpd_utilisateur            Varchar (30) NOT NULL ,
        mail_utilisateur           Varchar (150) NOT NULL ,
        telephone_utilisateur      Varchar (10) NOT NULL ,
        date_fin_utilisateur       Date NOT NULL ,
        permis_utilisateur         TinyInt NOT NULL ,
        accept_cg_utilisateur      TinyInt NOT NULL ,
		desactive_utilisateur	   TinyInt NOT NULL
	,CONSTRAINT utilisateur_PK PRIMARY KEY (id_utilisateur)
)ENGINE=InnoDB;


#------------------------------------------------------------
# Table: fonctionnalite
#------------------------------------------------------------

CREATE TABLE fonctionnalite(
        id_fonctionnalite      Int  Auto_increment  NOT NULL ,
        libelle_fonctionnalite Varchar (50) NOT NULL ,
        statut_fonctionnalite  TinyInt NOT NULL
	,CONSTRAINT fonctionnalite_PK PRIMARY KEY (id_fonctionnalite)
)ENGINE=InnoDB;


#------------------------------------------------------------
# Table: droit__utilisateur__fonctionnalite
#------------------------------------------------------------

CREATE TABLE droit__utilisateur__fonctionnalite(
        id_fonctionnalite Int NOT NULL ,
        id_droit          Int NOT NULL ,
        id_utilisateur    Int NOT NULL
	,CONSTRAINT droit__utilisateur__fonctionnalite_PK PRIMARY KEY (id_fonctionnalite,id_droit,id_utilisateur)

	,CONSTRAINT droit__utilisateur__fonctionnalite_fonctionnalite_FK FOREIGN KEY (id_fonctionnalite) REFERENCES fonctionnalite(id_fonctionnalite)
	,CONSTRAINT droit__utilisateur__fonctionnalite_droit_FK FOREIGN KEY (id_droit) REFERENCES droit(id_droit)
	,CONSTRAINT droit__utilisateur__fonctionnalite_utilisateur_FK FOREIGN KEY (id_utilisateur) REFERENCES utilisateur(id_utilisateur)
)ENGINE=InnoDB;



#------------------------------------------------------------
# Table: avis
#------------------------------------------------------------

CREATE TABLE avis(
        id_avis                        Int  Auto_increment  NOT NULL ,
        id_emetteur                    Int NOT NULL ,
        id_destinataire                Int NOT NULL ,
        type_expediteur                TinyInt NOT NULL ,
        date_avis                      Date NOT NULL ,
        texte_avis                     Varchar (255) NOT NULL ,
        note_avis                      Integer NOT NULL ,
        moderation_avis                TinyINT NOT NULL DEFAULT 0 ,
        motif_refus_avis               Varchar (255) NULL ,
        signalement_destinataire       TinyINT NOT NULL DEFAULT 0 ,
        motif_signalement_destinataire Varchar (255) NULL
	,CONSTRAINT avis_PK PRIMARY KEY (id_avis)
	
	,CONSTRAINT avis_utilisateur_emetteur_FK FOREIGN KEY (id_emetteur) REFERENCES utilisateur(id_utilisateur)
	,CONSTRAINT avis_utilisateur_destinataire_FK FOREIGN KEY (id_destinataire) REFERENCES utilisateur(id_utilisateur)
)ENGINE=InnoDB;


#------------------------------------------------------------
# Table: message
#------------------------------------------------------------

CREATE TABLE message(
        id_message              Int  Auto_increment  NOT NULL ,
        id_emetteur             Int NOT NULL ,
        id_destinataire         Int NOT NULL ,
        contenu_message         Varchar (200) NOT NULL ,
        dateheure_envoi_message Datetime NOT NULL ,
        statut_modere_message   TinyINT NOT NULL DEFAULT 0 ,
        comment_modere_message  Varchar (200) NULL
	,CONSTRAINT message_PK PRIMARY KEY (id_message)

	,CONSTRAINT message_utilisateur_emetteur_FK FOREIGN KEY (id_emetteur) REFERENCES utilisateur(id_utilisateur)
	,CONSTRAINT message_utilisateur_destinataire_FK FOREIGN KEY (id_destinataire) REFERENCES utilisateur(id_utilisateur)
)ENGINE=InnoDB;


#------------------------------------------------------------
# Table: vehicule
#------------------------------------------------------------

CREATE TABLE vehicule(
        id_vehicule          Int  Auto_increment  NOT NULL ,
        id_utilisateur       Int NOT NULL ,
        id_couleur           Int NOT NULL ,
        id_marque            Int NOT NULL ,
        id_modele            Int NOT NULL ,
        nb_places_vehicule   Integer NOT NULL ,
        validite_ct_vehicule Integer NOT NULL ,
        preference_vehicule  TinyINT NOT NULL ,
        url_photo_vehicule   Varchar (50) NULL ,
        statut_vehicule      TinyINT NOT NULL
	,CONSTRAINT vehicule_PK PRIMARY KEY (id_vehicule)

	,CONSTRAINT vehicule_utilisateur_FK FOREIGN KEY (id_utilisateur) REFERENCES utilisateur(id_utilisateur)
	,CONSTRAINT vehicule_couleur_FK FOREIGN KEY (id_couleur) REFERENCES couleur(id_couleur)
	,CONSTRAINT vehicule_marque_FK FOREIGN KEY (id_marque) REFERENCES marque(id_marque)
	,CONSTRAINT vehicule_modele_FK FOREIGN KEY (id_modele) REFERENCES modele(id_modele)
)ENGINE=InnoDB;


#------------------------------------------------------------
# Table: trajet
#------------------------------------------------------------

CREATE TABLE trajet(
        id_trajet            Int  Auto_increment  NOT NULL ,
        id_utilisateur       Int NOT NULL ,
        id_vehicule          Int NOT NULL ,
        sens_trajet          TinyINT NOT NULL ,        
        type_trajet          TinyINT NOT NULL,
        datedebut_trajet     Date NOT NULL ,
        datefin_trajet       Date NOT NULL ,
        places_trajet        TinyINT NOT NULL ,
        tabac_trajet         TinyINT NOT NULL ,
        bagage_trajet        TinyINT NOT NULL ,
        comment_trajet       Varchar (100) NOT NULL ,
        statut_modere_trajet TinyINT NOT NULL DEFAULT 0,
        change_util_trajet   TinyINT NOT NULL DEFAULT 0,
        comref_mode_trajet   Varchar (100) NULL
	,CONSTRAINT trajet_PK PRIMARY KEY (id_trajet)
	,CONSTRAINT trajet_utilisateur_FK FOREIGN KEY (id_utilisateur) REFERENCES utilisateur(id_utilisateur)
	,CONSTRAINT trajet_vehicule_FK FOREIGN KEY (id_vehicule) REFERENCES vehicule(id_vehicule)
)ENGINE=InnoDB;


#------------------------------------------------------------
# Table: jour
#------------------------------------------------------------

CREATE TABLE jour(
        id_jour      Int  Auto_increment  NOT NULL ,
        id_trajet    Int NOT NULL ,
        nom_jour     Varchar (10) NOT NULL ,
        aller_retour TinyINT NOT NULL ,
        heure_aller  Time NOT NULL ,
        heure_retour Time
	,CONSTRAINT jour_PK PRIMARY KEY (id_jour)

	,CONSTRAINT jour_trajet_FK FOREIGN KEY (id_trajet) REFERENCES trajet(id_trajet)
)ENGINE=InnoDB;


#------------------------------------------------------------
# Table: adresse
#------------------------------------------------------------

CREATE TABLE adresse(
        id_adresse         Int  Auto_increment  NOT NULL ,
        id_ville           Int NOT NULL ,
        id_utilisateur     Int NOT NULL ,
        rue_adresse        Varchar(255) NOT NULL ,
        complement_adresse Varchar(255) NOT NULL ,
        favorite_adresse   TinyINT NOT NULL
	,CONSTRAINT adresse_PK PRIMARY KEY (id_adresse)

	,CONSTRAINT adresse_ville_FK FOREIGN KEY (id_ville) REFERENCES ville(id_ville)
	,CONSTRAINT adresse_utilisateur_FK FOREIGN KEY (id_utilisateur) REFERENCES utilisateur(id_utilisateur)
)ENGINE=InnoDB;


#------------------------------------------------------------
# Table: ville__trajet
#------------------------------------------------------------

CREATE TABLE ville__trajet(
        id_trajet   Int NOT NULL ,
        id_ville    Int NOT NULL ,
        ordre_ville TinyINT NOT NULL
	,CONSTRAINT ville__trajet_PK PRIMARY KEY (id_trajet,id_ville)

	,CONSTRAINT ville__trajet_trajet_FK FOREIGN KEY (id_trajet) REFERENCES trajet(id_trajet)
	,CONSTRAINT ville__trajet_ville_FK FOREIGN KEY (id_ville) REFERENCES ville(id_ville)
)ENGINE=InnoDB;


#------------------------------------------------------------
# Table: trajet__utilisateur
#------------------------------------------------------------

CREATE TABLE trajet__utilisateur(
        id_trajet          Int NOT NULL ,
        id_utilisateur     Int NOT NULL ,
        statut_utilisateur TinyINT NOT NULL,
		ajout_sur_trajet   Datetime NOT NULL DEFAULT now()
	,CONSTRAINT trajet__utilisateur_PK PRIMARY KEY (id_trajet,id_utilisateur)

	,CONSTRAINT trajet__utilisateur_trajet_FK FOREIGN KEY (id_trajet) REFERENCES trajet(id_trajet)
	,CONSTRAINT trajet__utilisateur_utilisateur_FK FOREIGN KEY (id_utilisateur) REFERENCES utilisateur(id_utilisateur)
)ENGINE=InnoDB;

