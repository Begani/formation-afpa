-- phpMyAdmin SQL Dump
-- version 4.6.4
-- https://www.phpmyadmin.net/
--
-- Client :  127.0.0.1
-- Généré le :  Lun 22 Juin 2020 à 07:36
-- Version du serveur :  5.7.14
-- Version de PHP :  5.6.25

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de données :  `afpacar`
--

-- --------------------------------------------------------

--
-- Structure de la table `droit__utilisateur__fonctionnalite`
--

CREATE TABLE `droit__utilisateur__fonctionnalite` (
  `id_fonctionnalite` int(11) NOT NULL,
  `id_droit` int(11) NOT NULL,
  `id_utilisateur` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Contenu de la table `droit__utilisateur__fonctionnalite`
--

INSERT INTO `droit__utilisateur__fonctionnalite` (`id_fonctionnalite`, `id_droit`, `id_utilisateur`) VALUES
(1, 2, 1),
(1, 2, 3),
(2, 2, 3),
(3, 2, 3),
(4, 2, 1),
(4, 2, 3),
(5, 2, 3),
(6, 2, 3),
(7, 2, 3),
(5, 3, 1),
(6, 3, 1),
(7, 3, 1),
(1, 4, 2),
(2, 4, 1),
(2, 4, 2),
(3, 4, 1),
(3, 4, 2),
(4, 4, 2),
(5, 4, 2),
(6, 4, 2),
(7, 4, 2);

--
-- Index pour les tables exportées
--

--
-- Index pour la table `droit__utilisateur__fonctionnalite`
--
ALTER TABLE `droit__utilisateur__fonctionnalite`
  ADD PRIMARY KEY (`id_fonctionnalite`,`id_droit`,`id_utilisateur`),
  ADD KEY `droit__utilisateur__fonctionnalite_droit0_FK` (`id_droit`),
  ADD KEY `droit__utilisateur__fonctionnalite_utilisateur1_FK` (`id_utilisateur`);

--
-- Contraintes pour les tables exportées
--

--
-- Contraintes pour la table `droit__utilisateur__fonctionnalite`
--
ALTER TABLE `droit__utilisateur__fonctionnalite`
  ADD CONSTRAINT `droit__utilisateur__fonctionnalite_droit0_FK` FOREIGN KEY (`id_droit`) REFERENCES `droit` (`id_droit`),
  ADD CONSTRAINT `droit__utilisateur__fonctionnalite_fonctionnalite_FK` FOREIGN KEY (`id_fonctionnalite`) REFERENCES `fonctionnalite` (`id_fonctionnalite`),
  ADD CONSTRAINT `droit__utilisateur__fonctionnalite_utilisateur1_FK` FOREIGN KEY (`id_utilisateur`) REFERENCES `utilisateur` (`id_utilisateur`);

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
