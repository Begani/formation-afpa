#------------------------------------------------------------
#        Script MySQL.
#------------------------------------------------------------


#------------------------------------------------------------
# Table: Utilisateur
#------------------------------------------------------------

CREATE TABLE Utilisateur(
        id_utilisateur Int  Auto_increment  NOT NULL ,
        nom            Varchar (100) NOT NULL ,
        prenom         Varchar (100) NOT NULL ,
        n_beneficiaire Integer NOT NULL ,
        date_naissance Date NOT NULL ,
        mot_de_passe   Varchar (30) NOT NULL ,
        email          Varchar (150) NOT NULL ,
        photo_profil   Varchar (255) NOT NULL
	,CONSTRAINT Utilisateur_PK PRIMARY KEY (id_utilisateur)
)ENGINE=InnoDB;


#------------------------------------------------------------
# Table: Ville
#------------------------------------------------------------

CREATE TABLE Ville(
        id_ville    Int  Auto_increment  NOT NULL ,
        nom_ville   Varchar (150) NOT NULL ,
        code_postal Integer NOT NULL
	,CONSTRAINT Ville_PK PRIMARY KEY (id_ville)
)ENGINE=InnoDB;


#------------------------------------------------------------
# Table: adresse_utilisateur
#------------------------------------------------------------

CREATE TABLE adresse_utilisateur(
        id_ville       Int NOT NULL ,
        id_utilisateur Int NOT NULL ,
        rue            Varchar (255) NOT NULL
	,CONSTRAINT adresse_utilisateur_PK PRIMARY KEY (id_ville,id_utilisateur)

	,CONSTRAINT adresse_utilisateur_Ville_FK FOREIGN KEY (id_ville) REFERENCES Ville(id_ville)
	,CONSTRAINT adresse_utilisateur_Utilisateur0_FK FOREIGN KEY (id_utilisateur) REFERENCES Utilisateur(id_utilisateur)
)ENGINE=InnoDB;

